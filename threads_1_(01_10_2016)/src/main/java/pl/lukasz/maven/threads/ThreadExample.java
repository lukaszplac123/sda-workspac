package pl.lukasz.maven.threads;

/**
 * Hello world!
 *
 */
public class ThreadExample implements Runnable {
    
	private String name;
	//private StringBuilder strBuilder;
	private StringBuilder strBuilder1;
	private StringBuilder strBuilder2;

	public ThreadExample(String name, StringBuilder strBuild1) {
		this.name = name;
		this.strBuilder1 = strBuild1;
	}
	public ThreadExample(String name, StringBuilder strBuild1, StringBuilder strBuild2 ) {
		this(name, strBuild1);
		this.strBuilder2 = strBuild2;
	}

	@Override
	public void run() {
		//synchronizuje StringBuildera dla petli for dzieki czemu caly for jest synchronizowany (monitor nie dopusci
		//innego watku do momentu skonczenia fora)
		//String Buffer synchronizuje tylko append dlatego dla String Buffera rowniez nie dzialalo jak trzeba
		synchronized (strBuilder1){
//		for (int i = 0; i < 10; i++){
//			strBuilder1.append("Hi. I am thread " + name + i + "\n");
//			}
			try {
				Thread.sleep(500);
				System.out.println("!!!!!!!!!!");
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//przyklad jesli chcemy zrobic zakleszczenie. czyli jesli oba watki blokuja sobie dojscie do zasobow
			synchronized (strBuilder2) {
				System.out.println("wykonalo sie");
			}
		}
		
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}

}
