package pl.lukasz.maven.threads;

public class ThreadDemo {

	public static void main (String[] args) {
		
		//wspolny zasob StringBuilder dla dwoch watkow
		//problem polega na tym ze StringBuilder jest asynchroniczny
		StringBuilder strBuild1 = new StringBuilder();
		StringBuilder strBuild2 = new StringBuilder();
		
		//wprowadzenie string buffera tez nie poprawia, gdyz on synchronizuje tylko metode append
		//StringBuffer strBuild = new StringBuffer();
		
		Thread thread1 = new Thread(new ThreadExample("Wladek", strBuild1, strBuild2));
		Thread thread2  = new Thread(new ThreadExample("Misiek", strBuild2, strBuild1));
		
		thread1.start();
		thread2.start();
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
//		try {
//			thread1.join();
//			thread2.join();
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		//bez joinow do string buildera nic sie nie zapisze albo nie wiele, gdyz main skonczyl sie szybciej
		//niz dwa pozostale watki
		//join sprawia ze main czeka na skonczenie sie dwoch watkow (mozna tez uzyc sleepa i poczekac)
		//System.out.println(strBuild1.toString());
		//System.out.println(strBuild2.toString());
		System.out.println("Watek glowny doszedl do konca ale czeka na zakonczenie dwoch watkow");
		System.out.println("Watek 1 alive:" + thread1.isAlive());
		System.out.println("Watek 2 alive:" + thread2.isAlive());
		
	}
	
}
