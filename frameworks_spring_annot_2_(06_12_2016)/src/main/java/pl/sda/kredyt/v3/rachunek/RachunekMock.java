package pl.sda.kredyt.v3.rachunek;

import org.springframework.stereotype.Component;

@Component
public class RachunekMock implements Rachunek {

    public boolean zasil(double kwota) {
        return true;
    }
    public boolean pobierz(double kwota) {
        return true;
    }

    public String getNumer() {
        return "MOCK";
    }

    public String toString() {
        return getNumer();
    }
}

