package pl.sda.kredyt.v3.rachunek;

public interface Rachunek {
    String getNumer();
    boolean zasil(double kwota);
    boolean pobierz(double kwota);
}
