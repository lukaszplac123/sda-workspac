package pl.sda.kredyt.v3;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(value = { "pl.sda.kredyt.v3" })
public class AppConfig {
}
