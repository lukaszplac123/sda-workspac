package pl.sda.kredyt.v2.rachunek;

import org.springframework.stereotype.Component;

@Component
public class RachunekImpl implements Rachunek {
    private double saldo;
    private String numer;

    public boolean zasil(double kwota) {
        saldo = saldo + kwota;
        return true;
    }
    public boolean pobierz(double kwota) {
        if(saldo >= kwota) {
            saldo = saldo-kwota;
            return true;
        }
        return false;
    }

    public void setNumer(String numer) {
        this.numer = numer;
    }

    public String getNumer() {
        return numer;
    }

    public String toString() {
        return numer;
    }
}

