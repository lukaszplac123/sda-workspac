package pl.sda.kredyt.v2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.sda.kredyt.v2.rachunek.Rachunek;

import java.util.List;

@Component
public class Bank {
	@Autowired
    private List<Rachunek> rachunki;

	public List<Rachunek> getRachunki() {
        return rachunki;
	}

	public void setRachunki(List<Rachunek> rachunki) {
        this.rachunki = rachunki;
	}

	public String toString() {
        return "Moje rachunki: "+rachunki.toString();
	}
}
