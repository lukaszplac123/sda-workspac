package pl.sda.kredyt.v2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import pl.sda.kredyt.v2.rachunek.Rachunek;

@Component
public class Klient {
    private int id;
    private String imie;
    private String nazwisko;
    @Autowired
    private Kredyt kredyt;
    @Autowired
    @Qualifier(value = "rachunekImpl")
    private Rachunek rachunek;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public Kredyt getKredyt() {
        return kredyt;
    }

    public void setKredyt(Kredyt kredyt) {
        this.kredyt = kredyt;
    }

    public Rachunek getRachunek() {
        return rachunek;
    }

    public void setRachunek(Rachunek rachunek) {
        this.rachunek = rachunek;
    }

    @Override
    public String toString() {
        return String.format("id: %s, imie: %s, nazwisko: %s, kredyt: %s", id, imie, nazwisko, kredyt);
    }
}
