package pl.sda.kredyt.v2.rachunek;

public interface Rachunek {
    String getNumer();
    boolean zasil(double kwota);
    boolean pobierz(double kwota);
}
