package pl.sda.kredyt.v1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Program {

    private static final Logger logger = LoggerFactory.getLogger(Program.class);

    private void drukujBeany(ApplicationContext context) {
        for(String beanName : context.getBeanDefinitionNames()) {
            logger.info("Bean name: {}", beanName);
        }
    }

    void przyklad() {
        ApplicationContext context = new ClassPathXmlApplicationContext("application1.xml");

        logger.info("Przyklad XML -> Adnotacje");
        drukujBeany(context);

        Bank bank = context.getBean(Bank.class);
        logger.info(bank.toString());
    }

    public static void main(String[] args) {
        Program program = new Program();
        program.przyklad();
    }
}
