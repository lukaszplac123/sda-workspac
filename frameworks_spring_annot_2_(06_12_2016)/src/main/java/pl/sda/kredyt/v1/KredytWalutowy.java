package pl.sda.kredyt.v1;

import java.text.DecimalFormat;

public class KredytWalutowy extends Kredyt {
    private double przelicznik = 1;
    private String waluta = "PLN";

    public double getPrzelicznik() {
        return przelicznik;
    }

    public void setPrzelicznik(double przelicznik) {
        this.przelicznik = przelicznik;
    }

    public String getWaluta() {
        return waluta;
    }

    public void setWaluta(String waluta) {
        this.waluta = waluta;
    }

    @Override
    public double getRata() {
        return przelicznik * super.getRata();
    }

    public double getRataWaluta() {
        return super.getRata();
    }

    @Override
    public String toString() {
        return String.format("kwota: %s %s, procent: %s, rat: %s, rata: %s",
                getKwota(),
                waluta,
                getProcent(),
                getIloscRat(),
                new DecimalFormat("#.##").format(getRata()));
    }
}
