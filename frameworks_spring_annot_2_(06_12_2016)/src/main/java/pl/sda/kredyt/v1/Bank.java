package pl.sda.kredyt.v1;

import org.springframework.beans.factory.annotation.Autowired;
import pl.sda.kredyt.v1.rachunek.Rachunek;

import java.util.List;

public class Bank {
	@Autowired
    private List<Rachunek> rachunki;

	public List<Rachunek> getRachunki() {
        return rachunki;
	}

	public void setRachunki(List<Rachunek> rachunki) {
        this.rachunki = rachunki;
	}

	public String toString() {
        return "Moje rachunki: "+rachunki.toString();
	}
}
