package singletone;

import java.util.HashMap;
import java.util.Map;

import pl.sdacademy.course.api.Student;

//singletone - tylko jedna instancja
//prywatny konstruktow - zeby nikt nie mogl tworzyc innych instancji
//metoda pobierajaca instancje singletona, jezeli jeszcze jej nie ma to j� tworzy
public class StorageSingletone {
	
		private static StorageSingletone storageSingletone;
		private Map<String, Student> storageMap = new HashMap<>();
		
		private StorageSingletone(){
		}
		
		public static StorageSingletone getInstance(){
		
			if (storageSingletone == null){
				storageSingletone = new StorageSingletone();
			}
			return storageSingletone;
		}
		
		public Map<String, Student> getStorageMap(){
			return storageMap;
		}
	
}
