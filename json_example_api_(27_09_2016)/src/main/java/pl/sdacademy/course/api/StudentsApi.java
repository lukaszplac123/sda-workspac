package pl.sdacademy.course.api;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.UUID;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import singletone.StorageSingletone;

@Path("/course")
public class StudentsApi {

	//wystawiamy API dla metody GET - pobieranie
	//dodatkowo ustaqwiamy ze zwrocony obiekt ma byc JSONem
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/students")
	public List<Student> getStudents(){
		List<Student> students = new ArrayList<>();
		//metoda 1 - foreach
//		for(Entry<String, Student> entry : StorageSingletone.getInstance().getStorageMap().entrySet()){
//			students.add(entry.getValue());
//		}
		//metoda 2 - lambda - Java 8
		StorageSingletone.getInstance().getStorageMap().forEach((key, value) -> students.add(value));
		
		return students;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/students/{id}")
	public Student getStudents(@PathParam("id") String id){
		return StorageSingletone.getInstance().getStorageMap().get(id);
	}
	
	//post - wstawianie studenta
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/students")
	public void postStudent(Student student){
		//tworzenie ID studenta
		String id = UUID.randomUUID().toString();
		//wstawianie studenta do mapy obiektu storage ingletone
		StorageSingletone.getInstance().getStorageMap().put(id , student);
	}
	
}
