package pl.sdacademy.course.api;

public class Student {
	private String name;
	private String surname;
	private String email;
	private String interests;
	
	//nie moze byc konstruktora !!!!!!!!!!!!!!!!!!!!!!!!!!1
//	public Student(String name, String surname, String email, String interests) {
//		super();
//		this.name = name;
//		this.surname = surname;
//		this.email = email;
//		this.interests = interests;
//	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getInterests() {
		return interests;
	}
	public void setInterests(String interests) {
		this.interests = interests;
	}
}
