package pl.sda.spring.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.RequestPostProcessor;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import pl.sda.spring.config.AppConfig;
import pl.sda.spring.repository.EmployeeRepository;
import pl.sda.spring.rest.model.Created;
import pl.sda.spring.rest.model.Employee;

import javax.servlet.Filter;
import java.io.IOException;
import java.util.Optional;

import static org.fest.assertions.Assertions.assertThat;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = AppConfig.class)
public class EmployeeControllerWithSecurityTest {

    @Autowired
    private WebApplicationContext wac;
    @Autowired
    private Filter securityFilter;
    @Autowired
    private EmployeeRepository repository;

    private final ObjectMapper objectMapper = new ObjectMapper();
    private MockMvc mockMvc;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).addFilter(securityFilter).build();
    }

    @Test
    public void thatCanCreateEmployee() throws Exception {
        //given
        Employee newEmployee = newEmployee();

        //when
        MvcResult result = mockMvc.perform(post("/employee")
                .content(serialize(newEmployee))
                .contentType(MediaType.APPLICATION_JSON)
                .with(testUser())
                .with(csrf()))
        //then
                .andExpect(status().isCreated())
                .andDo(print())
                .andReturn();

        String rawResponse = result.getResponse().getContentAsString();
        assertThat(rawResponse).isNotNull().isNotEmpty();

        Created created = deserialize(rawResponse, Created.class);
        assertThat(created).isNotNull();

        Optional<Employee> maybeEmployee = repository.getOne(created.getId());
        assertThat(maybeEmployee.isPresent()).isTrue();

        Employee employee = maybeEmployee.get();
        assertThat(employee.getFirstName()).isEqualTo(newEmployee.getFirstName());
        assertThat(employee.getLastName()).isEqualTo(newEmployee.getLastName());
        assertThat(employee.getRole()).isEqualTo(newEmployee.getRole());
    }

    @Test
    public void thatCanUpdateEmployee() throws Exception {
        //given
        Employee newEmployee = newEmployee();
        repository.create(newEmployee);

        Employee updatedEmployee = updatedEmployee();

        //when
        mockMvc.perform(put("/employee/{id}", newEmployee.getId())
                .content(serialize(updatedEmployee))
                .contentType(MediaType.APPLICATION_JSON)
                .with(testUser())
                .with(csrf()))
        //then
                .andExpect(status().isOk())
                .andDo(print());

        Optional<Employee> maybeEmployee = repository.getOne(newEmployee.getId());
        assertThat(maybeEmployee.isPresent()).isTrue();

        Employee employee = maybeEmployee.get();
        assertThat(employee.getFirstName()).isEqualTo(updatedEmployee.getFirstName());
        assertThat(employee.getLastName()).isEqualTo(updatedEmployee.getLastName());
        assertThat(employee.getRole()).isEqualTo(updatedEmployee.getRole());
    }

    private RequestPostProcessor testUser() {
        return user("user").password("pass").roles("USER");
    }

    private Employee newEmployee() {
        return Employee.builder().firstName("Maciej").lastName("Zurawski").role("USER").build();
    }

    private Employee updatedEmployee() {
        return Employee.builder().firstName("Tomasz").lastName("Frankowski").role("USER").build();
    }

    private String serialize(Object object) {
        try {
            return objectMapper.writeValueAsString(object);
        } catch (JsonProcessingException ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
    }

    private <T> T deserialize(String serialized, Class<T> clazz) {
        try {
            return objectMapper.readValue(serialized, clazz);
        } catch (IOException ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
    }
}
