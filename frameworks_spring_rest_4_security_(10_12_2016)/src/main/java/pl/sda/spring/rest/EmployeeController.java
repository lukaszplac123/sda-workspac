package pl.sda.spring.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pl.sda.spring.repository.EmployeeRepository;
import pl.sda.spring.rest.model.Created;
import pl.sda.spring.rest.model.Employee;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping(path = "/employee")
public class EmployeeController {

    @Autowired
    private EmployeeRepository repository;

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Created> createEmployee(@RequestBody @Valid Employee employee) {
        repository.create(employee);
        return ResponseEntity.status(HttpStatus.CREATED).body(Created.from(employee));
    }

    @RequestMapping(method = RequestMethod.PUT, path = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> updateEmployee(@PathVariable("id") Long id, @RequestBody @Valid Employee employee) {
        employee.setId(id);
        repository.update(employee);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(method = RequestMethod.GET, path = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Employee>> getAll() {
        return ResponseEntity.ok(repository.getAll());
    }
}
