package pl.sda.spring.rest.model;

public class Created {
    long id;

    private Created() { /* do nothing */ }

    Created(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public static Created from(Employee employee) {
        return new Created(employee.getId());
    }
}
