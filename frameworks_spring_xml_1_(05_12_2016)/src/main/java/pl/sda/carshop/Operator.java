package pl.sda.carshop;

import java.util.List;

public class Operator {

	private List<CarShop> shops;

	public void setShops(List<CarShop> shops) {
		this.shops = shops;
	}

	public List<CarShop> getShops() {
		return shops;
	}

	public void prepareData() {
		for(CarShop shop : shops) {
			for(int i = 0; i < 5; i++) {
				Car car = new Car("Fiat", "Panda", 1000 + i);
				shop.addCar(car);
			}
		}
	}

	public void printData() {
		for(CarShop shop : shops) {
			System.out.println("Salon: " + shop.getName() + " wlasciciel: " + shop.getOwner().getName());
			for (Car car : shop.getCars()) {
				System.out.println(car.getMake() + " " + car.getModel() + " " + car.getPrice());
			}
		}
	}
}
