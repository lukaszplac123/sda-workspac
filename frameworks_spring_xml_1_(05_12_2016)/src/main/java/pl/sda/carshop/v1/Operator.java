package pl.sda.carshop.v1;

public class Operator {

	private CarShop carShop;

	public void setCarShop(CarShop carShop) {
		this.carShop = carShop;
	}

	public void prepareData() {
		for(int i = 0; i < 5; i++) {
			Car car = new Car("Fiat", "Panda", 1000 + i);
			carShop.addCar(car);
		}
	}

	public void printData() {
		System.out.println("Salon: " + carShop.getName() + " wlasciciel: " + carShop.getOwner().getName());
		for(Car car:carShop.getCars()) {
			System.out.println(car.getMake() + " " + car.getModel() + " " + car.getPrice());
		}
	}
}
