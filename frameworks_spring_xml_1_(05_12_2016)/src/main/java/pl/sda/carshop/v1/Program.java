package pl.sda.carshop.v1;


import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Program {

	void traditionalApproach() {
		Owner owner = new Owner();
		owner.setName("Jan Kowalski");
		owner.setAddress("Krakow");

		CarShop shop = new CarShop();
		shop.setName("Salon samochodowy");
		shop.setOwner(owner);

		Operator operator = new Operator();
		operator.setCarShop(shop);

		operator.prepareData();
		operator.printData();
	}

	void springApproach() {
		ApplicationContext context = new ClassPathXmlApplicationContext("application1.xml");

		Operator operator = context.getBean("operator", Operator.class);
		operator.prepareData();
		operator.printData();
	}

	public static void main(String[] args) {
		Program program = new Program();
		program.traditionalApproach();
		program.springApproach();
	}
}
