package pl.sda.carshop;


import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Program {

	void zadanie2() {
		ApplicationContext context = new ClassPathXmlApplicationContext("application2.xml");

		Operator operator = context.getBean("operator", Operator.class);
		operator.prepareData();
		operator.printData();
	}

	void zadanie3() {
		ApplicationContext context = new ClassPathXmlApplicationContext("application3.xml");

		Operator operator = context.getBean("operator", Operator.class);
		operator.prepareData();
		operator.printData();
		operator.getShops().get(0).getOwner().setName("Waldek Kiepski");
		operator.printData();
	}

	public static void main(String[] args) {
		Program program = new Program();
//		program.zadanie2();
		program.zadanie3();
	}
}
