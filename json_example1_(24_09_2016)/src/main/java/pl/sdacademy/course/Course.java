package pl.sdacademy.course;

import java.util.List;

public class Course {
	private String name;
	private String duration;
	private List<Topics> topics;
	private List<Participants> participants;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDuration() {
		return duration;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
	public List<Topics> getTopics() {
		return topics;
	}
	public void setTopics(List<Topics> topic) {
		this.topics = topic;
	}
	public List<Participants> getParticipants() {
		return participants;
	}
	public void setParticipants(List<Participants> participants) {
		this.participants = participants;
	}
}
