package pl.sdacademy.course;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Set;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.networknt.schema.JsonSchema;
import com.networknt.schema.JsonSchemaFactory;
import com.networknt.schema.ValidationMessage;

public class GsonExample 
{
    static File fileJson = null;
    static File fileJsonSchema = null;
    static ObjectMapper jsonMapper = null;
    InputStream schemaStream = null;
	JsonSchema schema = null;
    JsonNode node = null;
    
    
    
	
	public static void main( String[] args ) throws JsonParseException, JsonMappingException, IOException
    {
        GsonExample gsonExample = new GsonExample();
		fileJson = new File("src/main/resources/course.json");
		fileJsonSchema = new File("src/main/resources/course.schema");
		jsonMapper = new ObjectMapper();
		
        if (gsonExample.jsonValidationErrorsSet().isEmpty()){
        	gsonExample.showJson();
        } else{
        	System.out.println("Walidacja nieudana. Przetwarzanie zakonczone.");
        }
    }
    
	public Set<ValidationMessage> jsonValidationErrorsSet() throws JsonProcessingException, IOException{
		
		Set<ValidationMessage> setToBeReturned;
		
	    schemaStream = new FileInputStream(fileJsonSchema);
		schema = new JsonSchemaFactory().getSchema(schemaStream);
		node = jsonMapper.readTree(fileJson);
		
		//walidacja
		Set<ValidationMessage> errors = schema.validate(node);

		for (ValidationMessage item : errors){
			System.out.println(item.getMessage());
			System.out.println(item.getCode());
		}
		setToBeReturned = errors;
	return setToBeReturned;	
	}
	
	public void showJson() throws JsonParseException, JsonMappingException, IOException{
		Course course = null;
		course = jsonMapper.readValue(fileJson, Course.class);
		
		System.out.println(course.getName());
		System.out.println(course.getDuration());
		course.getTopics().forEach((item) -> {
									System.out.println(item.getTopic());
									System.out.println(item.getDuration());
		});
		
		course.getParticipants().forEach((item) -> {
			System.out.println(item.getName());
			System.out.println(item.getSurname());
			System.out.println(item.getPhoneNumber());
			System.out.println(item.getEmail());	
		});
		
	}
    
}
