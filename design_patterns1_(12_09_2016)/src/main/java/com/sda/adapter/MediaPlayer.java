package com.sda.adapter;


public interface MediaPlayer {

    void play(String audioType, String fileName);
}
