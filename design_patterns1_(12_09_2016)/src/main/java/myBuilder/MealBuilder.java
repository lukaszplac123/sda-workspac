package myBuilder;

public class MealBuilder {
	
	private Meal meal;
	
	public MealBuilder(){
		meal = new Meal();
	}

	public Meal prepareBeefBurger (){
		meal.addItem(new BeefBurger());
		meal.addItem(new Cocke());
		return meal;
	}
	
	public Meal prepareChickenBurger (){
		meal.addItem(new ChickenBurger());
		meal.addItem(new Pepsi());
		return meal;
	}	
	
}
