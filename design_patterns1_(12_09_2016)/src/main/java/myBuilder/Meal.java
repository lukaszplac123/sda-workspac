package myBuilder;

import java.util.ArrayList;
import java.util.List;

public class Meal {

	public List<Item> listItem;
	
	public Meal() {
		
		listItem = new ArrayList<Item>();
		
	}
	
	public void addItem(Item item){
		listItem.add(item);
	}
	
	public float getCost(){
		float sum = 0f;
		for (Item item: listItem){
			sum += item.price();
		}
		return sum;
	}
	
	public void showItems(){
		for (Item item: listItem){
			System.out.println(item.name());
		}
	}
}
