package myBuilder;

public interface Item {

	String name ();
	Packing packing();
	float price();
	
}
