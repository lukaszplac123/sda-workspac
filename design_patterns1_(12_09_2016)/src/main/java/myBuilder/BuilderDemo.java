package myBuilder;

public class BuilderDemo {

	public static void main (String[] args){
		
		MealBuilder set1 = new MealBuilder();
		Meal meal1 = set1.prepareBeefBurger();
		
		MealBuilder set2 = new MealBuilder();
		Meal meal2 = set2.prepareChickenBurger();
		
		System.out.println("Beef Burger zestaw:");
		System.out.println(meal1.getCost());
		meal1.showItems();
		System.out.println("-----------");
		System.out.println("Chicken Burger zestaw:");
		System.out.println(meal2.getCost());
		meal2.showItems();
	}
}
