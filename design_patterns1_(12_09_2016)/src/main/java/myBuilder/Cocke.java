package myBuilder;

public class Cocke extends ColdDrink{

	@Override
	public String name() {
		return "Coke";
	}

	@Override
	public float price() {
		return 2.7f;
	}

}
