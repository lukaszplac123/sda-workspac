package mySingleton;

import java.util.HashMap;
import java.util.Map;

public class MoneyExchanger {

	private static MoneyExchanger moneyExchanger;
	private Map<String, Rate> mapOfExchange;
	
	private MoneyExchanger(){
		
	}
	
	public static MoneyExchanger getInstance(){
		if (moneyExchanger == null){
			moneyExchanger = new MoneyExchanger();
		}
		return moneyExchanger;
	}
	
	public void exchange(Money money, String targetCurrency){
		
		Rate rateOfExchange = mapOfExchange.get(money.getCurrency()+"->"+targetCurrency);
		double afterExchange = money.getAmount() * rateOfExchange.getRate();
		System.out.println("Kurs wymiany "+money.getCurrency()+" na "+targetCurrency+" wynosi -> "+afterExchange);
		
		
	}
	
	public void setCurencyRate(String fromCurrency,String toCurrency,double rate){
		
		if (mapOfExchange == null) mapOfExchange = new HashMap<String, Rate>();
		
		Rate newRate = new Rate(fromCurrency, toCurrency, rate);
		mapOfExchange.put(fromCurrency+"->"+toCurrency, newRate);
		
		
	}
	
}
