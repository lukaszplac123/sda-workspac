package mySingleton;

public class Money {
	
	private double amount;
	private String currency;
	
	public Money(double amount, String currency) {
		this.setAmount(amount);
		this.setCurrency(currency);
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	
}
