package mySingleton;

public class MoneyExchangerDemo {

	public static void main(String[] args){
		MoneyExchanger moneyExchanger = MoneyExchanger.getInstance();
		
//		PLN -> USD 3.84
//		USD -> PLN 3.87
//		PLN -> EUR 4.32
//		EUR -> PLN 4.35
//		PLN -> GBP 5.10
//		GBP -> PLN 5.14
//		PLN -> CHF 3.93 
//		CHF -> PLN 3.99
		
		moneyExchanger.setCurencyRate("PLN", "USD", 3.84);
		moneyExchanger.setCurencyRate("USD", "PLN", 3.87);
		moneyExchanger.setCurencyRate("PLN", "EUR", 4.32);
		moneyExchanger.setCurencyRate("EUR", "PLN", 4.35);
		moneyExchanger.setCurencyRate("PLN", "GBP", 5.10);
		moneyExchanger.setCurencyRate("GBP", "PLN", 5.14);
		moneyExchanger.setCurencyRate("PLN", "CHF", 3.87);
		moneyExchanger.setCurencyRate("CHF", "PLN", 3.99);
		
		
		Money money1 = new Money(4.12, "GBP");
		
		moneyExchanger.exchange(money1, "PLN");
		
		
		
	}
	
}
