package mySingleton;

public class Rate {

	String fromCurrency;
	String toCurrency;
	double rate;
	
	public Rate(String from, String to, double rate){
		
		fromCurrency = from;
		toCurrency = to;
		this.rate = rate;
		
	}

	public double getRate() {
		return rate;
	}
	
	
	
}
