package myAdapter;

public class AdapterDemo {

	public static void main(String[] args) {
		
		Shape rectangle = new Rectangle();
		Shape square = new Square();
		Shape circle = new CircleAdapter();
		
		rectangle.draw();
		square.draw();
		circle.draw();

	}

}
