package pl.adcademy.course.client;

import java.io.BufferedWriter;
import java.io.File;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Comparator;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;

public class HelloConsume {

	
    public static void main(String[] args) throws IOException {
    	
        String url = "http://rest-showcase.cloudhub.io/api/students";
        File csvFile = new File("students.csv");
        File csvFileSorted = new File("studentsSorted.csv");
        File csvFileSortedInterests = new File("studentsInterestsSorted.csv");
        
        if (csvFile.exists()){
        	csvFile = new File("students.csv");
        }
        if (csvFileSorted.exists()){
        	csvFileSorted = new File("studentsSorted.csv");
        }
        if (csvFileSortedInterests.exists()){
        	csvFileSortedInterests = new File("studentsInterestsSorted.csv");
        }
        
        BufferedWriter writer1 = new BufferedWriter(new FileWriter(csvFile));
        BufferedWriter writer2 = new BufferedWriter(new FileWriter(csvFileSorted));
        BufferedWriter writer3 = new BufferedWriter(new FileWriter(csvFileSortedInterests));
        Consume consume = new Consume();
        
        //1.metoda zwracajaca liste z serwera
        //2.metoda zapisujaca ta liste do pliku csv
        //3.liste posortowac by name
        consume.getResponseFromREST(url, writer1);
        
        consume.getStudentsList().sort(new ListSorterByName());
        writeToCSV(writer2, consume);
        
        consume.getStudentsList().sort(new ListSorterByInterest());
        writeToCSV(writer3, consume);
        
        writer1.close();
        writer2.close();
        writer3.close();
    }
    
    private static void writeToCSV(BufferedWriter writer, Consume consume) throws IOException{
    	StringBuilder buildString = new StringBuilder();
    	consume.getStudentsList().forEach((student) -> buildString.append(student.toString()));
        writer.write(buildString.toString());
    }

}