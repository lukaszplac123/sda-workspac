package pl.adcademy.course.client;

import java.util.Comparator;

public class ListSorterByInterest implements Comparator<Student>{

	@Override
	public int compare(Student o1, Student o2) {
		if (o1.getInterests() == null){
			return 1;
		}
		
		if (o2.getInterests() == null){
			return 1;
		}
		return o1.getInterests().compareTo(o2.getInterests());
	}

}
