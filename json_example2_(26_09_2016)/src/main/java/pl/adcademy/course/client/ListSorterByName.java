package pl.adcademy.course.client;

import java.util.Comparator;

public class ListSorterByName implements Comparator<Student>{

	@Override
	public int compare(Student o1, Student o2) {
		if (o1.getName() == null){
		 return 1;
		}
		if (o2.getName() == null){
		 return 1;
		}
		return o1.getName().compareTo(o2.getName());
	}

}
