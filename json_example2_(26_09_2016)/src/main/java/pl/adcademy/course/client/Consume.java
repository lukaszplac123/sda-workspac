package pl.adcademy.course.client;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;



import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;


public class Consume {
	
    final static Logger logger = Logger.getLogger(Consume.class);
    List<Student> studentsList;

    public List<Student> getStudentsList() {
		return studentsList;
	}

	public void setStudentsList(List<Student> studentsList) {
		this.studentsList = studentsList;
	}

	//GET - pobiera zasob w RESCIE
    public String getResponseFromREST(String url, BufferedWriter writer) {
        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(url);
        StringBuffer response = new StringBuffer();
        try {
            HttpResponse httpResponse = client.execute(request);
            logger.info("Response Code : " + httpResponse.getStatusLine().getStatusCode());
            BufferedReader rd1 = new BufferedReader(
                    new InputStreamReader(httpResponse.getEntity().getContent()));
            String csvFormat = mapJsonToCSVFormat(rd1);
            writer.write(csvFormat);
            
        } catch (IOException e) {
            logger.error("Error while consuming: ", e);
        }
        return "";
    }
    
    //PUT - updatuje zasob w RESTcie
    public String sendPutRequest(String url, String id) throws JsonProcessingException{
    	Student student = new Student("Apacz2", "Apacz2", "apacz2@apacz2", "marchewka2");
        ObjectMapper mapper = new ObjectMapper();
        String jsonRequest = mapper.writeValueAsString(student);
    	
    	HttpClient client = HttpClientBuilder.create().build();
        HttpPut put = new HttpPut(url+id+"/");
        StringBuffer response = new StringBuffer();
        try {
        
        	StringEntity strEntity = new StringEntity(jsonRequest);
      
        	put.setHeader("content-type", "application/json");
        
        	put.setEntity(strEntity);
 
        
        	HttpResponse httpResponse = client.execute(put);
        	
        	for(Header header : httpResponse.getAllHeaders()){
        			response.append(header.getName() + " : "+ header.getValue());
        	}
        	response.append(httpResponse.getStatusLine());
        }catch (IOException e) {
            logger.error("Error while consuming: ", e);
        }
        
        return response.toString();
   }
    
    
    //POST tworzy - wg dokumentacji REST
    public String sendPostRequest(String url) throws JsonProcessingException, IOException {
    	//definiujemy klienta
        HttpClient client = HttpClientBuilder.create().build();
        //rodzaj requestu HTTP
        HttpPost httpPost = new HttpPost(url);
        StringBuffer response = new StringBuffer();
        //String reprezentujacy Jsona - metoda 1
//        String jsonRequest = "{"
//        		+ "\"name\":\"ABAB\","
//        		+ "\"surname\":\"TestFromApp\","
//        		+ "\"email\":\"test\","
//        		+ "\"interests\":\"testing\""
//        		+ "}";
        
        //Tworzenie stringa bezposrednio na podstawie obiektu student - metoda 2
        Student student = new Student("Apacz", "Apacz", "apacz@apacz", "marchewka");
        ObjectMapper mapper = new ObjectMapper();
        String jsonRequest = mapper.writeValueAsString(student);
        
        try {
        	//Tworzenie Jsona ze stringa na potrzeby requestu
        	StringEntity strEntity = new StringEntity(jsonRequest);
        	//ustawiamy type headera json
        	httpPost.setHeader("content-type", "application/json");
        	//ustawienie kontentu dla requestu HTTP
        	httpPost.setEntity(strEntity);
        	//uruchamianie requestu i wpisanie wyniku do httpRequest
        	HttpResponse httpResponse = client.execute(httpPost);
        	//wyswietlenie naglowku odpowiedzi servera
        	for(Header header : httpResponse.getAllHeaders()){
        			response.append(header.getName() + " : "+ header.getValue());
            }
        	response.append(httpResponse.getStatusLine());
        } catch (IOException e) {
            logger.error("Error while creating request: ", e);
        }
        return response.toString();
    }
    
    private String mapJsonToCSVFormat(BufferedReader reader){
    	
    	ObjectMapper mapper = new ObjectMapper();
    	List<Student> students = null;
    	try {
			 students = mapper.readValue(reader, new TypeReference<List<Student>>() {});
			 this.studentsList = students;
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}	
    	StringBuilder buildString = new StringBuilder();
    	students.forEach((student) -> buildString.append(student.toString()));
    	return buildString.toString();
    }
}
