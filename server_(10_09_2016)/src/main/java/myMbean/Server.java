package myMbean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Server	implements ServerMBean {
	
	private List<Customer> list;
	private String newName;
	private int newAge;
	
	Server (){
		Customer c1 = new Customer ("Antoni", 29);
		Customer c2 = new Customer ("Wies�aw", 52);
		Customer c3 = new Customer ("Zdzis�aw", 44);
		Customer c4 = new Customer ("Kazek", 44);
		Customer c5 = new Customer ("Lutek", 65);
		this.newName = "ktos";
		this.newAge = 0;
		list = Arrays.asList(c1,c2,c3,c4,c5);
	}
	
	@Override
	public int getRegisteredCustomers() {
		return list.size();
	}


	@Override
	public void addCustomerToList() {
		Customer customer = new Customer (this.getName(), this.getAge());
		this.list.add(customer);	
	}

	@Override
	public void setName(String name) {
		this.newName = name;
	}

	@Override
	public void setAge(int age) {
		this.newAge = age;
	}

	@Override
	public String getName() {
		return this.newName;
		
	}

	@Override
	public int getAge() {
		return this.newAge;
	}
	

}
