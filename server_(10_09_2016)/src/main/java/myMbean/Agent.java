package myMbean;

import java.lang.management.ManagementFactory;

import javax.management.MBeanServer;
import javax.management.ObjectName;

import com.sda.jmx.Hello;

public class Agent {

	private MBeanServer mbs = null;
	
	public Agent(){
        mbs = ManagementFactory.getPlatformMBeanServer();

        Server server = new Server();
        ObjectName serverName = null;

        try {
            serverName = new ObjectName("FOO:name=Server");
            mbs.registerMBean(server, serverName);
        } catch (Exception e) {
            e.printStackTrace();
        }
	}
	
	
    public void waitForEnterPressed() {
        try {
            System.out.println("Press  to continue...");
            System.in.read();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
  
	
}
