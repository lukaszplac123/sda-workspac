package myMbean;

public interface ServerMBean {
	
	int getRegisteredCustomers();
	public void setName(String name);
	public String getName();
	public void setAge(int age);
	public int getAge();
	public void addCustomerToList();
	
	
}
