package mergeArrays;

import java.util.Arrays;
import java.util.Random;

public class MergerDemo {

	public static void main (String[] args){
		ArrayMerger arrayMerger = new ArrayMerger();
		
		int[] table1 = getRandomInts(10, 100);
		int[] table2 = getRandomInts(7, 100);
		
		Arrays.sort(table1);
		Arrays.sort(table2);
		
		System.out.println(Arrays.toString(table1));
		System.out.println(Arrays.toString(table2));
		System.out.println("---------------");
		System.out.println(Arrays.toString(arrayMerger.merge(table1, table2)));
		
		
		
	}	
	public static int[] getRandomInts(int size, int range){
		Random rand = new Random();
		int[] tempArray = new int[size];
		for (int i = 0; i < size; i++){
			int random = rand.nextInt(range);
			tempArray[i] = random;
		}
		return tempArray;
	}
	
}
