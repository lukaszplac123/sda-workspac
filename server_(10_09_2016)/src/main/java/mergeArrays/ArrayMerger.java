package mergeArrays;

public class ArrayMerger {

	public ArrayMerger(){
		
	}
	
	
	int[] merge(int[] tab1, int[] tab2){
		
		int[] tempTab =  new int[tab1.length + tab2.length];
		int i = 0, j = 0, k = 0;
		
		while (i < tab1.length && j<tab2.length){
			if (tab1[i] < tab2[j]){
				tempTab[k] = tab1[i];
				i++;
				k++;
			} else{
				tempTab[k] = tab2[j];
				j++;
				k++;
			}
		}
		
		while (i < tab1.length){
			tempTab[k] = tab1[i];
			k++;
			i++;
		}
		
		while (j < tab2.length){
			k++;
			j++;	
		}
		return tempTab;
	}
}
