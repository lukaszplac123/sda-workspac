package Numbers;

import java.util.Scanner;

public class SumOfInts {

	public static void main (String[] args){
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Podaj jakas liczbe:");
		int number = scan.nextInt();
		int modulo = 0;
		int sum = 0;
		while (number != 0){
			modulo = number % 10;
			sum += modulo;
			number = number/10;
		}
		
		System.out.println("Suma cyfr:");
		System.out.println(sum);
	}
	
}
