package Numbers;

import java.util.Arrays;
import java.util.Scanner;

public class NumberDiv {

	public static int[] dividersArray;
	public static int[] perfectNumbers;
	
	public static void main (String[] args){
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Podaj jakas liczbe:");
		int number = scan.nextInt();
		
		
		NumberDiv dividers = new NumberDiv();
		
		dividersArray = dividers.getFactors(number);
		
		System.out.println(Arrays.toString(dividersArray));
		
		perfectNumbers = dividers.getPerfects(1000);
		
		System.out.println(Arrays.toString(perfectNumbers));
		
	}
		
	public int[] getFactors(int number){
		int[] array = new int[number];
		int j=0;
		for (int i = 1; i < number + 1 ; i++ ){
			if (number%i == 0) {
					array[j++] = i;
			}
		}
		
		return array;		
	}
	
	public int[] getPerfects(int range){
		int[] arrayOfPerfects = new int[range];
		int[] arrayOfDividers;
		int sumOfdividers;
		int j=0;
		for (int i = 1; i < range + 1 ; i++ ){
			dividersArray = getFactors(i);
			sumOfdividers = sumOfDivs(i);
			if (sumOfdividers == i) arrayOfPerfects[j++] = i;
		}
		return arrayOfPerfects;		
	}
	
	public int sumOfDivs(int number){
		int sum = 0;
		for (int divider : dividersArray){
					if (divider < number)
						sum += divider;	
		}
		return sum;	
	}
		
}
	

