package myBank;

import java.util.List;

public class BankAccount {

	private String nameOfAccount;
	private int cashOnAccount;
	
	public BankAccount(String name, int cash) {
		
		this.setNameOfAccount(name);
		this.setCashOnAccount(cash);
		
	}

	public int getCashOnAccount() {
		return cashOnAccount;
	}

	public void setCashOnAccount(int cashOnAccount) {
		this.cashOnAccount = cashOnAccount;
	}

	public String getNameOfAccount() {
		return nameOfAccount;
	}

	public void setNameOfAccount(String nameOfAccount) {
		this.nameOfAccount = nameOfAccount;
	}
	
}
