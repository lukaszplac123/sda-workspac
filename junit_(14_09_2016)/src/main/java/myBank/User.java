package myBank;

import java.util.ArrayList;
import java.util.List;

public class User {

	private String name;
	private List<BankAccount> accounts;
	private long sum;
	
	public User(String name) {
		this.name = name;
		accounts = new ArrayList<>();
	}
	
	public void addBankAccount(BankAccount bankAccount){
		accounts.add(bankAccount);
	}
	
	public long getTotalAmount(){
		sum = 0;
		accounts.forEach((item) -> {sum += item.getCashOnAccount();});
		return sum;	
	}
	
	public long getAmount(String account){
		for (BankAccount bk : this.accounts){
			if (account.equals(bk.getNameOfAccount())) return bk.getCashOnAccount();
		}
		return -1;	
	}
	
}
