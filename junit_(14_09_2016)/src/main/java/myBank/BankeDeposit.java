package myBank;

public class BankeDeposit {

	private long startMoney;
	private int years;
	private double interestRate;
	private int periods;
	
	
	public int getPeriods() {
		return periods;
	}
	public void setPeriods(int periods) {
		this.periods = periods;
	}
	public double getInterestRate() {
		return interestRate;
	}
	public void setInterestRate(double interestRate) {
		this.interestRate = interestRate;
	}
	public int getYears() {
		return years;
	}
	public void setYears(int years) {
		this.years = years;
	}
	public long getStartMoney() {
		return startMoney;
	}
	public void setStartMoney(long startMoney) {
		this.startMoney = startMoney;
	}

}
