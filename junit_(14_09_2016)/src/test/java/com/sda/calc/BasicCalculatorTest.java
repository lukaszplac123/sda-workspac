package com.sda.calc;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class BasicCalculatorTest {

    private Calculator calculator;

    @Before
    public void setUp() {
        calculator = new BasicCalculator();
        //       System.out.println("Before");
    }

    @After
    public void tearDown() {
        calculator = null;
//        System.out.println("After");
    }


    @Test
    public void testAdd() {
        //given
        int expected = 5;

        //when
        int result = calculator.add(2, 3);

        //then
        Assert.assertEquals(expected, result);
    }

    @Test
    public void testSubtract() {
    	//given
        int expected = 5;

        //when
        int result = calculator.subtract(10, 5);

        //then
        Assert.assertEquals(expected, result);
    }


    @Test
    public void testMultiply() {
    	//given
        int expected = 20;

        //when
        int result = calculator.multiply(2, 10);

        //then
        Assert.assertEquals(expected, result);
    }


	@Test
    public void testDivide() {
    	//given
        float expected = 15.0f;

        //when
        float result = calculator.divide(30, 2);

        //then
        Assert.assertEquals(expected, result, 0.01f);
    }


    @Test(expected = IllegalArgumentException.class)
    public void testThrownIllegalArgumentException() {
        calculator.divide(10, 0);
    }


    @Test
    public void testThrownExceptionOtherApproach() {
        try {
            calculator.divide(10, 0);
            Assert.fail();
        } catch (IllegalArgumentException e) {
            Assert.assertEquals("Can't divide by 0", e.getMessage());
        }
    }


}