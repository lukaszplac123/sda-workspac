package myBank;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class BankServiceTest {

	private BankeDeposit bankDeposit;
	private BankService bankService;
	
	@Before
	public void setUp() throws Exception {
		
		bankDeposit = new BankeDeposit();
		bankService = new BankService();
		
		bankDeposit.setInterestRate(6);//r [%]
		bankDeposit.setPeriods(4); //m
		bankDeposit.setStartMoney(2000);//V0
		bankDeposit.setYears(2); //n
		
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCalculateInvestment() {
		
		//given
		double expected = 1500.0; 
		
		//when
		double actual = bankService.calculateInvestment(bankDeposit);
		System.out.println(actual);
		
		//then
		Assert.assertEquals(expected, actual, 0.1f);
		
		//fail("Not yet implemented");
	}

}
