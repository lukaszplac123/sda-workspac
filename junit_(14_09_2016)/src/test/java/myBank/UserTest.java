package myBank;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import junit.framework.Assert;

public class UserTest {

	private User user;
	
	@Before
	public void setUp() throws Exception {
		user = new User("Jan Wisniewski");
		user.addBankAccount(new BankAccount("mBank", 315));
		user.addBankAccount(new BankAccount("PEKAO", 1841));
		user.addBankAccount(new BankAccount("JanuszBank", 2753));
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetTotalAmount() {
		//given
		long expected = 4909;
		
		//when
		long actual  = user.getTotalAmount();
		System.out.println(actual);
		
		//then
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	public void testGetAmount() {
		//given
		long expected = 1841;
		
		//when
		long actual  = user.getAmount("PEKAO");
		System.out.println(actual);
		
		//then
		Assert.assertEquals(expected, actual);

	}

}
