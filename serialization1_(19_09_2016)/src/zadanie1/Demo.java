package zadanie1;

import java.util.Arrays;

public class Demo {

	public static void main (String[] args){
		
		Person person1 = new Person("Marian Kowalski", 50, Arrays.asList("Fanta", "Cola"), new Address("Warszawa", "Mila", 23));
		
		Person person2 = new Person("Pawl Kukiz", 48, Arrays.asList("Pepsi", "Sprite", "water"), new Address("Warszawa", "3 Maja", 23));
		
		Person person3 = new Person("Agata Buzek", 37, Arrays.asList("milk"), null);
		
		ReportPrinter rp = new ReportPrinter();
		
		rp.addPersonToList(person1);
		rp.addPersonToList(person2);
		rp.addPersonToList(person3);
		
		rp.printReport();
		
	}
	
}
