package zadanie1;

import java.lang.reflect.Type;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class PersonSerializer implements JsonSerializer<Person>{

	@Override
	public JsonElement serialize(Person arg0, Type arg1, JsonSerializationContext arg2) {
		
		final JsonObject jsonObject1 = new JsonObject();
		jsonObject1.add("imie", new JsonPrimitive(arg0.name));
		jsonObject1.add("wiek", new JsonPrimitive(arg0.age));
		jsonObject1.add("zamowienia", arg2.serialize(arg0.orders));
		jsonObject1.add("adres", arg2.serialize(arg0.address));
		return jsonObject1;
	}
}
