package zadanie1;

import java.lang.reflect.Type;
import java.util.List;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class Person {

	String name;
	int age;
	List<String> orders;
	Address address;
	
	public Person(String name, int age, List<String> orders, Address address){
		this.name = name;
		this.age = age;
		this.orders = orders;
		this.address = address;
	}

}
