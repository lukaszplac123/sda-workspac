package zadanie1;

import java.awt.print.Book;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class AddressSerializer implements JsonSerializer<Address>{


	@Override
	public JsonElement serialize(Address arg0, Type arg1, JsonSerializationContext arg2) {
		final JsonObject jsonObject1 = new JsonObject();
		jsonObject1.add("miasto", new JsonPrimitive(arg0.city));
		jsonObject1.add("ulica", new JsonPrimitive(arg0.street));
		jsonObject1.add("numer", new JsonPrimitive(arg0.number));
		return jsonObject1;
	}
	
	
}
