package zadanie1;

import java.awt.print.Book;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class ReportPrinter {

	List<Person> personList;
	
	public ReportPrinter() {
		personList = new ArrayList<Person>();
	}
	
	public void printReport(){
		
		GsonBuilder gb = new GsonBuilder();
			gb.registerTypeAdapter(Person.class, new PersonSerializer())
			  .registerTypeAdapter(Address.class, new AddressSerializer())
			  .serializeNulls()
			  .setPrettyPrinting();
			
		Gson gson = gb.create();
		String report = gson.toJson(personList);
		
		System.out.println(report);
	}
	
	
	
	public void addPersonToList(Person person){
		personList.add(person);
	}
	
	
	
}
