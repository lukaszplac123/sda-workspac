package pl.sda.spring.aop.model.carshop;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import java.util.List;

public class Operator implements ApplicationContextAware {

	private ApplicationContext context;
	private List<CarShop> shops;

	public void setShops(List<CarShop> shops) {
		this.shops = shops;
	}

	public List<CarShop> getShops() {
		return shops;
	}

	public void prepareData() {
		for(CarShop shop : shops) {
			for(int i = 0; i < 5; i++) {
				Car car = context.getBean(Car.class);
				car.setMake("Fiat");
				car.setModel("Panda");
				car.setPrice(1000.0 + i);
				shop.addCar(car);
			}
		}
	}

	public void printData() {
		for(CarShop shop : shops) {
			System.out.println("Salon: " + shop.getName() + " wlasciciel: " + shop.getOwner().getName());
			for (Car car : shop.getCars()) {
				System.out.println(car.getMake() + " " + car.getModel() + " " + car.getPrice());
			}
		}
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.context = applicationContext;
	}
}
