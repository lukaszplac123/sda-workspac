package pl.sda.spring.aop.p1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import pl.sda.spring.aop.model.customer.CustomerService;

public class Program {

    private static final Logger logger = LoggerFactory.getLogger(Program.class);
    private final CustomerService service;

    Program(ApplicationContext context) {
        service = context.getBean(CustomerService.class);
    }

    void test() {
        service.addCustomer();
        service.addCustomerReturnValue();
        service.addCustomerAround("Test Customer");
        try {
            service.addCustomerThrowException();
        } catch(Exception ex) {
            logger.warn(ex.getMessage(), ex);
        }
    }

    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);

        Program program = new Program(context);
        program.test();
    }
}
