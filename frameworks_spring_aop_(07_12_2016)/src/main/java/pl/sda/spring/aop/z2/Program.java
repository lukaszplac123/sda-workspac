package pl.sda.spring.aop.z2;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import pl.sda.spring.aop.model.carshop.Operator;

public class Program {

    private final Operator operator;

    Program(ApplicationContext context) {
        operator = context.getBean(Operator.class);
    }

    void test() {
        operator.prepareData();
        operator.printData();
    }

    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);

        Program program = new Program(context);
        program.test();
    }
}
