package pl.sda.spring.aop.z5;

import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.sda.spring.aop.model.carshop.CarShop;

import java.util.List;

@Aspect
public class Filter {

    private static final Logger logger = LoggerFactory.getLogger(Filter.class);

    @AfterReturning(value = "execution(* pl.sda.spring.aop.model.carshop.Operator.getShops())", returning = "shops")
    public void filter(List<CarShop> shops) throws Throwable {
        logger.info("Make parameter: {}", shops);
        shops.removeIf(carShop -> !carShop.getName().startsWith("S"));
    }
}
