package pl.sda.spring.aop.z1;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Aspect
public class MyAspect {

    private static final Logger logger = LoggerFactory.getLogger(MyAspect.class);

    @Before("execution(* pl.sda.spring.aop.model.carshop.Operator.p*(..))")
    public void check(JoinPoint joinPoint) {
        logger.debug("Działa! " + joinPoint.getSignature());
    }

}
