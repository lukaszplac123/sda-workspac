package pl.sda.spring.aop.z4;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Aspect
public class Validator {

    private static final Logger logger = LoggerFactory.getLogger(Validator.class);

    @Around("execution(* pl.sda.spring.aop.model.carshop.Car.setMake(..)) && args(make)")
    public void checkMake(ProceedingJoinPoint pjp, String make) throws Throwable {
        logger.info("Make parameter: {}", make);
        pjp.proceed();
    }

    @Around("execution(* pl.sda.spring.aop.model.carshop.Car.setPrice(..)) && args(price)")
    public void checkPrice(ProceedingJoinPoint pjp, double price) throws Throwable {
        logger.info("Price parameter: {}", price);
        double validatedPrice = price > 1000 ? 1000 : price;
        pjp.proceed(new Object[] { validatedPrice });
    }
}
