package pl.sda.spring.aop.z3;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

@Aspect
public class ExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(ExceptionHandler.class);

    @Around("execution(* pl.sda.spring.aop.model..*.*(..))")
    public Object handle(ProceedingJoinPoint pjp) throws Throwable {
        logger.info("Measuring {} method", pjp.getSignature());
        try {
            return pjp.proceed();
        } catch (Throwable throwable) {
            logger.warn(throwable.getMessage(), throwable);
            writeResultToFile(throwable.getMessage());
            throw throwable;
        }
    }

    private void writeResultToFile(String message) {
        File file = new File("./errors.txt");
        try {
            FileWriter fw = new FileWriter(file,true);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(message+"\n");
            bw.close();
        }
        catch (IOException ex) {
            logger.warn(ex.getMessage(), ex);
        }
    }
}
