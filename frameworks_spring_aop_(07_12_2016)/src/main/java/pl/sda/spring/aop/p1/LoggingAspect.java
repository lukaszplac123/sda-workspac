package pl.sda.spring.aop.p1;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Aspect
public class LoggingAspect {

    private static final Logger logger = LoggerFactory.getLogger(LoggingAspect.class);

    @Before("execution(void pl.sda.spring.aop.model.customer.CustomerService.addCustomer(..))")
    public void logBefore(JoinPoint joinPoint) {
        logger.debug("logBefore() is running!");
        logger.debug(joinPoint.getSignature().getName() + " has been intercepted");
    }

    @Around("execution(void *Around(..))")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        logger.debug("around() is running!");
        logger.debug(joinPoint.getSignature().getName() + " has been intercepted");
        return joinPoint.proceed();
    }

    @AfterReturning(value = "execution(String *ReturnValue(..))", returning = "value")
    public void afterReturning(Object value) {
        logger.debug("afterReturning() is running!");
        logger.debug(value + " has been returned");
    }

    @AfterThrowing(value = "execution(void *ThrowException(..))", throwing = "ex")
    public void afterThrowing(JoinPoint joinPoint, Exception ex) {
        logger.debug("afterThrowing() is running!");
        logger.debug(joinPoint.getSignature().getName() + " has been intercepted");
    }

    @Around("@annotation(ExceptionHandler)")
    public Object exceptionHandler(ProceedingJoinPoint pjp) {
        logger.debug("exceptionHandler() is running!");
        logger.debug(pjp.getSignature().getName() + " has been intercepted");

        try {
            return pjp.proceed(pjp.getArgs());
        } catch (Throwable throwable) {
            logger.warn(throwable.getMessage(), throwable);
            throw new RuntimeException(throwable);
        }
    }
}
