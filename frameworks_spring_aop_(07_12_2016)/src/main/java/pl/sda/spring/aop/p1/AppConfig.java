package pl.sda.spring.aop.p1;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import pl.sda.spring.aop.model.customer.CustomerService;

@Configuration
@EnableAspectJAutoProxy
public class AppConfig {

    @Bean
    public CustomerService customerService() {
        return new CustomerService();
    }

    @Bean
    public LoggingAspect loggingAspect() {
        return new LoggingAspect();
    }
}
