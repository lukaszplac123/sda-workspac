package pl.sda.spring.aop.model.carshop;

import java.util.ArrayList;
import java.util.List;

public class CarShop {

    private String name;
    private Owner owner;
    private List<Car> cars = new ArrayList<>();

	public CarShop() {

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Owner getOwner() {
		return owner;
	}

	public void setOwner(Owner owner) {
		this.owner = owner;
	}

	public List<Car> getCars() {
		return cars;
	}

	public void setCars(List<Car> cars) {
		this.cars = cars;
	}
	
	public void addCar(Car car) {
		cars.add(car);
	}

	@Override
	public String toString() {
		return String.format("name: %s, owner: %s", name, owner.toString());
	}
}
