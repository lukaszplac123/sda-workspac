package pl.sda.spring.aop.z5;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import pl.sda.spring.aop.model.carshop.Operator;

public class Program {

    private static final Logger logger = LoggerFactory.getLogger(Program.class);
    private final Operator operator;

    Program(ApplicationContext context) {
        operator = context.getBean(Operator.class);
    }

    void test() {
        operator.prepareData();
        operator.printData();
        logger.info("Filtered: {}", operator.getShops());
    }

    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);

        Program program = new Program(context);
        program.test();
    }
}
