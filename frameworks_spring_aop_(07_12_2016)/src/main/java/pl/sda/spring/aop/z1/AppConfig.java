package pl.sda.spring.aop.z1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import pl.sda.spring.aop.model.carshop.CarShop;
import pl.sda.spring.aop.model.carshop.Operator;
import pl.sda.spring.aop.model.carshop.Owner;
import pl.sda.spring.aop.model.customer.CustomerService;

import java.util.List;

@Configuration
@EnableAspectJAutoProxy
public class AppConfig {

    @Bean("owner1")
    public Owner owner1() {
        Owner owner = new Owner();
        owner.setName("Jan Kowalski");
        owner.setAddress("Krakow");

        return owner;
    }

    @Bean("owner2")
    public Owner owner2() {
        Owner owner = new Owner();
        owner.setName("Adam Nowak");
        owner.setAddress("Rzeszow");

        return owner;
    }

    @Bean("carShop1")
    public CarShop carShop1(@Autowired @Qualifier("owner1") Owner owner){
        CarShop shop = new CarShop();
        shop.setName("Salon samochodowy");
        shop.setOwner(owner);

        return shop;
    }

    @Bean("carShop2")
    public CarShop carShop2(@Autowired @Qualifier("owner2") Owner owner){
        CarShop shop = new CarShop();
        shop.setName("Salon samochodowy");
        shop.setOwner(owner);

        return shop;
    }

    @Bean
    public Operator operator(@Autowired List<CarShop> shops) {
        Operator operator = new Operator();
        operator.setShops(shops);

        return operator;
    }

    @Bean
    public MyAspect myAspect() {
        return new MyAspect();
    }
}
