package pl.sda.spring.aop.z2;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

@Aspect
public class Meter {

    private static final Logger logger = LoggerFactory.getLogger(Meter.class);

    @Around("execution(* pl.sda.spring.aop.model..*.*(..))")
    public Object measure(ProceedingJoinPoint pjp) throws Throwable {
        logger.info("Measuring {} method", pjp.getSignature());
        long startTime = System.nanoTime();
        try {
            return pjp.proceed();
        } catch (Throwable throwable) {
            logger.warn(throwable.getMessage(), throwable);
            throw throwable;
        } finally {
            long stopTime = System.nanoTime();
            long elapsed = stopTime - startTime;
            logger.info("Time elapsed: {} ns", elapsed);
            writeResultToFile(elapsed);
        }
    }

    private void writeResultToFile(long result) {
        File file = new File("./results.txt");
        try {
            FileWriter fw = new FileWriter(file,true);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(String.valueOf(result)+"\n");
            bw.close();
        }
        catch (IOException ex) {
            logger.warn(ex.getMessage(), ex);
        }
    }
}
