package pl.sda.spring.rest.model;

public class ErrorInfo {
    private String errorCode;
    private String message;

    public ErrorInfo(String errorCode, String message) {
        this.errorCode = errorCode;
        this.message = message;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getMessage() {
        return message;
    }
}
