import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class App2 {

	public static void main(String[] args) throws IOException {
		
		String pathToFile = null;
		if (args.length<1) { 
			System.out.println("Podaj sciezke do pliku jako argument porgramu");
		} else {
			FileReader fileReader = null;
			pathToFile = args[0];
			try {
				fileReader = new FileReader(pathToFile);
				int data = 0;
				Map<Character, Integer> map = new HashMap<>();
				
				while((data = fileReader.read()) != -1) {
					char c = Character.toLowerCase((char)data);
					if (Character.isLetter(c)) {
						if (map.containsKey(c)) {
							map.put(c, map.get(c) + 1);
						}
						else {
							map.put(c, 1);
						}
					}
				}
				for(Character c: map.keySet()) {
					System.out.println(c + " - "+ map.get(c));
				}
			}
			finally {
				if (fileReader != null) {
					fileReader.close();
				}
			}
		}

		
	}
}
