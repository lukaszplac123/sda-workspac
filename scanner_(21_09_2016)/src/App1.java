import java.util.Scanner;

public class App1 {

	//aJestPrzyklad
	public static void main(String[] args) {
		System.out.println("Podaj wyraz:");
		Scanner scanner = new Scanner(System.in);
		
		String word = scanner.nextLine();
		int counter = 0;
			
		for (int i = 0; i<word.length(); i++) {
			char c = word.charAt(i);
			if(Character.isUpperCase(c) && i==0) {
				System.out.println("Nie jest camel case");
				System.exit(0);
			} else
			if(Character.isUpperCase(c)) {
				counter++;
			}
		}
		if (!word.isEmpty()) {
			counter++;
		}
		System.out.println(counter);
		
		
		
		scanner.close();
		

	}

}
