package pl.sda.spring.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import pl.sda.spring.auth.CustomUserDetailsService;
import pl.sda.spring.exception.ForbiddenHandler;
import pl.sda.spring.exception.UnauthorizedHandler;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private CustomUserDetailsService userDetailsService;
    @Autowired
    private UnauthorizedHandler unauthorizedHandler;
    @Autowired
    private ForbiddenHandler forbiddenHandler;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        customUserDetailsService(auth);
    }

    private void inMemory(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("user").password("pass").roles("USER")
                .and()
                .withUser("admin").password("pass").roles("ADMIN");
    }

    private void customUserDetailsService(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService);
    }

    private void jdbc(AuthenticationManagerBuilder auth, DataSource dataSource) throws Exception {
        auth.jdbcAuthentication().dataSource(dataSource)
                .usersByUsernameQuery(
                        "select username, password, enabled from USER where username = ?")
                .authoritiesByUsernameQuery(
                        "select username, role from USER_ROLES where username = ?");
    }

    @Override
    public void configure(final WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/");
        web.ignoring().antMatchers("/resources/**");
        web.ignoring().antMatchers("/views/**");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                    .antMatchers("/performLogin").permitAll()
                    .antMatchers("/employee/**").hasAnyRole("USER", "ADMIN")
                    .anyRequest().authenticated()
                .and()
                    .formLogin()
                    .loginProcessingUrl("/performLogin")
                    .defaultSuccessUrl("/success", true)
                .and()
                    .exceptionHandling()
                    .accessDeniedHandler(forbiddenHandler)
                    .authenticationEntryPoint(unauthorizedHandler)
                .and()
                    .csrf().disable();
    }
}
