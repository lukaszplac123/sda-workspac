package pl.sda.spring.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import pl.sda.spring.auth.CustomPermissionEvaluator;

@Configuration
@EnableWebMvc
@ComponentScan(value = {
        "pl.sda.spring.rest",
        "pl.sda.spring.repository",
        "pl.sda.spring.exception",
        "pl.sda.spring.auth"})
@Import(value = { GlobalSecurityConfig.class, SecurityConfig.class, ValidationConfig.class })
public class AppConfig extends WebMvcConfigurerAdapter {

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/resources/**", "/views/**")
                .addResourceLocations("/resources/", "/views/");
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        super.addViewControllers(registry);
        registry.addRedirectViewController("/", "views/index.html");
    }
}
