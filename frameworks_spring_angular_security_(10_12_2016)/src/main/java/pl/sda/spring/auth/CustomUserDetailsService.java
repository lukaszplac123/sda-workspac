package pl.sda.spring.auth;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class CustomUserDetailsService implements UserDetailsService {

    private List<String> usernames = Arrays.asList("user", "admin");
    private List<String> roles = Arrays.asList("ROLE_USER", "ROLE_ADMIN");

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        if(!usernames.contains(username)) {
            throw new UsernameNotFoundException("No user found with username: " + username);
        }

        List<? extends GrantedAuthority> authorities = roles.stream()
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());

        return new User(username, "pass", authorities);
    }
}
