package pl.sda.spring.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import pl.sda.spring.rest.model.ErrorInfo;

@ControllerAdvice
public class ExceptionController {

    private static final Logger logger = LoggerFactory.getLogger(ExceptionController.class);

    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorInfo> badRequest(MethodArgumentNotValidException ex) {
        logger.warn(ex.getMessage(), ex);
        ErrorInfo body = new ErrorInfo("bad.request", ex.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(body);
    }

    @ExceptionHandler(value = AccessDeniedException.class)
    public ResponseEntity<ErrorInfo> accessDenied(AccessDeniedException ex) {
        logger.warn(ex.getMessage(), ex);
        ErrorInfo body = new ErrorInfo("internal.server.error", ex.getMessage());
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(body);
    }

    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<ErrorInfo> exception(Exception ex) {
        logger.warn(ex.getMessage(), ex);
        ErrorInfo body = new ErrorInfo("internal.server.error", ex.getMessage());
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(body);
    }
}
