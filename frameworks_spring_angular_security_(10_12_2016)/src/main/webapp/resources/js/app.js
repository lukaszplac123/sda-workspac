var app = angular.module("app", ["ngRoute"]);

app.factory('authnInterceptor', [
    "$rootScope", "$q", "$location",
    function ($rootScope, $q, $location) {
        return {
            responseError: function (response) {
                if(response.status == 401) {
                    $location.path("/login");
                }
                return $q.reject(response);
            }
        };
    }]);

app.config(["$httpProvider", function ($httpProvider) {
    $httpProvider.interceptors.push([ "$injector", function ($injector) {
        return $injector.get("authnInterceptor");
    }
    ]);
}]);

app.run(['$route', function($route)  {
    $route.reload();
}]);

app.controller("appCtrl", [
    "$scope", "$location", "$routeParams", "$window", "$http",
    function ($scope, $location, $routeParams, $window, $http) {

        $scope.inProgress = false;

        $scope.getAll = function() {
            $http.get("/ui/employee/all")
            .then(function(response) {
                console.log("SUCCESS EMPLOYEE");
            })
            .catch(function(error) {
                console.log("FAIL EMPLOYEE");
            });
        };

        $scope.login = function (credentials) {
            $scope.inProgress = true;
            $http.post("performLogin", serializeData(credentials), {
                "headers" : {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            })
            .then(loginSuccess)
            .catch(loginFailed);
        };

        function serializeData(credentials) {
            return $.param({
                "username" : credentials.email,
                "password" : credentials.password
            });
        }

        function loginSuccess() {
            console.log("SUCCESS");
            $scope.inProgress = false;
            $location.path("/home")
        }

        function loginFailed(error) {
            console.log("FAIL");
            $scope.inProgress = true;
        }

}]);

app.config(["$routeProvider", function ($routeProvider) {
    $routeProvider

        .when("/", {
            redirectTo: "/login"
        })
        .when("/login", {
            templateUrl: "pages/login.view.html",
            controller: "appCtrl"
        })
        .when("/home", {
            templateUrl: "pages/home.view.html",
            controller: "appCtrl"
        })
        // Not found page
        .otherwise({
            templateUrl: "views/core/navigation/not-found.view.html"
        });
}]);


