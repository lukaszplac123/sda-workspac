<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>HelloWorld page</title>

    <!-- JQuery -->
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"
            integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
            crossorigin="anonymous"></script>

    <!-- Bootstrap -->
    <spring:url value="/resources/css/bootstrap.min.css" var="bootstrapCss" />
    <spring:url value="/resources/css/bootstrap-theme.min.css" var="bootstrapThemeCss" />
    <spring:url value="/resources/css/bootstrap-theme.min.css" var="bootstrapJs" />
    <link href="${bootstrapCss}" rel="stylesheet" />
    <link href="${bootstrapThemeCss}" rel="stylesheet" />
    <link href="${bootstrapJs}" rel="stylesheet" />
</head>
<body>
    <div class="container">
        <div>
            Greeting : ${greeting}
        </div>
    </div>
</body>
</html>