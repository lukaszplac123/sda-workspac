package pl.sdacademy.lukasz.lib.test;


import java.io.IOException;

import com.fasterxml.jackson.core.JsonProcessingException;

import pl.sdacademy.lukasz.lib.data.Student;
import pl.sdacademy.lukasz.lib.json_data_reader.GetRequest;
import pl.sdacademy.lukasz.lib.json_data_reader.PutRequest;
import pl.sdacademy.lukasz.lib.json_data_reader.Request;

public class Main {

	public static void main(String[] args){
		GetRequest getRequest = new GetRequest("http://rest-showcase.cloudhub.io/api/students");
		getRequest.getAllStudentsRequest();
		System.out.println(getRequest.toString());
		try {
			getRequest.writeToCSV();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		PutRequest putRequest = new PutRequest();
		putRequest.setURL("http://rest-showcase.cloudhub.io/api/students");
		try {
			putRequest.putNewStudentRequest(new Student("luk20","luk2","luk@luk.pl","pilka"), "luk10");
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
