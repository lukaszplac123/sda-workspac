package pl.sdacademy.lukasz.lib.json_data_reader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import pl.sdacademy.lukasz.lib.data.Student;


//String url = "http://rest-showcase.cloudhub.io/api/students";
public class GetRequest extends Request {

	
	public GetRequest() {
		super();
	}

	public GetRequest(String url) {
		super(url);
	}


	public List<Student> getAllStudentsRequest() {
        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(this.url);
        StringBuffer response = new StringBuffer();
        try {
            HttpResponse httpResponse = client.execute(request);
            logger.info("Response Code : " + httpResponse.getStatusLine().getStatusCode());
            BufferedReader reader = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
            ObjectMapper mapper = new ObjectMapper();
            this.studentsList = mapper.readValue(reader, new TypeReference<List<Student>>() {});
        }catch (IOException e) {
            logger.error("Error while consuming: ", e);
        }
        return this.studentsList;
    	}
	}
