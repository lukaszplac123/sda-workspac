package pl.sdacademy.lukasz.lib.json_data_reader;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import pl.sdacademy.lukasz.lib.data.Student;

public class Request {
	
	final static Logger logger = Logger.getLogger(Request.class);
	protected String url;
	protected List<Student> studentsList;

	public List<Student> getStudentsList() {
		return studentsList;
	}

	public String getURL() {
		return url;
	}
	
	public void setURL(String url) {
		this.url = url;
	}
	
	public Request() {
		this.url = null;
		this.studentsList = new ArrayList<>();
	}

	public Request(String URL) {
		this();
		this.url = URL;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		studentsList.forEach((student) -> builder.append(student.toString()));
		return studentsList.toString();
	}
	
	public void writeToCSV() throws IOException{
		File csvFile = new File("students.csv");
	    if (!csvFile.exists()){
	        	csvFile = new File("students.csv");
	    }
	    StringBuilder builder = new StringBuilder();
	    BufferedWriter writer = new BufferedWriter(new FileWriter(csvFile));
	    studentsList.forEach((student) -> builder.append(student.toString()));
	    writer.write(builder.toString()); 
	    writer.close();
	}
}
