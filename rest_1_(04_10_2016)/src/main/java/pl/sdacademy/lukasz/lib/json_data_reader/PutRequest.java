package pl.sdacademy.lukasz.lib.json_data_reader;

import java.io.IOException;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import pl.sdacademy.lukasz.lib.data.Student;


public class PutRequest extends Request{


public PutRequest() {
		super();
}

public PutRequest(String URL) {
		super(URL);
}
	
public String putNewStudentRequest (Student student, String id) throws JsonProcessingException{
        ObjectMapper mapper = new ObjectMapper();
        String jsonRequest = mapper.writeValueAsString(student);
    	HttpClient client = HttpClientBuilder.create().build();
        HttpPut put = new HttpPut(url+"/"+id);
        StringBuffer response = new StringBuffer();
        try {
        	StringEntity strEntity = new StringEntity(jsonRequest);
        	put.setHeader("content-type", "application/json");
        	put.setEntity(strEntity);
        	HttpResponse httpResponse = client.execute(put);
        	for(Header header : httpResponse.getAllHeaders()){
        			response.append(header.getName() + " : "+ header.getValue());
        	}
        	response.append(httpResponse.getStatusLine());
        }catch (IOException e) {
            logger.error("Error while consuming: ", e);
        } 
        return response.toString();
   }
}
