package pl.sdacademy.lukasz.lib.json_data_reader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import pl.sdacademy.lukasz.lib.data.Student;

public class PostRequest extends Request{

	
public PostRequest() {
		super();

}

public PostRequest(String URL) {
		super(URL);
}


public String postNewStudentRequest(Student student) throws JsonProcessingException {
	    HttpClient client = HttpClientBuilder.create().build();
	    HttpPost httpPost = new HttpPost(this.url);
	    StringBuffer response = new StringBuffer();
	    ObjectMapper mapper = new ObjectMapper();
	    String jsonRequest = mapper.writeValueAsString(student);
    
	    try {
   
	    	StringEntity strEntity = new StringEntity(jsonRequest);
	    	httpPost.setHeader("content-type", "application/json");
	    	httpPost.setEntity(strEntity);
	    	HttpResponse httpResponse = client.execute(httpPost);
	    	for(Header header : httpResponse.getAllHeaders()){
    			response.append(header.getName() + " : "+ header.getValue());
        }
    	response.append(httpResponse.getStatusLine());
    } catch (IOException e) {
        logger.error("Error while creating request: ", e);
    }
    return response.toString();
}
}

