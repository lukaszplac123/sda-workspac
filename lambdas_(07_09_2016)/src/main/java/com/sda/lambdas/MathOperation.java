package com.sda.lambdas;


public interface MathOperation {

    int operation(int a, int b);
}
