package com.sda.lambdas;

import java.util.function.Function;

public class DemoMathOperation {
	public static void main(String[] args){
		
	    MathOperation adding = (a,b) -> a+b;
		MathOperation multiply = (a,b) -> a*b;
		MathOperation substract = (a,b) -> a-b;
		MathOperation divide = (a,b) -> a/b;
		
		System.out.println(adding.operation(5,2));
		System.out.println(multiply.operation(5,2));
		System.out.println(substract.operation(5,2));
		System.out.println(divide.operation(6,2));
	}

}
