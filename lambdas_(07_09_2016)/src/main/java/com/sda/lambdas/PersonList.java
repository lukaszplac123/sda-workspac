package com.sda.lambdas;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;

public class PersonList {

		public static void main(String[] args){
		
			
			Set<String> set1 = Stream.of("Java", "COBOL", "HTML").collect(Collectors.toSet());
			Set<String> set2 = Stream.of("Java", "LOGO", "C++", "C").collect(Collectors.toSet());
			Set<String> set3 = Stream.of("HTML", "VisualBasic", "C++", "Pascal").collect(Collectors.toSet());
			
			SoftwareDeveloper p1 = new SoftwareDeveloper ("Jacek", "P", 18, set1);
			SoftwareDeveloper p2 = new SoftwareDeveloper ("Marek", "S", 54, set2);
			SoftwareDeveloper p3 = new SoftwareDeveloper  ("Marian", "K", 21, set1);
			SoftwareDeveloper p4 = new SoftwareDeveloper  ("Wladek", "F", 16, set2);
			SoftwareDeveloper p5 = new SoftwareDeveloper  ("Roman", "E", 73, set1);
			SoftwareDeveloper p6 = new SoftwareDeveloper  ("Gienek", "W", 34, set3);
			SoftwareDeveloper p7 = new SoftwareDeveloper  ("Jozek", "C", 49, set3);
			
			List<Person> list = new ArrayList<Person>();
			list.add(p1);
			list.add(p2);
			list.add(p3);
			list.add(p4);
			list.add(p5);
			list.add(p6);
			list.add(p7);
			
			Comparator<Person> comp1 = (pe1, pe2) -> Integer.compare(pe1.getAge(), pe2.getAge());
			Collections.sort(list, comp1);
			
			list.forEach(System.out::println);
			
			Comparator<Person> comp2 = Collections.reverseOrder(comp1);
			Collections.sort(list, comp2);
			System.out.println("-------------");
			
			list.forEach(System.out::println);
			
			List<SoftwareDeveloper> softwareDevelopers = Arrays.asList(p1,p2,p3,p4,p5,p5,p7);
			
			softwareDevelopers.stream().map(SoftwareDeveloper::getKnownLanguages).flatMap(Set::stream).forEach(System.out::println);
			
		}

	
}
