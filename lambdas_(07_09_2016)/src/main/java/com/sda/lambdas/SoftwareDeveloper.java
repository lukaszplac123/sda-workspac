package com.sda.lambdas;

import java.util.HashSet;
import java.util.Set;


public class SoftwareDeveloper extends Person {

	private Set<String> knownLanguages =  new HashSet<String>();
	
	public SoftwareDeveloper(String name, String surename, int age, Set<String> set) {
		super(name, surename, age);
		this.setKnownLanguages(set);
		
		
	}

	public Set<String> getKnownLanguages() {
		return knownLanguages;
	}

	public void setKnownLanguages(Set<String> knownLanguages) {
		this.knownLanguages = knownLanguages;
	}
	
	
	
}
