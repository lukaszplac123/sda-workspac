package com.sda.lambdas;



public class Person {

protected String name;
protected String surename;
protected int age;
	
	public Person(String name, String surename, int age){
		this.setName(name);
		this.setSurename(surename);
		this.setAge(age);
	}

	public String getSurename() {
		return surename;
	}

	public void setSurename(String surename) {
		this.surename = surename;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String toString(){
		return (this.getName() + " " + this.getSurename() + " " + this.getAge() );
	}
}
