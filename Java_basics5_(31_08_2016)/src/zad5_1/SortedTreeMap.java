package zad5_1;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Random;
import java.util.TreeMap;

public class SortedTreeMap {

	private List<Integer> listOfIntegers;
	
	public static void main(String[] args) {
		
		SortedTreeMap program = new SortedTreeMap();
	
		System.out.print(program.listOfIntegers);

		System.out.println();
		
		System.out.print(program.sort(program.listOfIntegers));
		
	}
	
	public SortedTreeMap() {
		this.listOfIntegers = new ArrayList<>();
		for (int i = 0; i < 10; i++){
			listOfIntegers.add(new Random().nextInt(5)+1);
		}
		
		
	}
	
	public List<Integer> sort(List<Integer> entryList) {
		TreeMap<Integer, Integer> tempMap = new TreeMap<Integer, Integer>();
		for (Integer number: entryList){
		    if (tempMap.containsKey(number)){
		    	Integer temp = tempMap.get(number);
		    	tempMap.put(number, ++temp);
		    }
		    else tempMap.put(number, 1);
		}
		
		List<Integer> tempList = new ArrayList<Integer>();
		for (Entry<Integer, Integer> keyEntry: tempMap.entrySet()){
			for (int i = 0; i < keyEntry.getValue(); i++){
				tempList.add(keyEntry.getKey());
			}
		}
		return tempList;
	
	}
}
