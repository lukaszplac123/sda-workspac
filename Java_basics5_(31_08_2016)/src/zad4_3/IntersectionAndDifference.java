package zad4_3;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class IntersectionAndDifference {

	private Set<Integer> set1;
	private Set<Integer> set2;
	
	public static void main (String[] args){
		
		List<Integer> list1 = new ArrayList<Integer>();
		List<Integer> list2 = new ArrayList<Integer>();
		list1.add(1);
		list1.add(2);
		list1.add(3);
		list1.add(8);
		list1.add(4);
		list1.add(5);
		
		list2.add(1);
		list2.add(2);
		list2.add(3);
		list2.add(4);
		list2.add(7);
		list2.add(8);
		
		IntersectionAndDifference program = new IntersectionAndDifference(list1, list2);
		
		System.out.print("List 1:");
		System.out.print(list1);
		System.out.println();
		System.out.print("List 2:");
		System.out.print(list2);
		
		System.out.println();
		
		System.out.print("Intersection:");
		System.out.print(program.intersectionOptimized(program.set1, program.set2));
		System.out.println();
		System.out.print("Difference:");
		System.out.print(program.differenceOptimized(program.set1, program.set2));
	}
	
	public IntersectionAndDifference(List<Integer> tab1, List<Integer> tab2) {
		
		this.set1 = new HashSet<Integer>(tab1);
		this.set2 = new HashSet<Integer>(tab2);
	}
	
	public Set<Integer> intersectionOptimized(Set<Integer> set1, Set<Integer> set2){
		
		Set<Integer> temp = new HashSet<Integer>();
		
		for(Integer number: set1){
			if (set2.contains(number)) temp.add(number);	
		}

		return temp;
	}
	
	public Set<Integer> differenceOptimized(Set<Integer> set1, Set<Integer> set2){
		
		Set<Integer> temp = new HashSet<Integer>();
		
		for(Integer number1: set1){
			if (!set2.contains(number1)) temp.add(number1);
		}	
		
		for(Integer number2: set2){
			if (!set1.contains(number2)) temp.add(number2);
		}	
		return temp;
		
	}
}
