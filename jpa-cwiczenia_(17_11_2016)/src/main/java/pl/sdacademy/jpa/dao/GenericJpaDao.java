package pl.sdacademy.jpa.dao;

public interface GenericJpaDao<T, PK> {
    T create(T entity);
    T read(PK id);
    T update(T entity);
    void delete(T entity);
}
