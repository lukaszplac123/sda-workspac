package pl.sdacademy.jpa.dao;

import java.lang.reflect.ParameterizedType;

import javax.persistence.EntityManager;


public class GenericJpaDaoImpl<T, PK> implements GenericJpaDao<T, PK> {

    protected final EntityManager em;
    protected final Class<T> entityClass;
    
    @SuppressWarnings("unchecked")
    protected GenericJpaDaoImpl(EntityManager entityManager) {
        ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
        this.entityClass = (Class<T>) genericSuperclass.getActualTypeArguments()[0];
        this.em = entityManager;
    }

    // TODO: 00c. Dodac implementacje generycznego dao za pomoca metod EntityManager
    
    @Override
    public T create(T entity) {
    	em.persist(entity);
        return entity;
    }

    @Override
    public T read(PK id) {
        return em.find(entityClass, id);
    }

    @Override
    public T update(T entity) {
        return em.merge(entity);
    }

    @Override
    public void delete(T entity) {
    	em.remove(entity);
    }

   

}
