package pl.sdacademy.jpa.dao;

import javax.persistence.EntityManager;

import pl.sdacademy.jpa.domain.Osoba;

public class OsobyDao extends GenericJpaDaoImpl<Osoba, Integer> {

	public OsobyDao(EntityManager entityManager) {
		super(entityManager);
	}

}
