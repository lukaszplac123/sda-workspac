package pl.sdacademy.jpa.dao;

import javax.persistence.EntityManager;

import pl.sdacademy.jpa.domain.Losowanie;

public class LosowanieDao extends GenericJpaDaoImpl<Losowanie, Integer>{

	public LosowanieDao(EntityManager entityManager) {
		super(entityManager);
	}

}
