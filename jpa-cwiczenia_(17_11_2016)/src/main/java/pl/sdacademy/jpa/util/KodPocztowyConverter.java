package pl.sdacademy.jpa.util;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import pl.sdacademy.jpa.domain.KodPocztowy;

@Converter
public class KodPocztowyConverter implements AttributeConverter<KodPocztowy, String> {

    @Override
    public String convertToDatabaseColumn(KodPocztowy kod) {
    	String converted = kod.toString().replace("_", "-");
        return converted.toString();
    }

    @Override
    public KodPocztowy convertToEntityAttribute(String wartosc) {
    	String converted = wartosc.replace("-", "_");
    	KodPocztowy nowyKod = KodPocztowy.valueOf(converted);
        return nowyKod;
    }
    
}
