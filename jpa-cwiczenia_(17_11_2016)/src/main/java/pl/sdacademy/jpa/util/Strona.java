package pl.sdacademy.jpa.util;

import java.util.List;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@RequiredArgsConstructor
@Getter
@ToString
public class Strona<T> {
    private final Integer numer;
    private final Integer rozmiar;
    private final Long calkowitaLiczbaRekordow;
    private final List<T> rekordy;
}