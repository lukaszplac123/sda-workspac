package pl.sdacademy.jpa.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "gry")
public class Gra {

	@Id
	private Integer id;
	private String nazwa;
	
	public Integer getId() {
		return id;
	}

	public String getNazwa() {
		return nazwa;
	}
		
}
