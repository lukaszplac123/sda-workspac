package pl.sdacademy.jpa.domain;

import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.Table;

@Embeddable
public class WylosowanaLiczba {
	
	Integer liczba;
	
	public Integer getLiczba() {
		return liczba;
	}
	
}
