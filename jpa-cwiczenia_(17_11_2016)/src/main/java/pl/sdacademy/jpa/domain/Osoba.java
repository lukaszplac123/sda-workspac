package pl.sdacademy.jpa.domain;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table (name = "osoby")
public class Osoba{

	@Id
	@SequenceGenerator(allocationSize = 1, name = "my_seq", sequenceName = "osoby_id_seq")
	@GeneratedValue(generator = "my_seq", strategy = GenerationType.SEQUENCE)
	private Integer id;
	
	private String imie;
	private String nazwisko;
	
	@OneToOne (mappedBy="osoba")
	private StatystykaGier statystykaGier;
	
	@OneToMany(mappedBy="osoba")
	private List<Adres> adresy;
	
	public List<Adres> getAdresy() {
		return adresy;
	}

	public void setAdresy(List<Adres> adresy) {
		this.adresy = adresy;
	}

	public StatystykaGier getStatystykaGier() {
		return statystykaGier;
	}

	public Osoba(){
		
	}
	
	public Osoba (String imie, String nazwisko){
		this.imie = imie;
		this.nazwisko = nazwisko;
	}
	public Integer getId() {
		return id;
	}
	public String getImie() {
		return imie;
	}
	public String getNazwisko() {
		return nazwisko;
	}
	
}
