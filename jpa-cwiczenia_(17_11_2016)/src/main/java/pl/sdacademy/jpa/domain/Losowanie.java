package pl.sdacademy.jpa.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OrderColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "losowanie")
public class Losowanie {
	
	@Id
	Integer id;
	
	Integer numer;
	
	String nazwa;
	
	@Temporal(TemporalType.TIMESTAMP)
	Date data;
	
	public Integer getNumer() {
		return numer;
	}
	public String getNazwa() {
		return nazwa;
	}
	public Date getData() {
		return data;
	}
	
	@ElementCollection
	@CollectionTable (name = "wylosowane_liczby", 
					  joinColumns = @JoinColumn(name = "losowanie_id"))
	@OrderColumn(name = "numer")
	private List<WylosowanaLiczba> liczby;

	public List<WylosowanaLiczba> getLiczby() {
		return liczby;
	}

}
