package pl.sdacademy.jpa.domain;


public class KodPocztowy {
    private String wartosc;
    
    private KodPocztowy(String kod) {
        this.wartosc = kod;
    }
    
    public static KodPocztowy valueOf(String kod) {
        if (kod.contains("_")) return new KodPocztowy(kod);
        	else return new KodPocztowy("Niprawidlowy kod");
    }
    
    @Override
    public String toString() {
        return wartosc;
    }
}
