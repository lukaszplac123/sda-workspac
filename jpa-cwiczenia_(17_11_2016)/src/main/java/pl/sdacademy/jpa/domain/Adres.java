package pl.sdacademy.jpa.domain;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import pl.sdacademy.jpa.util.KodPocztowyConverter;

@Entity
@Table (name = "adresy")
public class Adres {
	
	  @Id
	  private Integer id;
	  
	  @Enumerated(EnumType.STRING)
	  private Type typ;
	  
	  private String ulica;
	  private String nr_domu;
	  private String nr_mieszkania;
	  
	  @Convert(converter = KodPocztowyConverter.class)
	  private KodPocztowy kod_pocztowy;
	  
	  private String miasto;
	  
	  
	@ManyToOne
	@JoinColumn(name="osoba_id")
	private Osoba osoba;
		
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public Type getTyp() {
		return typ;
	}
	public void setTyp(Type typ) {
		this.typ = typ;
	}
	public String getUlica() {
		return ulica;
	}
	public void setUlica(String ulica) {
		this.ulica = ulica;
	}
	public String getNr_domu() {
		return nr_domu;
	}
	public void setNr_domu(String nr_domu) {
		this.nr_domu = nr_domu;
	}
	public String getNr_mieszkania() {
		return nr_mieszkania;
	}
	public void setNr_mieszkania(String nr_mieszkania) {
		this.nr_mieszkania = nr_mieszkania;
	}
	public KodPocztowy getKod_pocztowy() {
		return kod_pocztowy;
	}
	public void setKod_pocztowy(KodPocztowy kod_pocztowy) {
		this.kod_pocztowy = kod_pocztowy;
	}
	public String getMiasto() {
		return miasto;
	}
	public void setMiasto(String miasto) {
		this.miasto = miasto;
	}
	  
}
