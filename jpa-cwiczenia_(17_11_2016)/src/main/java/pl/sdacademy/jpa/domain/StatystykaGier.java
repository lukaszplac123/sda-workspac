package pl.sdacademy.jpa.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table (name = "statystyki_gier")
public class StatystykaGier {
	
	@Id
	private Integer osoba_id;
	
	private Integer liczba_gier;
	private Integer liczba_wygranych;
	
	private Double kwota_wygranych;
	
	public Integer getOsoba_id() {
		return osoba_id;
	}

	public Integer getLiczba_gier() {
		return liczba_gier;
	}

	public Integer getLiczba_wygranych() {
		return liczba_wygranych;
	}

	public Double getKwota_wygranych() {
		return kwota_wygranych;
	}

	public Osoba getOsoba() {
		return osoba;
	}

	public Date getOstatniaWygrana() {
		return ostatnia_wygrana;
	}

	@MapsId
	@OneToOne
	@JoinColumn(name="osoba_id")
	private Osoba osoba;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date ostatnia_wygrana;
	
}
