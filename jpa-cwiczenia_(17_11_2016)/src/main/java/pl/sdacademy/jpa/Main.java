package pl.sdacademy.jpa;

import javax.persistence.CollectionTable;
import javax.persistence.ElementCollection;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Persistence;

import pl.sdacademy.jpa.dao.GraDao;
import pl.sdacademy.jpa.dao.LosowanieDao;
import pl.sdacademy.jpa.dao.OsobyDao;
import pl.sdacademy.jpa.domain.Gra;
import pl.sdacademy.jpa.domain.Losowanie;
import pl.sdacademy.jpa.domain.Osoba;

public class Main 
{
    private static EntityManagerFactory emf;
    private static EntityManager em;
    
    public static void main( String[] args )
    {
        // TODO: 00a. Skonfigurowac persistance.xml - url, user, pass, driver, dialect
        // TODO: 00a.bonus: Dodac do persistance.xml connection pool poprzez HikariCP
        // TODO: 00b. Wydzielic orm.xml do przechowywania zapytan

        start();

        cwiczenie01_dao_i_podstawowe_mapowanie();
        cwiczenie02_mapowanie_kolekcji_elementow();
        cwiczenie03_zapis_i_podstawy_transakcji();
        cwiczenie04_mapowanie_relacji();
        cwiczenie05_zapytania_i_grafy_relacji();
        cwiczenia06_zaawansowane_mapowania_i_locking();
        
        cleanup();
    }

    private static void start() {
        // TODO: 00d. Utworzyć instancje EntityManagerFactory i EntityManager
        
        emf = Persistence.createEntityManagerFactory("sda-pu");
        em = emf.createEntityManager();
    }
    
    private static void cleanup() {
        // TODO: 00e. Zadbac o odpowiednie zamkniecie zasobow EntityManagerFactory i EntityManager
        
        if (em.isOpen()) {
            em.close();
        }
        if (emf.isOpen()) {
            emf.close();
        }
    }

    private static void cwiczenie01_dao_i_podstawowe_mapowanie() {
        // TODO: 01a. Dodac mapowanie dla tabeli `gry`
        // TODO: 01b. Dodac repozytorium dla encji Gry 
        // TODO: 01c. Pobrac i wyswietlic gre o id = 1
        // Przydatne: @Entity, @Table, @Id  	
    	GraDao graDao = new GraDao(em);
    	Gra gra = graDao.read(1);
    	System.out.println(gra.getNazwa());
        
    }
    
    private static void cwiczenie02_mapowanie_kolekcji_elementow() {
        // TODO: 02a. Dodac mapowanie dla tabeli `losowanie`
        // TODO: 02b. Dodac repozytorium dao: LosowanieDao
        // TODO: 02c. Pobrac i wyswietlic losowanie o id = 1
        // Przydatne: @Temporal
        LosowanieDao losowanieDao = new LosowanieDao(em);
        Losowanie losowanie = losowanieDao.read(1);
        System.out.println(losowanie.getNazwa());
    	
        // TODO: 02d. Dodac mapowanie dla tabeli `wylosowane_liczby` 
        // Przydatne: @Embeddable, @ElementCollection, @JoinTable, @OrderColumn
        losowanie.getLiczby().forEach(item -> {
        		System.out.print(item.getLiczba()+"|");
        });
        System.out.println();
    	
        
    }

    private static void cwiczenie03_zapis_i_podstawy_transakcji() {
        // TODO: 03a. Dodać mapowanie dla tabeli `osoby`
        // TODO: 03b. Dodać repozytrium dao: OsobaDao
        // TODO: 03c. Pobrać i wyświetlić osobę o id = 1
    	OsobyDao osobyDao = new OsobyDao(em);
    	Osoba osoba = osobyDao.read(2);
    	System.out.println(osoba.getNazwisko());
    	
        // TODO: 03d. Stworzyć i zapisać nową encję Osoba i zapisać za pomocą OsobaDao::create
        // Przydatne: @SequenceGenerator, @GeneratedValue
   
    	Osoba nowaOsoba = new Osoba("Jan", "Najlepszy");
        EntityTransaction tx = em.getTransaction();
        tx.begin();
        	osobyDao.create(nowaOsoba);
        tx.commit();
    }
    
    private static void cwiczenie04_mapowanie_relacji() {
        // TODO: 04a. Dodać mapowanie dla tabeli `statystyki_gier`
        // TODO: 04b. Dodać relację Osoba->StatystykiGier
        // TODO: 04c. Wyswietlić statystyki osoby o id = 1
        // Przydatne: @OneToOne, @Column, @MapsId, @JoinColumn
    	OsobyDao osobyDao = new OsobyDao(em);
    	Osoba osoba = osobyDao.read(1);
    	System.out.println(osoba.getStatystykaGier().getKwota_wygranych());
    	
        // TODO: 04d. Dodać mapowanie dla tabeli `adresy`
        // TODO: 04e. Dodać relację Osoba->Adres
        // TODO: 04f. Wyswietlić adresy osoby o id = 1
        // TODO: 04g. Dodać konwerter do mapowania kodu pocztowego dla adresów
        // Przydatne: @OneToMany, @Enumerated, @Converter
        osoba.getAdresy().forEach(item->{
        		System.out.println(item.getKod_pocztowy());
        });
    
        // TODO: 04h. Dodać relację Osoba->Gra
        // TODO: 04i. Wyswietlić gry osoby o id = 1
        // Przydatne: @OneToMany, @Enumerated, @Converter
        
        // TODO: 04j. Zapisać pełny obiekt Osoba wraz z dwoma adresami, statystykami gier oraz dwoma grami
        
    }
    
    private static void cwiczenie05_zapytania_i_grafy_relacji() {
        // TODO: 05a. Dodać do OsobaDao metodę znajdzOsobyWgImienia i obsłużyć zapytanie jpql pobierajace osobe o zadanym imieniu
        // TODO: 05b. Wyświetlić wyniki zapytania dla imienia = Stefan
        // Przydatne: EntityManager::createQuery, Query::setParameter, Query::getResultList
        // TODO: zaimplementowac zapytanie jpql pobierajace liste osob - kontrola liczby wynikow zapytania setFirstResut/setMaxResults (obserwowac jak przeklada sie na zapytanie)
        
        // TODO: 05a. Dodać do OsobaDao metodę znajdzOsobyWgImienia z dodatkowymi parametrami: numerem strony i rozmiarem strony
        // TODO: 05d. Zwrócić reszultat opakowując go w klasę Strona
        // TODO: 05e. Wyświetlić wyniki zapytania dla imienia = Stefan, strona = 2, rozmiar = 2
        // Przydatne: Query::setFirstResult, Query::setMaxResults, Query::getSingleResult
        
        // TODO: 05f. Dodać do OsobaDao metodę znajdzOsoby i obsłużyć zapytanie pobierające osoby o imieniu lub nazwisku (lub obydwu parametrach)
        // TODO: 05f.bonus Wykorzystać generowanie metamodelu i klasy Osoba_ przy tworzeniu zapytania
        // TODO: 05g. Zwrócić rezultat wyszukiwania osób o określonym imieniu lub określonym nazwisku 
        // Przydatne: QueryBuilder
        
        // TODO: 05h. Zdefiniować w klasie Osoba nazwane zapytanie: Osoba.znajdzWgIdZAdresami (dołączyć relację)
        // TODO: 05i. Dodać do OsobaDao metodę znajdzWgIdZAdresami pobierającą osobę dla zadanego id używając nazwanego zapytania
        // TODO: 05j. Wyświetlić adresy osoby o id = 1, zwracając uwagę czy nie są generowane dodatkowe zapytania (problem N + 1)
        // Przydatne: @NamedQuery, JOIN FETCH
        
        // TODO: 05k. Zdefiniować w klasie Osoba nazwany graf Osoba.zGrami
        // TODO: 05l. Dodać do OsobaDao metodę znajdzWgIdZGrami pobierającą osobę dla zadanego id używając jpql oraz nazwanego grafu
        // TODO: 05m. Wyświetlić gry osoby o id = 1, zwracając uwagę czy nie są generowane dodatkowe zapytania (problem N + 1)
        // Przydatne: @NamedEntityGraph, EntityManager::getEntityGraph, Query::setHint ("javax.persistance.fetchgraph")
        
    }

    private static void cwiczenia06_zaawansowane_mapowania_i_locking() {
        // TODO: 06a. Dodać mapowanie dla tabeli `numery_losowan` 
        // Przydatne: @EmbeddedId, @Version
        // TODO: 06b. Dodać repozytorium dao NumerLosowaniaDao 
        // TODO: 06c. Stworzyć metodę do pobrania ostaniego numeru losowania dla zadanej gry i roku 
        // TODO: 06d. Dodac lokowanie wiersza podczas zapytania na numer losowania (przetestować różne wartości)
        // Przydatne: LockModeType
        // TODO: 06e. Zaimplementować generowanie wyników nowego losowania (pobierając i zwiekszając NumerLosowania)
        //       dbajac o odpowiedni format nazwy losowania: GRA#numer#rok) - wszystko powinno znaleźć się w metodzie MaszynaLotto::wylosuj
        // TODO: 06f: Wyświetlić wynik losowania
        // Przydatne
        
    }

}
