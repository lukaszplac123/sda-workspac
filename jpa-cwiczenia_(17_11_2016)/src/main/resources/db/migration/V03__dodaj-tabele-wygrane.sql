create table wygrane (
	id serial primary key,
	osoba_id integer not null,
	typ varchar(10) not null,
	kwota decimal(10,2) not null,
	waluta varchar(5) not null default 'PLN',
	odebrana boolean not null default true,
	data_odebrania timestamp,
	constraint fk_wygrane_osoba_id foreign key (osoba_id) references osoby(id)
);

