create table osoby (
	id serial primary key,
	imie varchar(20) not null,
	nazwisko varchar(30) not null
);

create table adresy (
	id serial primary key,
	osoba_id integer not null,
	typ varchar(20) not null,
	ulica varchar(30) not null,
	nr_domu varchar(10) not null,
	nr_mieszkania varchar(10),
	kod_pocztowy varchar(10) not null,
	miasto varchar(30) not null,
	constraint fk_adresy_osoba_id foreign key (osoba_id) references osoby(id) on delete cascade,
	constraint uq_adresy_osoba_id_typ unique (osoba_id, typ)
);

create table statystyki_gier (
	osoba_id integer primary key,
	liczba_gier integer not null default 0,
	liczba_wygranych integer not null default 0,
	kwota_wygranych decimal(10,2) not null default 0,
	ostatnia_wygrana timestamp,
	constraint fk_statystyki_gier_osoba_id foreign key (osoba_id) references osoby(id)
);

insert into osoby (imie, nazwisko) values ('Stefan', 'Pierwszy');

insert into adresy (osoba_id, typ, ulica, nr_domu, nr_mieszkania, kod_pocztowy, miasto)
     values (1, 'DOMOWY', 'Zdrowa', '14', '3a', '61-012', 'Draków');
insert into adresy (osoba_id, typ, ulica, nr_domu, nr_mieszkania, kod_pocztowy, miasto)
     values (1, 'KORESPONDENCYJNY', 'Czysta', '7', null, '63-744', 'Jelonki');

insert into statystyki_gier (osoba_id, liczba_gier, liczba_wygranych, kwota_wygranych, ostatnia_wygrana)
     values (1, 2, 3, 12513.52, '2016-03-31T13:41:52.2512Z');
     
insert into osoby (imie, nazwisko) values ('Arnold', 'Godny');
insert into osoby (imie, nazwisko) values ('Alfred', 'Godny');
insert into osoby (imie, nazwisko) values ('Stefan', 'Drugi');
insert into osoby (imie, nazwisko) values ('Stefan', 'Trzeci');
insert into osoby (imie, nazwisko) values ('Stefan', 'Czwarty');