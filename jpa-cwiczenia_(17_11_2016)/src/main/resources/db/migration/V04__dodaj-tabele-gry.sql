create table gry (
	id serial primary key,
	nazwa varchar(20),
	constraint uq_gry_nazwa unique (nazwa)
);

insert into gry(id, nazwa) values (1, 'Lotto');
insert into gry(id, nazwa) values (2, 'Multi Multi');

create table gry_osob (
	gra_id integer not null,
	osoba_id integer not null,
	constraint uq_gry_osob_gra_osoba unique (gra_id, osoba_id),
	constraint fk_gry_osob_osoba_id foreign key (osoba_id) references osoby(id),
	constraint fk_gry_osob_gra_id foreign key (gra_id) references gry(id)
);

insert into gry_osob(gra_id, osoba_id) values (1, 1);
insert into gry_osob(gra_id, osoba_id) values (2, 1);

create table numery_losowan (
	gra_id integer not null,
	rok smallint not null,
	ostatni_numer_losowania integer not null default 1,
	wersja smallint not null,
	constraint pk_numery_losowan_gra_id_rok primary key (gra_id, rok)
);

insert into numery_losowan(gra_id, rok, ostatni_numer_losowania, wersja) values (1, 2016, 2, 3);