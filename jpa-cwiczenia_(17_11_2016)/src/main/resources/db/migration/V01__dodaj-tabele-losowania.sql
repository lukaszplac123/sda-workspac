create table losowanie (
	id serial primary key,
	numer smallint not null,
	nazwa varchar(20) not null,
	data timestamp not null default current_timestamp,
	constraint uq_losowanie_nazwa unique (nazwa)
);

insert into losowanie(nazwa, numer) values ('LOTTO#001/2016', 1);
insert into losowanie(nazwa, numer) values ('LOTTO#002/2016', 2);

create table wylosowane_liczby (
	losowanie_id integer not null,
	numer smallint not null,
	liczba smallint not null,
	constraint pk_wylosowane_liczby_losowanie_id_numer primary key (losowanie_id, numer),
	constraint uk_wylosowane_liczby_losowanie_id_liczba unique (losowanie_id, liczba)
);

insert into wylosowane_liczby (losowanie_id, numer, liczba) values (1, 0, 13);
insert into wylosowane_liczby (losowanie_id, numer, liczba) values (1, 1, 5);
insert into wylosowane_liczby (losowanie_id, numer, liczba) values (1, 2, 6);
insert into wylosowane_liczby (losowanie_id, numer, liczba) values (1, 3, 41);
insert into wylosowane_liczby (losowanie_id, numer, liczba) values (1, 4, 28);
insert into wylosowane_liczby (losowanie_id, numer, liczba) values (1, 5, 33);

insert into wylosowane_liczby (losowanie_id, numer, liczba) values (2, 0, 13);
insert into wylosowane_liczby (losowanie_id, numer, liczba) values (2, 1, 4);
insert into wylosowane_liczby (losowanie_id, numer, liczba) values (2, 2, 44);
insert into wylosowane_liczby (losowanie_id, numer, liczba) values (2, 3, 14);
insert into wylosowane_liczby (losowanie_id, numer, liczba) values (2, 4, 21);
insert into wylosowane_liczby (losowanie_id, numer, liczba) values (2, 5, 19);
