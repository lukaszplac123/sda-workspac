package pl.sda.spring.repository;

import org.springframework.stereotype.Repository;
import org.springframework.validation.annotation.Validated;
import pl.sda.spring.rest.model.Employee;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Validated
@Repository
public class EmployeeRepository {

    private static long id = 0;
    private Map<Long, Employee> db = new HashMap<>();

    public void create(@NotNull Employee employee) {
        employee.setId(id++);
        db.put(employee.getId(), employee);
    }

    public Optional<Employee> getOne(long id) {
        return Optional.ofNullable(db.get(id));
    }

    public List<Employee> getAll() {
        return db.values().stream().collect(Collectors.toList());
    }

    public void update(@NotNull Employee employee) {
        db.put(employee.getId(), employee);
    }

    public void delete(@Min(value = 0) long id) {
        db.remove(id);
    }
}
