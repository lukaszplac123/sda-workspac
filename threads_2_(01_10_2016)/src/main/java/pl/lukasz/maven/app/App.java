package pl.lukasz.maven.app;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import pl.lukasz.maven.threads.ReadThread;

/**
 * Hello world!
 *
 */
public class App 
{
	public final static String URL = "http://rest-showcase.cloudhub.io/api/students";
	private final File file;
	private BufferedReader bufferedReader;
	private Thread readThread;
	
	
	public BufferedReader getBufferedReader() {
		return bufferedReader;
	}

	public void setBufferedReader(BufferedReader bufferedReader) {
		this.bufferedReader = bufferedReader;
	}

	public Thread getReadThread() {
		return readThread;
	}

	public void setReadThread(Thread readThread) {
		this.readThread = readThread;
	}

	public File getFile() {
		return file;
	}

	App() throws IOException{
		file = new File("names.csv");
		if (!file.exists()){
			file.createNewFile();
		}
		
		bufferedReader = new BufferedReader(new FileReader(file));
		readThread = new ReadThread(bufferedReader);
	}
	
	
    public static void main( String[] args ) throws IOException
    {
    	App app = null;
        try {
			app = new App();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
        
        app.readThread.start();
        
        try {
			app.readThread.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        app.bufferedReader.close();
    }
}
