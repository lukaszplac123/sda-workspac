package pl.lukasz.maven.threads;

import java.io.IOException;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.ObjectMapper;

import pl.lukasz.maven.data.*;

public class SendThread extends Thread{

	private String url;
	private String lineToSend;
	

	public SendThread (String line, String URL) {
		this.url = URL;
		this.lineToSend = line;
	}


	@Override
	public void run() {
    
        HttpClient client = HttpClientBuilder.create().build();

        HttpPost httpPost = new HttpPost(this.url);
        StringBuffer response = new StringBuffer();
        
        String[] stringTable = this.lineToSend.split(",");
        Student student = new Student();
        student.setName(stringTable[0]);
        student.setSurname(stringTable[1]);
        student.setEmail(stringTable[2]);
        student.setInterests("sport");
       
        ObjectMapper mapper = new ObjectMapper();
        System.out.println(student.toString());
        try {

            String jsonRequest = mapper.writeValueAsString(student);
            
        	StringEntity strEntity = new StringEntity(jsonRequest);
       
        	httpPost.setHeader("content-type", "application/json");
        
        	httpPost.setEntity(strEntity);
   
        	HttpResponse httpResponse = client.execute(httpPost);

        	for(Header header : httpResponse.getAllHeaders()){
        			response.append(header.getName() + " : "+ header.getValue());
            }
        	response.append(httpResponse.getStatusLine());
        	System.out.println(response.toString());
        } catch (IOException e) {
            System.err.println("Error while creating request: ");
        }
		
	}
	
}
