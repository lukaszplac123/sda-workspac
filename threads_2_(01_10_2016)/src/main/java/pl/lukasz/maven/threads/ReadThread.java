package pl.lukasz.maven.threads;

import java.io.BufferedReader;
import java.io.IOException;

import pl.lukasz.maven.app.App;
import pl.lukasz.maven.threads.SendThread;

public class ReadThread extends Thread{

	private BufferedReader bufferedReader;
	
	public ReadThread(BufferedReader bufferedReader) {
		this.setBufferedReader(bufferedReader);
	}
	
	@Override
	public void run() {
		String line;
		try {
			while((line = this.bufferedReader.readLine()) != null){
			SendThread send = new SendThread(line, App.URL);
			send.start();
			Thread.sleep(5000);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public BufferedReader getBufferedReader() {
		return bufferedReader;
	}

	public void setBufferedReader(BufferedReader bufferedReader) {
		this.bufferedReader = bufferedReader;
	}

	
}
