<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<html>
<jsp:include page="../fragment/header.jsp" />
<body>
    <div class="container">
        <div class="panel panel-primary">
            <div class="panel-heading">User details</div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-2">Id</div>
                    <div class="col-sm-10">${userForm.id}</div>
                </div>
                <div class="row">
                    <div class="col-sm-2">First name</div>
                    <div class="col-sm-10">${userForm.firstName}</div>
                </div>
                <div class="row">
                    <div class="col-sm-2">Last name</div>
                    <div class="col-sm-10">${userForm.lastName}</div>
                </div>
                <div class="row">
                    <div class="col-sm-2">Role</div>
                    <div class="col-sm-10">${userForm.role}</div>
                </div>
            </div>
        </div>
        <spring:url value="/views/user/list" var="allUrl"/>
        <div class="row">
            <button class="btn btn-danger" onclick="location.href='${allUrl}'">Back</button>
        </div>

    </div>
    <jsp:include page="../fragment/footer.jsp" />
</body>
</html>
