<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<html>
<jsp:include page="../fragment/header.jsp" />
<body>
    <div class="container">
        <table class="table">
            <thead>
            <tr>
                <th>Id</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Role</th>
                <th>Action</th>
            </tr>
            </thead>
            <c:forEach var="user" items="${users}">
                <tr>
                    <td>${user.id}</td>
                    <td>${user.firstName}</td>
                    <td>${user.lastName}</td>
                    <td>${user.role}</td>
                    <td>
                        <spring:url value="/views/user/${user.id}/details/readonly" var="userUrl" />
                        <spring:url value="/views/user/${user.id}/details" var="updateUrl" />

                        <button class="btn btn-info" onclick="location.href='${userUrl}'">Show</button>
                        <button class="btn btn-primary" onclick="location.href='${updateUrl}'">Update</button>
                        <button class="btn btn-danger" onclick="this.disabled=true;">Delete</button>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </div>

    <jsp:include page="../fragment/footer.jsp" />
</body>
</html>
