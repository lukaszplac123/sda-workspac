<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
<jsp:include page="../fragment/header.jsp" />
<body>
    <div class="container">
        <div class="panel panel-primary">
            <div class="panel-heading">User details</div>
            <div class="panel-body">
                <form:form class="form-horizontal" method="POST" modelAttribute="userForm">
                    <form:hidden path="id"/>

                    <spring:bind path="firstName">
                        <div class="form-group ${status.error ? 'has-error' : ''}">
                            <label class="col-sm-2 control-label">First name</label>
                            <div class="col-sm-10">
                                <form:input path="firstName" type="text" class="form-control" id="firstName" placeholder="First Name" />
                                <form:errors path="firstName" class="control-label" />
                            </div>
                        </div>
                    </spring:bind>

                    <spring:bind path="lastName">
                        <div class="form-group ${status.error ? 'has-error' : ''}">
                            <label class="col-sm-2 control-label">Last name</label>
                            <div class="col-sm-10">
                                <form:input path="lastName" type="text" class="form-control" id="lastName" placeholder="Last Name" />
                                <form:errors path="lastName" class="control-label" />
                            </div>
                        </div>
                    </spring:bind>

                    <spring:bind path="role">
                        <div class="form-group ${status.error ? 'has-error' : ''}">
                            <label class="col-sm-2 control-label">Role</label>
                            <div class="col-sm-10">
                                <form:select path="role" items="${roles}" class="form-control" />
                                <form:errors path="role" class="control-label" />
                            </div>
                        </div>
                    </spring:bind>

                    <spring:url value="/views/user/list" var="allUrl"/>

                    <button class="btn btn-success" type="submit">Save</button>
                    <div class="btn btn-danger" onclick="location.href='${allUrl}'">Back</div>
                </form:form>
            </div>
        </div>
    </div>
    <jsp:include page="../fragment/footer.jsp" />
</body>
</html>
