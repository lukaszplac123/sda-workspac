package pl.sda.spring.repository;

import org.springframework.stereotype.Repository;
import pl.sda.spring.controller.model.User;

import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class UserRepository {

    private static long id = 0;
    private Map<Long, User> db = new HashMap<>();

    public UserRepository() {
        db.put(++id, User.builder().id(id).lastName("Einstein").role("ADMIN").build());
        db.put(++id, User.builder().id(id).firstName("Isaac").lastName("Newton").role("USER").build());
        db.put(++id, User.builder().id(id).firstName("Stephen").lastName("Hawking").role("USER").build());
    }

    public void create(User user) {
        user.setId(++id);
        db.put(user.getId(), user);
    }

    public Optional<User> getOne(Long id) {
        return Optional.ofNullable(db.get(id));
    }

    public List<User> getAll() {
        return db.values().stream().collect(Collectors.toList());
    }

    public void update(User user) {
        db.put(user.getId(), user);
    }

    public void delete(long id) {
        db.remove(id);
    }
}
