package pl.sda.spring.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import pl.sda.spring.controller.model.User;
import pl.sda.spring.repository.UserRepository;
import pl.sda.spring.validator.UserValidator;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Arrays;

@Controller
public class UserController {

    private final Logger logger = LoggerFactory.getLogger(UserController.class);
    static final String SHOW_USER_LIST = "/views/user/list";
    static final String SHOW_READONLY_USER_DETAILS = "/views/user/{id}/details/readonly";
    static final String SHOW_NEW_USER_DETAILS = "/views/user/new";
    static final String SHOW_EXISTING_USER_DETAILS = "/views/user/{id}/details";

    static final String USER_LIST_VIEW = "/user/user-list";
    static final String USER_READONLY_VIEW = "/user/user-readonly";
    static final String USER_EDITABLE_VIEW = "/user/user-details";

    @Autowired
    private UserRepository repository;
    @Autowired
    private UserValidator userValidator;

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(userValidator);
    }

    @RequestMapping(path = "/", method = RequestMethod.GET)
    public ModelAndView index() {
        logger.debug("REDIRECT TO SHOW USER LIST");
        return redirectToListPage();
    }

    @RequestMapping(path = SHOW_USER_LIST, method = RequestMethod.GET)
    public String getAll(Model model) {
        logger.debug("SHOW USER LIST");
        model.addAttribute("users", repository.getAll());
        return USER_LIST_VIEW;
    }

    @RequestMapping(path = SHOW_READONLY_USER_DETAILS, method = RequestMethod.GET)
    public ModelAndView showUserDetails(@PathVariable @NotNull @Min(0) Long id) {
        logger.debug("SHOW READONLY USER DETAILS");
        return repository
                .getOne(id)
                .map((user) -> addUserContext(USER_READONLY_VIEW, user))
                .orElseGet(this::redirectToListPage);
    }

    @RequestMapping(path = SHOW_NEW_USER_DETAILS, method = RequestMethod.GET)
    public ModelAndView addUserDetails() {
        logger.debug("SHOW USER NEW");
        return addUserContext(USER_EDITABLE_VIEW, new User());
    }

    @RequestMapping(path = SHOW_NEW_USER_DETAILS, method = RequestMethod.POST)
    public ModelAndView saveUserDetails(@ModelAttribute("userForm") @Validated User user,
                                        BindingResult bindingResult) {
        logger.debug("SAVE NEW USER");
        if(bindingResult.hasErrors()) {
            logger.debug("User has errors ? {}", bindingResult.hasErrors());
            for(ObjectError error : bindingResult.getAllErrors()) {
                logger.debug(error.toString());
            }
            return addUserContext(USER_EDITABLE_VIEW, user);
        } else {
            repository.create(user);
            return redirectToListPage();
        }
    }

    @RequestMapping(path = SHOW_EXISTING_USER_DETAILS, method = RequestMethod.GET)
    public ModelAndView editUserDetails(@PathVariable @NotNull @Min(0) Long id) {
        logger.debug("SHOW USER EDIT");
        return repository
                .getOne(id)
                .map((user) -> addUserContext(USER_EDITABLE_VIEW, user))
                .orElseGet(() -> addUserContext(USER_EDITABLE_VIEW, new User()));
    }

    private ModelAndView addUserContext(String viewName, User user) {
        ModelAndView model = new ModelAndView(viewName);
        model.addObject("userForm", user);
        model.addObject("roles", Arrays.asList("ADMIN", "USER"));

        return model;
    }

    private ModelAndView redirectToListPage() {
        return new ModelAndView("redirect:" + SHOW_USER_LIST);
    }

    @RequestMapping(path = SHOW_EXISTING_USER_DETAILS, method = RequestMethod.POST)
    public ModelAndView saveOrUpdateUser(@ModelAttribute("userForm") @Validated User user,
                                         BindingResult bindingResult) {
        logger.debug("SAVE OR UPDATE USER DETAILS");
        if(bindingResult.hasErrors()) {
            logger.debug("User has errors ? {}", bindingResult.hasErrors());
            for(ObjectError error : bindingResult.getAllErrors()) {
                logger.debug(error.toString());
            }
            return addUserContext(USER_EDITABLE_VIEW, user);
        } else {
            repository.update(user);
            return redirectToListPage();
        }
    }
}
