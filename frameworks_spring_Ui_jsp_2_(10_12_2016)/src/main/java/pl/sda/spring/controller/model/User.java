package pl.sda.spring.controller.model;

public class User {

    private Long id;
    private String firstName;
    private String lastName;
    private String role;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public static UserBuilder builder() {
        return new UserBuilder();
    }

    @Override
    public String toString(){
        return "{ ID = " + id + ", First name = " + firstName + ", Last name = " + lastName + ", Role = " + role + " }";
    }

    public static class UserBuilder {
        private final User user = new User();

        public UserBuilder id(Long id) {
            user.id = id;
            return this;
        }

        public UserBuilder firstName(String firstName) {
            user.firstName = firstName;
            return this;
        }

        public UserBuilder lastName(String lastName) {
            user.lastName = lastName;
            return this;
        }

        public UserBuilder role(String role) {
            user.role = role;
            return this;
        }

        public User build() {
            return user;
        }
    }
}