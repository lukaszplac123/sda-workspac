package pl.sda.spring.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import pl.sda.spring.controller.model.User;

@Component
public class UserValidator implements Validator {
    @Override
    public boolean supports(Class<?> clazz) {
        return User.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        User user = User.class.cast(target);

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "firstName", "user.firstName.null.or.empty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lastName", "user.lastName.null.or.empty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "role", "user.role.null.or.empty");
    }
}
