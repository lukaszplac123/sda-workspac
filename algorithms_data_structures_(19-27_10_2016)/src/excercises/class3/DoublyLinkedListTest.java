package excercises.class3;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class DoublyLinkedListTest {
	
	private DoublyLinkedList<Integer> list;

	@Before
	public void setUp(){
		list = new DoublyLinkedList<Integer>();	
	}
	
	@Test
	public void append_works_correctly() {
		list.append(1);
		list.append(2);
		
		assertListsEqual(list, 1, 2);
	}

	@Test
	public void prepend_works_correctly(){
		list.prepend(1);
		list.prepend(2);
		
		assertListsEqual(list, 2, 1);
	}
	
	@Test(expected = IndexOutOfBoundsException.class)
	public void insert_throws_exception_when_index_out_of_bound(){
		list.insert(1, 2);
	}
	
	@Test
	public void insert_works_correctly(){
		list.insert(0, 5);
		list.insert(0, 3);
		list.insert(1, 2);
		
		assertListsEqual(list, 3, 2, 5);
	}
	
	@Test
	public void delete_works_correctly(){
		list.append(1);
		list.append(2);
		list.append(3);
		
		list.delete(1);
		
		assertListsEqual(list, 1, 3);
		
		list.delete(1);
		
		assertListsEqual(list, 1);
	}
	
	@Test
	public void delete_last_works_correctly(){
		list.append(1);
		list.append(2);
		list.append(3);
		
		list.deleteLast();
		
		assertListsEqual(list, 1, 2);
		
		list.deleteLast();
		
		assertListsEqual(list, 1);
	}
	
	@Test
	public void delete_first_works_correctly(){
		list.append(1);
		list.append(2);
		list.append(3);
		
		list.deleteFirst();
		
		assertListsEqual(list, 2, 3);
		
		list.deleteFirst();
		
		assertListsEqual(list, 3);
	}
	
	@Test
	public void length_works_correctly(){
		list.append(1);
		list.append(2);
		list.append(3);
		
		assertEquals(3, list.length());
	}
	
	private void assertListsEqual(DoublyLinkedList<Integer> array, int... elements){
		for(int i=0;i<elements.length;i++){
			assertEquals(elements[i], (int) array.get(i));
		}
		assertEquals(array.length(), elements.length);
	}
}
