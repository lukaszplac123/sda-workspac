package excercises.class3;

import java.util.Random;


public class DoublyLinkedList<T> {
	private int length;
	private Node first;
	private Node last;
	
	public static void main(String[] args){
		

	}
	
	private class Node{
		T value;
		Node previous;
		Node next;
		
		public Node(T value, Node previous, Node next){
			this.value = value;
			this.previous = previous;
			this.next = next;
		}
	}


	public void append(T element) {
		Node newNode = new Node(element, null, null);
		if(first == null){
			first = newNode;
			last = newNode;
		} else{
			newNode.previous = last;
			last.next = newNode;
			//last = newNode;
		}
		length++;
	}
	
	public void prepend(T element) {
		Node newNode = new Node(element, null, null);
		if(first == null){
			first = newNode;
			last = newNode;
		} else{
			newNode.next = first;
			first.previous = newNode;
			first = newNode;
		}
		length++;
	}


	public int length() {
		return length;
	}

	public T get(int index) {
		return getNode(index).value;
	}

	public T deleteFirst() {
		checkRangeForGet(0);
		T result = null;
		if(length == 1){
			result = first.value;
			first = null;
			last = null;
		} else{
			result = first.value;
			first.next.previous = null;
			first = first.next;
		}
		length--;
		return result;
	}


	public T deleteLast() {
		checkRangeForGet(length - 1);
		T result = null;
		if(length == 1){
			result = first.value;
			first = null;
			last = null;
		} else{
			result = last.value;
			last.previous.next = null;
			last = last.previous;
		}
		length--;
		return result;
	}


	public T delete(int index) {
		checkRangeForGet(index);
		if(index == 0){
			return deleteFirst();
		} else if(index == length - 1){
			return deleteLast();
		}
		Node node = getNode(index);
		T result = node.value;
		node.previous.next = node.next;
		node.next.previous = node.previous;
		length--;
		return result;
	}


	public void insert(int index, T element) {
		checkRangeForInsert(index);
		if(index == 0){
			prepend(element);
		} else if (index == length){
			append(element);
		} else{
			Node currentAtIndex = getNode(index);
			Node newNode = new Node(element, currentAtIndex.previous, currentAtIndex);
			currentAtIndex.previous.next = newNode;
			currentAtIndex.previous = newNode;
			length++;
		}
	}
	
	private Node getNode(int index){
		checkRangeForGet(index);
		Node current = null;
		if (index < (length / 2)) {
			current = first;
			for (int i = 0; i < index; i++){
				current = current.next;
			}
		} else {
			current = last;
			for (int i = length - 1; i > index; i--){
				current = current.previous;
			}
		}
		
		return current;
	}
	
	private void checkRangeForInsert(int index) {
		if(index > length || index < 0){
			throw new IndexOutOfBoundsException();
		}
	}
	
	private void checkRangeForGet(int index) {
		if(index >= length || index < 0){
			throw new IndexOutOfBoundsException();
		}
	}
}
