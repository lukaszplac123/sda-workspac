package excercises.class1;

public class DynamicArray<T> {
	private T[] elements;
	private int firstEmpty;
	
	@SuppressWarnings("unchecked")
	public DynamicArray(int initialSize){
		elements = (T[]) new Object[initialSize];
		firstEmpty = 0;
	}
	
	private void changeArraySize(int newSize){
		T[] newElements = (T[]) new Object[newSize];
		System.arraycopy(elements, 0, newElements, 0, firstEmpty);
		elements = newElements;
	}

	private void extendArrayIfNecessary(){
		if(elements.length == firstEmpty){
			changeArraySize(elements.length * 2);
		}
	}
	
	public void append(T element){
		extendArrayIfNecessary();
		elements[firstEmpty] = element;
		firstEmpty++;
	}
	
	public static void main(String[] args){
		DynamicArray<Integer> arr = new DynamicArray<>(10);
		arr.append(10);
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	private void shortenArray(){
		if(elements.length / 4 <= firstEmpty){
			changeArraySize(elements.length / 2);
		}
	}
	
	public void prepend(T element){
		insert(0, element);
	}
	
	public void insert(int index, T element){
		if(index < 0 || index > firstEmpty){
			throw new IndexOutOfBoundsException("index = " + index);
		}
		extendArrayIfNecessary();
		for(int i=firstEmpty; i>index;i--){
			elements[i] = elements[i - 1];
		}
		elements[index] = element;
		firstEmpty++;
	}
	
	public T delete(int index){
		T deleted = elements[index];
		elements[index] = null;
		
		for(int i=index;i<firstEmpty;i++){
			elements[i] = elements[i+1];
		}
		
		firstEmpty--;
		
		shortenArray();
		
		return deleted;
	}
	
	public int length(){
		return firstEmpty;
	}
	
	public T get(int index){
		return elements[index];
	}
}
