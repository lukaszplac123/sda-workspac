package excercises.class1;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class DynamicArrayTest {

	private DynamicArray<Integer> array;

	@Before
	public void setUp(){
		array = new DynamicArray<>(1);		
	}
	
	@Test
	public void append_works_correctly() {
		array.append(1);
		array.append(2);
		
		assertListsEqual(array, 1, 2);
	}

	@Test
	public void prepend_works_correctly(){
		array.prepend(1);
		array.prepend(2);
		
		assertListsEqual(array, 2, 1);
	}
	
	@Test(expected = IndexOutOfBoundsException.class)
	public void insert_throws_exception_when_index_out_of_bound(){
		array.insert(1, 2);
	}
	
	@Test
	public void insert_works_correctly(){
		array.insert(0, 5);
		array.insert(0, 3);
		array.insert(1, 2);
		
		assertListsEqual(array, 3, 2, 5);
	}
	
	@Test
	public void delete_works_correctly(){
		array.append(1);
		array.append(2);
		array.append(3);
		
		array.delete(1);
		
		assertListsEqual(array, 1, 3);
	}
	
	@Test
	public void length_works_correctly(){
		array.append(1);
		array.append(2);
		array.append(3);
		
		assertEquals(3, array.length());
	}
	
	private void assertListsEqual(DynamicArray<Integer> array, int... elements){
		for(int i=0;i<elements.length;i++){
			assertEquals(elements[i], (int) array.get(i));
		}
		assertEquals(array.length(), elements.length);
	}
}
