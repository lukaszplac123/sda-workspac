package excercises.class6;

import java.util.ArrayList;
import java.util.List;

public class BinaryHeap {
 
 private List<Integer> heap;

 
 public BinaryHeap() {
  heap = new ArrayList<>();
 }
 
 public static void main(String[] args) {
  BinaryHeap binaryHeap = new BinaryHeap();
  binaryHeap.insert(12);
  binaryHeap.insert(23);
  binaryHeap.insert(11);
  binaryHeap.insert(18);
  binaryHeap.insert(7);
  binaryHeap.insert(27);
  binaryHeap.insert(16);
  binaryHeap.insert(2);
  binaryHeap.insert(10);
  binaryHeap.insert(7);
  binaryHeap.insert(22);
  binaryHeap.insert(13);
  binaryHeap.insert(7);
  binaryHeap.insert(27);
  binaryHeap.insert(16);
  binaryHeap.insert(2);
  binaryHeap.heap.forEach((item) -> System.out.print(item.toString()+ " "));
  System.out.println();
  System.out.println("Peek min -> " + binaryHeap.peekMin());
  binaryHeap.heap.forEach((item) -> System.out.print(item.toString()+ " "));
  System.out.println();
  System.out.println("Delete min -> " + binaryHeap.deleteMin());
  binaryHeap.heap.forEach((item) -> System.out.print(item.toString()+ " "));
  System.out.println();
  System.out.println("Delete min -> " + binaryHeap.deleteMin());
  binaryHeap.heap.forEach((item) -> System.out.print(item.toString()+ " "));
  System.out.println();
  System.out.println("Delete min -> " + binaryHeap.deleteMin());
  binaryHeap.heap.forEach((item) -> System.out.print(item.toString()+ " "));
 }
 
 public Integer peekMin(){
  return heap.get(0); 
 }
 
 public Integer deleteMin(){
  Integer temp = heap.remove(heap.size()-1); //pobiera ostatni i usuwa z drzewa
  Integer value = heap.remove(0); //pobiera pierwszy i usuwa z drzewa
  heap.add(0, temp); //wstawia pobrany ostatni element na pierwszem miesce w drzewie
  if (heap.size() > 1){
   updateHeapOnDelete(heap); //update drzewa przy usuwaniu
  }
  return value; 
 }
 
 public void insert(Integer element){
  heap.add(element); //wstawia element na koniec
  if (heap.size() > 1){
   updateHeapOnInsert(heap); //update drzewa przy wstawianiu
  }
 }
 
 private void updateHeapOnInsert(List<Integer> heap){
  int childIndex = heap.size() - 1; //index ostatnio wstawionego elementu - child index
  int parentIndex = (childIndex - 1)/2; // index rodzica ostatnio wstawionego elementu
  Integer child = heap.get(childIndex);
  Integer parent = heap.get(parentIndex);
   while (parent > child){
    switchElements(childIndex, parentIndex);
    childIndex = parentIndex;
    parentIndex = (childIndex - 1)/2;
    child = heap.get(childIndex);
    parent = heap.get(parentIndex);
   }
 }
 
 private void updateHeapOnDelete(List<Integer> heap){
  int parentIndex = 0; //poczatkowy index rodzica
  int parent = 1; //zapewnia pierwsze wejscie do petli
  int child = 0; //zapewnia pierwsze wejscie do petli
  int child1 = 0;
  int child2 = 0;
  int indexOfSmaller = 0;
  while ((parent > child)){
  int child1Index = 2*parentIndex + 1;
  int child2Index = 2*parentIndex + 2;
   if ((child2Index >= heap.size()) && (child1Index >= heap.size())) { //sprawdzenie warunkow instnienia dzieci danego rodzica
    break;
    }
    else if (child2Index >= heap.size()) { //jezeli prawe dziecko nie istnieje to podstawiania jest do niego wartosc maksymalna istotna przy porownaniu
      child2 = Integer.MAX_VALUE;
      child1 = heap.get(child1Index);
    }else {
     child1 = heap.get(child1Index);
     child2 = heap.get(child2Index);
    }
   if (child1 < child2){  //wybranie mniejszego dziecka i pobranie jego indexu
    indexOfSmaller = child1Index;
   } else if (child1 == child2){
    indexOfSmaller = child1Index;
   } else{
    indexOfSmaller = child2Index;
   }
  parent = heap.get(parentIndex);  //pobranie rodzica
  child = heap.get(indexOfSmaller); //pobranie mniejszego dziecka
  if (parent > child) switchElements(indexOfSmaller, parentIndex); //zamiana miejscami rodzica z mniejszym dzieckiem
  parentIndex = indexOfSmaller; //aktualizacja indexu rodzica na index dziecka z ktorym sie zamienil miejscami 
  } 
 }
 
 private void switchElements(int index1, int index2){
  Integer temp1 = heap.get(index1); //child
  Integer temp2 = heap.get(index2); //parent
  heap.add(index2, temp1);
  heap.remove(index2+1);
  heap.add(index1, temp2);
  heap.remove(index1+1);
 }
}
