package excercises.class7;

import java.util.List;

//pivot jako pierwszy elelement listy
public class QuickSortPivotZero<T extends Comparable<T>> extends QuickSort<T> {

	@Override
	public int pivot(List<T> input) {	
		return 0;
	}

}
