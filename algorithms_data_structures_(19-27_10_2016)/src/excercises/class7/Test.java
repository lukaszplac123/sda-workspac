package excercises.class7;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Test {

	public static void main(String[] args) {
		
		//List<Integer> input = Arrays.asList(12,1,34,3,45,28,1,24,6,34,15,13);
		List<Integer> input = new ArrayList<>();
		int limit = 5000;
		for (int i = limit; i>=0; i--){
			input.add(i);
		}
		
		
		long time1a = System.currentTimeMillis();
		QuickSortPivotZero<Integer> qspiv_zero = new QuickSortPivotZero<>();
		System.out.println("sorted1:"+qspiv_zero.sort(input) + ",");
		System.out.println("Time1:"+(System.currentTimeMillis() - time1a));
		
		long time2a = System.currentTimeMillis();
		QuickSortPivotMedian<Integer> qspiv_median = new QuickSortPivotMedian<>();
		System.out.println("sorted2"+qspiv_median.sort(input) + ",");
		System.out.println("Time2:"+(System.currentTimeMillis() - time2a));
		
		long time3a = System.currentTimeMillis();
		QuickSortPivotRandom<Integer> qspiv_rand = new QuickSortPivotRandom<>();
		System.out.println("sorted3"+qspiv_rand.sort(input) + ",");
		System.out.println("Time2:"+(System.currentTimeMillis() - time3a));
	}
	
}
