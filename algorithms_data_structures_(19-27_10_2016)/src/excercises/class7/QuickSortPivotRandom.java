package excercises.class7;

import java.util.List;
import java.util.Random;

public class QuickSortPivotRandom<T extends Comparable<T>> extends QuickSort<T>{

	@Override
	public int pivot(List<T> input) {
		Random random = new Random();
		int rand = random.nextInt(input.size());
		return rand;
	}

}
