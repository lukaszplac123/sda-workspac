package excercises.class7;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class QuickSort<T extends Comparable<T>>{

	public List<T> sort(List<T> input){
		if (input.size() == 0){
			return Collections.EMPTY_LIST;
		} 
		int pivotIndex = pivot(input);
		T pivotElement = input.get(pivotIndex);
//		System.out.println("pivot->"+pivotElement);
		List<T> leftList = new ArrayList<>();
		List<T> rightList = new ArrayList<>();

		List<T> output = new ArrayList<>();
			for (int i = 0; i < input.size(); i++){
				if (i == pivotIndex) continue;
				T currentElement = input.get(i);
				if (pivotElement.compareTo(currentElement) >= 0){
					leftList.add(currentElement);
				} else if (pivotElement.compareTo(currentElement) < 0){
					rightList.add(currentElement);
				}
			}
//		System.out.println("1 ->"+leftList.toString());
//		System.out.println("2 ->"+rightList.toString());
		output.addAll(sort(leftList));
		output.add(pivotElement);
		output.addAll(sort(rightList));
//		System.out.println("Zwracanie:" + output.toString());
		return output;
	}
	
	public abstract int pivot (List<T> input);
}
