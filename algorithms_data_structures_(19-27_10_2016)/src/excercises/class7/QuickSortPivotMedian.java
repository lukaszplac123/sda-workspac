package excercises.class7;

import java.util.List;

public class QuickSortPivotMedian<T extends Comparable<T>> extends QuickSort<T> {

	@Override
	public int pivot(List<T> input) {
		int first = (int) input.get(0);
		int seccond = (int) input.get(input.size()/2);
		int third = (int) input.get(input.size()-1);
		int middle = mid(first, seccond, third);
//		System.out.print(first+","+seccond+","+third+",middle:"+middle+"|");
		return input.indexOf(middle);
	}
	
	private int mid(int a, int b, int c)
	{
	    return (a<b && a>c) || (a>b && a<c) ? a : ( (a>b && b>c) || (a<b && b<c) ? b : c);
	}

}
