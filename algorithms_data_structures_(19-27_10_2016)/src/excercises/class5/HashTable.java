package excercises.class5;

import java.lang.reflect.Array;

public class HashTable<K, V>{
	
	private final Entry[] entries;
	
	public static void main(String[] args){
		HashTable<Integer, Integer> table = new HashTable<>(10);
		table.put(10, 10);
		System.out.println(table.get(10));
	}
	
	public HashTable(int length){
		entries = (Entry[]) (Entry[]) Array.newInstance(Entry.class, length);
	}
	
	public void put(K key, V value){
		int index = index(key);
		if(entries[index] == null){
			entries[index] = new Entry(key, value, null);
		} else{
			entries[index] = new Entry(key, value, entries[index]);
		}
	}
	
	public V get(K key){
		int index = index(key);
		Entry entry = entries[index];
		while(entry != null){
			if(entry.key.equals(key)){
				return entry.value;
			}
			entry = entry.next;
		}
		return null;
	}
	
	private int index(K key){
		int hash = key.hashCode();
		return hash % entries.length;
	}
	
	private class Entry{
		private K key;
		private V value;
		private Entry next;
		
		public Entry(K key, V value, Entry next){
			this.key = key;
			this.value = value;
			this.next = next;
		}
	}
}
