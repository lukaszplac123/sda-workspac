package excercises.class5;

import java.lang.reflect.Array;

public class HashTableOpenAddressing<K, V>{
	
	private Entry[] entries;
	private int emptyEntries;
	
	public HashTableOpenAddressing(int length){
		entries = (Entry[]) Array.newInstance(Entry.class, length);
		emptyEntries = length;
	}
	
	public void put(K key, V value){
		int index = getIndex(key);
		if(entries[index] == null){
			emptyEntries--;
		}
		if(emptyEntries <= resizeThreshold()){
			resize();
			put(key, value);
		} else{
			entries[index] = new Entry(key, value);
		}
	}
	
	private void resize() {
		Entry[] oldEntries = entries;
		entries = (Entry[]) Array.newInstance(Entry.class, entries.length * 2);
		emptyEntries = entries.length;
		for(Entry entry : oldEntries){
			if(entry != null){
				put(entry.key, entry.value);
			}
		}
	}

	private int resizeThreshold(){
		return 1 * entries.length / 4;
	}

	private int getIndex(K key) {
		int index = index(key);
		while(entries[index] != null && !entries[index].key.equals(key)){
			index++;
		}
		return index;
	}
	
	public V get(K key){
		int index = getIndex(key);
		return entries[index] == null ? null : entries[index].value;
	}
	
	
	
	private int index(K key){
		int hash = key.hashCode();
		return hash % entries.length;
	}
	
	private class Entry{
		private K key;
		private V value;
		
		public Entry(K key, V value){
			this.key = key;
			this.value = value;
		}
		
		public String toString(){
			return String.format("%s, %s", key.toString(), value.toString());
		}
	}
}
