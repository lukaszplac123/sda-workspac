package excercises.class4;

public interface Stack<T> {
	T pop();
	T peek();
	void push(T element);
	boolean isEmpty();
}
