package excercises.class4;

public class CircularBuffer<T> {

	private int write = 0;
	private int read = 0;
	private T[] elements; 
	
	public static void main(String[] args){
		CircularBuffer<Integer> buffer = new CircularBuffer<>(4);
		buffer.enqueue(1);
		buffer.enqueue(2);
		buffer.enqueue(3);
		buffer.enqueue(4);
		buffer.enqueue(5);
	}
	
	@SuppressWarnings("unchecked")
	public CircularBuffer(int size){
		elements = (T[]) new Object[size];
	}
	
	public void enqueue(T element){
		if(isFull()){
			throw new IllegalStateException("buffer full");
		} else{
			elements[write] = element;
			write = increaseIndex(write);
		}
	}
	
	public T dequeue(){
		if(isEmpty()){
			throw new IllegalStateException("buffer empty");
		}
		T next = elements[read];
		elements[read] = null;
		read = increaseIndex(read);
		return next;
	}
	
	private int increaseIndex(int size){
		return (size + 1) % elements.length;
	}
	
	public boolean isEmpty(){
		return write == read;
	}
	
	public boolean isFull(){
		return (write + 1) % elements.length == read;
	}
}
