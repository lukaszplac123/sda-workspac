package excercises.class4;

import excercises.class2.SinglyLinkedList;

public class LinkedListStack<T> implements Stack<T>{

	private SinglyLinkedList<T> list;
	
	public LinkedListStack(SinglyLinkedList<T> list){
		this.list = list;
	}

	@Override
	public T pop() {
		return list.deleteFirst();
	}

	@Override
	public void push(T element) {
		list.prepend(element);
	}
	
	@Override
	public T peek(){
		return list.get(0);
	}

	@Override
	public boolean isEmpty() {
		return list.length() == 0;
	}
	
}
