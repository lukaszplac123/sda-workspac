package excercises.class4;

import excercises.class2.SinglyLinkedList;

public class QueueOnStacks<T> {
	private Stack<T> one;
	private Stack<T> other;
	
	public QueueOnStacks(){
		one = new LinkedListStack<T>(new SinglyLinkedList<>());
		other = new LinkedListStack<T>(new SinglyLinkedList<>());
	}
	
	public T dequeue(){
		if(other.isEmpty()){
			while(!one.isEmpty()){
				other.push(one.pop());
			}
		}
		return other.pop();
	}
	
	public void enqueue(T element){
		one.push(element);
	}
}
