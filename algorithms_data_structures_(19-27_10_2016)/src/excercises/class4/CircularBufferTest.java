package excercises.class4;

import static org.junit.Assert.*;
import org.junit.Test;

public class CircularBufferTest {

	@Test(expected = IllegalStateException.class)
	public void should_throw_exception_when_enqueue_and_queue_is_full(){
		CircularBuffer<Integer> buffer = new CircularBuffer<>(3);
		buffer.enqueue(1);
		buffer.enqueue(2);
		buffer.enqueue(3);
	}
	
	@Test(expected = IllegalStateException.class)
	public void should_throw_exception_when_dequeue_and_queue_is_empty(){
		CircularBuffer<Integer> buffer = new CircularBuffer<>(3);
		buffer.dequeue();
	}
	
	@Test
	public void queue_works_correctly(){
		CircularBuffer<Integer> buffer = new CircularBuffer<>(3);
		assertTrue(buffer.isEmpty());
		assertFalse(buffer.isFull());
		buffer.enqueue(1);
		assertFalse(buffer.isEmpty());
		assertFalse(buffer.isFull());
		buffer.enqueue(2);
		assertFalse(buffer.isEmpty());
		assertTrue(buffer.isFull());
		
		assertEquals((Integer) 1, buffer.dequeue());
		assertEquals((Integer) 2, buffer.dequeue());
	}
}
