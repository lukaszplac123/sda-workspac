package zadanie4;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;


public class Demo {

	public static void main (String[] args){
		
		Person person1 = new Person("Marian Kowalski", 50, Arrays.asList("Fanta", "Cola"), new Address("Warszawa", "Mila", 23));
		
		Person person2 = new Person("Pawl Kukiz", 48, Arrays.asList("Pepsi", "Sprite", "water"), new Address("Warszawa", "3 Maja", 23));
		
		Person person3 = new Person("Agata Buzek", 37, Arrays.asList("milk"), null);
		
		List<Person> personList = Arrays.asList(person1, person2, person3);
		
		ReportPrinter rp = new ReportPrinter(personList);
		

		try {
			rp.jackson();
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
}
