package zadanie4;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Person {

	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public int getAge() {
		return age;
	}


	public void setAge(int age) {
		this.age = age;
	}


	public List<String> getOrders() {
		return orders;
	}


	public void setOrders(List<String> orders) {
		this.orders = orders;
	}


	public Address getAddress() {
		return address;
	}


	public void setAddress(Address address) {
		this.address = address;
	}


	String name;
	int age;
	List<String> orders;
	Address address;
	
	
	public Person(String name, int age, List<String> orders, Address address){
		this.name = name;
		this.age = age;
		this.orders = orders;
		this.address = address;
	}

}
