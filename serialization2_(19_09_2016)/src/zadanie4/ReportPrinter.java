package zadanie4;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ReportPrinter {

	List<Person> personList;
	
	public ReportPrinter(List<Person> personList) {
		this.personList = new ArrayList<Person>(personList);
	}
	



	public void jackson() throws JsonProcessingException{
		
		ObjectMapper mapper = new ObjectMapper();
		String personsReport = mapper
				.writerWithDefaultPrettyPrinter()
				.writeValueAsString(personList);
		System.out.println(personsReport);
		
	}
	
	
}
