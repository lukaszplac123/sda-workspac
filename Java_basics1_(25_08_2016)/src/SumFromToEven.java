public class SumFromToEven {
	
	public static void main(String[] args){
		System.out.println(sumFromToEven(2,6));
	}
	
	private static long sumFromToEven(int a, int b){
		long sum = 0;
		for (int i = a; i <= b; i++){
			if (i%2 == 0) sum += i;
		}
		return sum;
	}
	
}

