
public class AbsoluteValue {
	public static void main (String[] args){
		AbsoluteValue abs = new AbsoluteValue();
		System.out.println(abs.abs(-10));
		System.out.println(abs.abs(9));
		System.out.println(abs.abs(-1));
	}
	
	public int abs(int a){
//		if (a >= 0){
//			return a;
//		} else {
//			return -a;
//		}
//		
		return a >= 0 ? a : -a;
	}
}
