package Exercises;

import java.util.Random;
import java.util.function.Function;

public class Exercises {

	public static void main(String[] args) {
		Exercises exercises = new Exercises();
//		System.out.println(exercises.abs(10));
//		System.out.println(exercises.abs(-10));
//		System.out.println(exercises.abs(0));
//
//		System.out.println(exercises.min(new int[] { 1, 2, 3 }));
//
//		System.out.println(exercises.min(new int[] { 3, 2, 1 }));
//
//		System.out.println(exercises.min(new int[] { 2, 1, 3 }));
		
		String test = "";
		for (int i = 0; i < 100000; i++) {
			test += (char) new Random().nextInt(256);
		}
		
		long start = System.currentTimeMillis();
		exercises.reverse(test);
		long end = System.currentTimeMillis();
		System.out.println("charAt + conversion to string = " + (end - start));
		
		
		start = System.currentTimeMillis();
		exercises.reverse2(test);
		end = System.currentTimeMillis();
		System.out.println("charAt + stringbuilder = " + (end - start));
		
		start = System.currentTimeMillis();
		exercises.reverse3(test);
		end = System.currentTimeMillis();
		System.out.println("substring = " + (end - start));
		
		start = System.currentTimeMillis();
		exercises.reverse4(test);
		end = System.currentTimeMillis();
		System.out.println("char array = " + (end - start));
		
		System.out.println(exercises.reverse("abcdef"));
		System.out.println(exercises.reverse2("abcdef"));
		System.out.println(exercises.reverse3("abcdef"));
		System.out.println(exercises.reverse4("abcdef"));
		
		//System.out.println(exercises.min(new int[] { 2, 4, 3, 1, 2, 5 }));
	}

	public int abs(int a) {
		if (a >= 0) {
			return a;
		} else {
			return -a;
		}
	}

	// 2. Napisz funkcję znajdującą minimum w tablicy liczb całkowitych.
	// public int min(int[] arr)

	public int min(int[] arr) {
		int min = Integer.MAX_VALUE;

		for (int i = 0; i < arr.length; i++) {
			if (arr[i] < min) {
				min = arr[i];
			}
		}

		return min;
	}

	// 3. Napisz funkcję sumującą wszystkie liczby całkowite w przedziale (a,b)
	public int sum(int a, int b) {
		if (a > b) {
			int temp = a;
			a = b;
			b = temp;
		}
		int sum = 0;
		for (int i = a; i <= b; i++) {
			sum += i;
		}
		return sum;
	}

	// 4. Napisz podobną funkcję jak w 3. ale sumującą tylko parzyste liczby z
	// podanego przedziału.
	public int sumEven(int a, int b) {
		int sum = 0;
		for(int i=a;i<=b;i++){
			if(i % 2 == 0){
				sum += i;
			}
		}
		return sum;
	}

	// 5. Napisz funkcję odwracającą String, np dla wejścia "napis" metoda
	// zwróci "sipan"
	
	public String reverse(String str) {
		String result = "";
		for(int i=str.length() - 1; i>=0;i--){
			char ch = str.charAt(i);
			result += String.valueOf(ch);
		}
		return result;
	}
	
	public String reverse2(String str) {
		StringBuilder result = new StringBuilder(str.length());
		for(int i=str.length() - 1; i>=0;i--){
			char ch = str.charAt(i);
			result.append(ch);
		}
		return result.toString();
	}
	
	public String reverse3(String str) {
		String result = "";
		for(int i=str.length(); i>=1;i--){
			result += str.substring(i-1, i);
		}
		return result;
	}
	
	public String reverse4(String str) {
		char[] result = new char[str.length()];
		for(int i=0;i<str.length();i++){
			result[i] = str.charAt(str.length() - 1 - i);
		}
		return new String(result);
	}
	
	public boolean isPalindrome(String str){
		String reversed = reverse(str);
		return reversed.equals(str);
	}
	
	// abba 2
	// abcba 2
	
	public boolean isPalindrome2(String str){
		if(str.isEmpty()){
			return true;
		}
		for(int i=0;i<str.length() / 2;i++){
			char one = str.charAt(i);
			char other = str.charAt(str.length() - 1 - i);
			if(one != other){
				return false;
			}
		}
		return true;
	}
	
	public static final int COSTS = 0 ;
	public static final int TAX_THRESHOLD = 85528;
	public static final double LOW_RATE = 0.18;
	public static final double HIGH_RATE = 0.32;
	public static final double LOW_TAX = TAX_THRESHOLD * LOW_RATE;
	public static final double FREE_AMOUNT = 556.02;
	
	public double calculateTax(double income, double socialInsurance, double healthInsurance){
		double base = income - 0;
		
		double taxableIncome = base - socialInsurance;
		
		double taxAmount = 0.0;
		if(taxableIncome <= TAX_THRESHOLD){
			taxAmount = taxableIncome * LOW_RATE;
		} else{
			taxAmount = LOW_TAX;
			double overThreshold = taxableIncome - TAX_THRESHOLD;
			taxAmount += overThreshold * HIGH_RATE;
		}
		
		taxAmount -= healthInsurance;
		
		return taxAmount - FREE_AMOUNT;
	}
}
