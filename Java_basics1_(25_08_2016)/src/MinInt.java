
public class MinInt {
	private int[] array;
	
	public static void main (String[] args){
			MinInt minInt = new MinInt(new int[]{2,4,8,4,5,123,44,11,22,11,22,1});
			System.out.println("Najmniejsza to: " + minInt.minValue());
	}
	MinInt(int[] tab){
		this.array = new int[tab.length];
		for (int i = 0 ; i < tab.length; i++){
			this.array[i] = tab[i];
		}
	}
	
	public int minValue(){
		int min = Integer.MAX_VALUE;
		for (int i=0; i < this.array.length ; i++){
			if (this.array[i] < min){
				min = this.array[i];
			}
		}
		return min;
	}

}
	
