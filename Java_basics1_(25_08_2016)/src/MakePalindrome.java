
public class MakePalindrome {
	public static void main(String[] args){
		System.out.println(makePalindrome("afffasdfola"));
	}
	
	private static StringBuilder makePalindrome(String strArg){
		StringBuilder str = new StringBuilder();
		for (int i = strArg.length() - 1; i >= 0 ; i--){
			char c = strArg.charAt(i);
			str.append(c);
		}
		return str;
	}
}
