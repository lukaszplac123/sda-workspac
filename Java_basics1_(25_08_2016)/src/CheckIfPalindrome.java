import java.time.format.ResolverStyle;

public class CheckIfPalindrome {
		public static void main(String[] args){
			if (checkPalindorem("abba")) System.out.println("jest palindromem");
		}
		
		private static boolean checkPalindorem(String argStr){
			String reversed = reverse(argStr);
			return reversed.equals(argStr);
		}
		
		public static String reverse(String str) {
			String result = "";
			for(int i=str.length() - 1; i>=0;i--){
				char ch = str.charAt(i);
				result += String.valueOf(ch);
			}
			return result;
		}
		
}
