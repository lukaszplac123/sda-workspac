public class SumFromTo {
	
	public static void main(String[] args){
		System.out.println(sumFromTo(2,5));
	}
	
	private static long sumFromTo(int a, int b){
		long sum = 0;
		for (int i = a; i <= b; i++){
			sum += i;
		}
		return sum;
	}
	
}
