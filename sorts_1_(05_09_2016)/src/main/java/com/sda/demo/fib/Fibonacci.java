package com.sda.demo.fib;


public class Fibonacci {


    public static void main(String[] args) {

        //Uruchom dla n = 36,38,43.. kiedy czas oczekiwania jest nieakceptowalny ?
        int n = 43;

        System.out.println("Dla: " + n + " wartosc ciagu fibbonacciego: " + fibonacci(n));


    }


    // Jaka tutaj jest zlozonosc ?
    
    //wykadnicza klasa zlozonoscy 2^n, kazde wywolanie oblicza swoich poprzednikow wielokrotnie
    
    public static int fibonacci(int n) {
        if (n < 2) {
            return n;
        } else {
            return fibonacci(n - 1) + fibonacci(n - 2);
        }
    }


}
