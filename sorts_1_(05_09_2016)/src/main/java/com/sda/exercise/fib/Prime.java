package com.sda.exercise.fib;
//zadanie 1.3
public class Prime {

	public boolean isPrime(int number){
		
		for (int i = 2; i < number; i++){
			if ((number%i) == 0) return false;
		}
		return true;
		
	}
	
	//zadanie 1.5
	public boolean isPrimeOptimized(int number){
		
		for (int i = 2; i < Math.ceil(Math.sqrt(number)) ; i++){
			if ((number%i) == 0) return false;
		}
		return true;
		
	}
	
	//zadanie 1.6 - sito
	public boolean sitoEratosntenesa(int number){
		boolean[] boolArray = new boolean[number];
		
		boolArray[0] = true;
		boolArray[1] = true;
		
		for (int i = 2 ; i < number; i++){
			boolArray[i] = true;
		}
		
		for (int j = 2; j < number; j++){
			if (boolArray[j]){
				for (int k=2*j; k < number; k=k+j){
					boolArray[k] = false;
				}
			}
		}
		
		for (int i = 0; i < number; i++){
			if (boolArray[i]) System.out.println(i);
		}
		return true;	
	}

	
	
	//zadanie 1.4
	public void showPriemesFromTo(int start, int end){
		
		for (int i = start; i < end; i++){
			if (!this.isPrimeOptimized(i)) {} 
				else{
					System.out.println(i);
				}
		}	
	}
	
	public static void main(String[] args){
		Prime prime = new Prime();
		System.out.println(prime.isPrime(4) ?  "pierwsza" :"nie pierwsza");
		long time1 = System.currentTimeMillis();
		prime.showPriemesFromTo(0, 40000);
		System.out.println(System.currentTimeMillis() - time1);
		
		System.out.println("################");
		
		long time2 = System.currentTimeMillis();
		prime.sitoEratosntenesa(10000);
		System.out.println(System.currentTimeMillis() - time2);
	}
}
