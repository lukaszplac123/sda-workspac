package com.sda.exercise.fib;


public class DynamicFibonacci {

    public static void main(String[] args) {

        int n = 46;
        int[] tab = new int[n+1];
        System.out.println("Dla: " + n + " wartosc ciagi fibbonacciego[tablice]: " + fibonacci1(n, tab));
        System.out.println("Dla: " + n + " wartosc ciagi fibbonacciego[zmienne]: " + fibonacci2(n));

    }


    public static int fibonacci1(int n, int[] tab){
    	//Zadanie 1.1
        /*
        * Tutaj zaimplementuj funkcje dzialajaca w czasie O(n) i pamieci O(n)
        *
        * Wyniki mozesz przechowywa w tablicy rozmiar n+1
        *
        * */
   	
    	tab[0] = 0;
    	tab[1] = 1;
    	for (int i = 0; i < n+1; i++){
			if (i > 1){
    			tab[i] = tab[i-1] + tab[i-2];
    		}
    	}
    	return tab[n];
    }
    
    public static int fibonacci2(int n){
    	//Zadanie 1.2
        /*
        * Zadanie 1 bez uzycia tablicy
        * z trzema zmiennymi
        *
        * */
 
    	int first = 0;
    	int second = 1;
    	int third = 1;
    	for (int i = 0; i < n + 2; i++){
    		if (i > 1){
    			third = second + first;
    			second = first;
    			first = third;
    		}
    		
    	}
    	return third;
    }
       
}


