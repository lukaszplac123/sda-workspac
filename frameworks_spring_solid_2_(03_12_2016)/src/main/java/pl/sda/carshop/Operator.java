package pl.sda.carshop;

public class Operator {

	public void prepareData(CarShop carShop) {
		for(int i = 0; i < 5; i++) {
			Car car = new Car("Fiat", "Panda", 1000 + i);
			carShop.addCar(car);
		}
	}

	public void printData(CarShop carShop) {
		System.out.println("Salon: " + carShop.getName() + " wlasciciel: " + carShop.getOwner().getName());
		for(Car car:carShop.getCars()) {
			System.out.println(car.getMake() + " " + car.getModel() + " " + car.getPrice());
		}
	}
}
