package pl.sda.carshop;


public class Start {

	public static void main(String[] args) {
		Owner owner = new Owner("Jan Kowalski", "Krakow");
		CarShop shop = new CarShop("Salon samochodowy", owner);

		Operator operator = new Operator();

		operator.prepareData(shop);
		operator.printData(shop);
	}

}
