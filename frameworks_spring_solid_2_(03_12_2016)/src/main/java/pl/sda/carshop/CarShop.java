package pl.sda.carshop;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CarShop {

    private String name;
    private Owner owner;
    private List<Car> cars = new ArrayList<Car>();

	public CarShop(String name, Owner owner) {
		this.name = name;
		this.owner = owner;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Owner getOwner() {
		return owner;
	}

	public void setOwner(Owner owner) {
		this.owner = owner;
	}

	public List<Car> getCars() {
		return Collections.unmodifiableList(cars);
	}
	
	public void addCar(Car car) {
		cars.add(car);
	}

	public void removeCar(Car car) {
		cars.remove(car);
	}
}
