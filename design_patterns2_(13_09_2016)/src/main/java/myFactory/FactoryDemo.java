package myFactory;

public class FactoryDemo {

	public static void main(String[] args) {
		
		ShapeFactory shapeFactory = new ShapeFactory();
		
		shapeFactory.createShape("rectangle").draw();
		
		shapeFactory.createShape("circle").draw();
		
		shapeFactory.createShape("square").draw();

	}

}
