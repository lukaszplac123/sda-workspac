package myFactory;

public class ShapeFactory {
	
	public Shape createShape (String shapeName){
			
		switch (shapeName){
		
		case "rectangle" : {return new Rectangle();}
		
		case "square" : {return new Square();}
		
		case "circle" : {return new Circle();}
		
		}
		return null;
		
	}

}
