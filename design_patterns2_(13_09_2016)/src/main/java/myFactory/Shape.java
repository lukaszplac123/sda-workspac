package myFactory;

public interface Shape {

	public void draw();
	
}
