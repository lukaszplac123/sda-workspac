package myDecorator;

public class Expresso implements Drink{

	@Override
	public String getDescription() {
		
		return "Kawa expresso";
	}

	@Override
	public double calculateCosts() {
		
		return 5.2;
	}

}
