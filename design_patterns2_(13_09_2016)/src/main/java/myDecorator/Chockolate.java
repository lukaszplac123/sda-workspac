package myDecorator;

public class Chockolate extends DrinkIngredient{

	public Chockolate(Drink drink) {
		this.drink = drink;
	}
	
	@Override
	public String getDescription() {
		
		return drink.getDescription() + "+ chockolate";
	}

	@Override
	public double calculateCosts() {
		
		return drink.calculateCosts() + 1.5;
	}

		
}
