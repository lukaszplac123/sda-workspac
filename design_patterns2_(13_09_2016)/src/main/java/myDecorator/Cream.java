package myDecorator;

public class Cream extends DrinkIngredient{

	
	public Cream(Drink drink){
		this.drink = drink;
	}
	
	@Override
	public String getDescription() {
		
		return drink.getDescription() + "+ bita smietana";
	}

	@Override
	public double calculateCosts() {
		
		return drink.calculateCosts() + 2.3;
	}

}
