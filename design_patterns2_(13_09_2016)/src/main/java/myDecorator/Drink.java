package myDecorator;

public interface Drink {
	
	String getDescription();
	double calculateCosts();

}
