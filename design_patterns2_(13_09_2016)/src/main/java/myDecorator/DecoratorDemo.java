package myDecorator;

public class DecoratorDemo {

	public static void main(String[] args) {
		
		Drink drink1 = new Cream(new Chockolate(new Expresso()));

		
		System.out.println(drink1.getDescription());
		
		System.out.println(drink1.calculateCosts());
		
		
		Drink drink2 = new Chockolate(new Cream(new Expresso()));

		
		System.out.println(drink2.getDescription());
		
		System.out.println(drink2.calculateCosts());
		
		
		Drink drink3 = new Cream(new Expresso());

		
		System.out.println(drink3.getDescription());
		
		System.out.println(drink3.calculateCosts());
		
		
		Drink drink4 = new Late(new Cappucino(new Cream(new Chockolate(new Expresso()))));

		
		System.out.println(drink4.getDescription());
		
		System.out.println(drink4.calculateCosts());
	}

}
