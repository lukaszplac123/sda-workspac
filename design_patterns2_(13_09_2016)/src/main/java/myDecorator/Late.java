package myDecorator;

public class Late extends DrinkIngredient{

	public Late(Drink drink) {
		this.drink = drink;
	}
	
	@Override
	public String getDescription() {
		
		return drink.getDescription() + "+ late";
	}

	@Override
	public double calculateCosts() {
		
		return drink.calculateCosts() + 2.33;
	}

		
}
