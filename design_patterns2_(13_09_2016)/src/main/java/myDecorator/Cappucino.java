package myDecorator;

public class Cappucino extends DrinkIngredient{

	public Cappucino(Drink drink) {
		this.drink = drink;
	}
	
	@Override
	public String getDescription() {
		
		return drink.getDescription() + "+ cappucino";
	}

	@Override
	public double calculateCosts() {
		
		return drink.calculateCosts() + 1.75;
	}

		
}
