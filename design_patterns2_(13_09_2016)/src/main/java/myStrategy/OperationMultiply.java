package myStrategy;

public class OperationMultiply implements OperationStrategy{

	@Override
	public int doOperation(int a, int b) {
		
		return a * b;
	}

}
