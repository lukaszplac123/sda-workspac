package myStrategy;

public class OperationDivide implements OperationStrategy{

	@Override
	public int doOperation(int a, int b) {
		return a/b;
	}

}
