package myStrategy;

import java.util.Scanner;

public class DemoStrategy {

	public static void main (String[] args){
		
		OperationStrategy operation = null;
		
		Scanner scanner = new Scanner(System.in);
		
		while (true){
			
			System.out.print("Podaj liczbe 1 : ");
			int number1 = scanner.nextInt();
			System.out.print("Podaj liczbe 2 : ");
			int number2 = scanner.nextInt();

			System.out.println();
			
			System.out.println("1) Dodawanie ");
			System.out.println("2) Odejmowanie ");
			System.out.println("3) Mnozenie ");
			System.out.println("4) Dzielenie ");
			System.out.println("5) Koniec");
			
			int choosenOption = scanner.nextInt();
			
			switch (choosenOption){
			
			case 1 : {operation = new OperationAdd(); break;}
			case 2 : {operation = new OperationSubstract(); break;}
			case 3 : {operation = new OperationMultiply(); break;}
			case 4 : {operation = new OperationDivide(); break;}
			case 5 : {System.exit(0);}
			
			}
			System.out.println("Wynik operacji to : " + operation.doOperation(number1, number2));
		}
		
	}
	
}
