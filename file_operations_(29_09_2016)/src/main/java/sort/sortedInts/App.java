package sort.sortedInts;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * Hello world!
 *
 */
public class App {
    public static void main( String[] args ) throws IOException{
    	
       List<Integer> intList = new ArrayList<>();
       File file1 = new File ("resources/generated.txt");
       File file2 = new File("resources/sorted1.txt");
       File file3 = new File("resources/sorted2.txt");
       if (!file2.exists()){
    	  file2.createNewFile();
       }
       if (!file3.exists()){
     	  file2.createNewFile();
        }
       BufferedReader reader = new BufferedReader(new FileReader(file1));
	   BufferedWriter writer1 = new BufferedWriter(new FileWriter(file2));
	   BufferedWriter writer2 = new BufferedWriter(new FileWriter(file3));

	   String line; 
	   while ((line = reader.readLine()) != null){
	    	intList.add(Integer.parseInt(line));   
	       }
	   
	   Collections.sort(intList);
	   
	   saveValues(writer1, intList);
	   
	   Collections.reverse(intList);
       
       saveValues(writer2, intList);
       
       
       reader.close();
       writer1.close();
       writer2.close();
    }
    
    private static void saveValues(BufferedWriter writer, List<Integer> list){
    	list.forEach((item) -> {
       		try {
       			writer.write(item.toString());
       			writer.newLine();
       		} catch (Exception e) {
       			e.printStackTrace();
       		}});
    }
}
