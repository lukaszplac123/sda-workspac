package pl.sdacademy.course;

import java.io.*;
import java.text.FieldPosition;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

/**
 * Created by lukasz on 28.09.16.
 */
public class Generate {

        public void generateBigFile() {
            int capacity = 100_000;
            int counter = 1;
            int min = 1;
            Set<Integer> generatedList = new HashSet<Integer>(capacity);
            Random random = new Random();
            FileOutputStream fileOutputStream = null;
            OutputStreamWriter outputStreamWriter = null;
            File file = new File("generated.txt");
            try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)))){

                long timeBefore = System.currentTimeMillis();
                while (counter < capacity) {
                    int randomNumber = random.nextInt((Integer.MAX_VALUE - min) + 1) + min;
                   
                        generatedList.add((Integer) randomNumber);
                        writer.write(((Integer)randomNumber).toString());
                        writer.write("\n");
                        counter++;
                    
                }
                System.out.println(System.currentTimeMillis() - timeBefore);
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            } 

        }

    }
