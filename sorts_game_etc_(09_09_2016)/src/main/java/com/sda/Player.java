package com.sda;

public interface Player {
	
	int randomNumber();
	int nextGuess();

}
