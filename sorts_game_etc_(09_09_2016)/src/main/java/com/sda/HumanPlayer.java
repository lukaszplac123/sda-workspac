package com.sda;

import java.util.Scanner;

public class HumanPlayer implements Player{
	
	String name;
	
	public HumanPlayer(String name){
		this.name = name;
	}
	
	@Override
	public int randomNumber() {
		int scanInput = new Scanner(System.in).nextInt();
		return scanInput;
	}

	@Override
	public int nextGuess() {
		int scanInput = new Scanner(System.in).nextInt();
		return scanInput;
	}

}
