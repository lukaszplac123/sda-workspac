package com.sda;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PersonGenerator {
	List<Person> list;
	Person p1 = new Person("Lukasz");
	Person p2 = new Person("Lukasz");
	Person p3 = new Person("Lukasz");
	Person p4 = new Person("Lukasz");
	Person p5 = new Person("Lukasz");
	
	PersonGenerator(){
		
		list = this.getRandomPeople();
		
		}
	
	public List<Person> getRandomPeople(){
		return Arrays.asList(p1,p2,p3,p4,p5);
		
	}
		
}
