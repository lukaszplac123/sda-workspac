package com.sda;

import java.util.Random;

public class RandomCpuPlayer implements Player{

	String name;
	
	public RandomCpuPlayer(String name) {
		this.name = name;
	}
	
	@Override
	public int randomNumber() {
		Random random = new Random();
		return random.nextInt(100);
	}

	@Override
	public int nextGuess() {
		Random random = new Random();
		return random.nextInt(100);
	}
}
