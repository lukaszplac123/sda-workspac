package com.sda;

import java.time.LocalDate;

public class Person {
	
	String name;
	public Person(String name) {
		super();
		this.name = name;
	}

	LocalDate dateOfBirth; 
	
	Person(String name, LocalDate dateOfBirth){
		this.name = name;
		this.dateOfBirth = dateOfBirth;
	}
	
}
