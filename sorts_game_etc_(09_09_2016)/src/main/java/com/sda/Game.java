package com.sda;

public class Game {
	
	private int attempts = 0;
	private int winningNumber;
	
	Game(int winningNumber){
		attempts = 0;
		this.winningNumber = winningNumber;
	}
	
	int isWinningNumber (Integer number){
		if (number == this.winningNumber ) return 0;
		else if (number > this.winningNumber ) return -1;
		else return 1;
	}
	
	public void setAttempts() {
		this.attempts++;
	}

	public int getAttempts() {
		return attempts;
	}		
}
