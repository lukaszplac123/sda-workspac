package com.sda;

public class BinarySearch {

	public static void main (String[] args){
		
		int myNumber = 65;
		Integer cpuChoice = -100;
		Integer myChoice = -100;
		
		
		
		RandomCpuPlayer cpuPlayer = new RandomCpuPlayer("Komputer");
		HumanPlayer humanPlayer = new HumanPlayer("Lukasz");
		Game game = new Game(myNumber);
		
		while (game.isWinningNumber(cpuChoice) != 0){
			cpuChoice = cpuPlayer.nextGuess();
			System.out.println("Komputer : "+ cpuChoice);
			game.setAttempts();
		}
		System.out.println("-------");
		System.out.println("podejsc -> " + game.getAttempts());
		
		Game game2 = new Game(cpuPlayer.randomNumber());
		
		while (game2.isWinningNumber(myChoice) != 0){
			System.out.println("Zgaduj");
			myChoice = humanPlayer.nextGuess();
			game2.setAttempts();
		}
		System.out.println("-------");
		System.out.println("podejsc -> " + game.getAttempts());
	}
}
