package pl.sda.spring.config;

import org.dbunit.database.DatabaseDataSourceConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import javax.sql.DataSource;

@ContextConfiguration(classes = PostgresqlConfig.class)
public abstract class DatabaseTest {

    @Autowired
    protected DataSource dataSource;
    private String dbImportFileLocation = "/sql/import.xml";

    public DatabaseTest() {
    }

    public DatabaseTest(String dbImportFileLocation) {
        this.dbImportFileLocation = dbImportFileLocation;
    }

    @Before
    public void setUp() throws Exception {
        IDatabaseConnection dbConn = new DatabaseDataSourceConnection(dataSource);
        DatabaseOperation.CLEAN_INSERT.execute(dbConn, getDataSet());
        init();
   }

    public abstract void init();

    private IDataSet getDataSet() throws Exception{
        return new FlatXmlDataSetBuilder().build(this.getClass().getResource(dbImportFileLocation).openStream());
    }
}
