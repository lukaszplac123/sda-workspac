package pl.sda.spring.jpa;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import pl.sda.model.Employee;
import pl.sda.spring.config.DatabaseTest;

import java.util.List;
import java.util.Optional;

import static org.fest.assertions.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = JpaAppTestConfig.class)
public class JpaEmployeeDaoTest extends DatabaseTest {

    @Autowired
    private JpaEmployeeDao dao;

    @Override
    public void init() {

    }

    @Test
    public void thatCanCreateEmployee() {
        Employee newEmployee = Employee.builder().name("Maciej Zurawski").role("PLAYER").build();

        boolean isSuccess = dao.create(newEmployee);
        assertThat(isSuccess).isTrue();

        Optional<Employee> maybePersistedEmployee = dao.getAll().stream()
                .filter(employee -> employee.getId() == newEmployee.getId())
                .findFirst();
        assertThat(maybePersistedEmployee.isPresent()).isTrue();
    }

    @Test
    public void thatCanGetAllEmployees() {
        List<Employee> foundEmployees = dao.getAll();
        assertThat(foundEmployees.size()).isEqualTo(3);
    }
}
