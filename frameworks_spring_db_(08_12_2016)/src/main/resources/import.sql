CREATE TABLE Employee (
  id serial PRIMARY KEY,
  name varchar(20) NOT NULL,
  role varchar(20) NOT NULL
);