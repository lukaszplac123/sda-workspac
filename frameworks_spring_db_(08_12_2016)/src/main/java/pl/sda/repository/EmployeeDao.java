package pl.sda.repository;

import pl.sda.model.Employee;

import java.util.List;

public interface EmployeeDao {

    String createQuery = "INSERT INTO EMPLOYEE(id, name, role) VALUES (?, ?, ?)";
    String getAll = "SELECT * FROM EMPLOYEE";
    String deleteQuery = "DELETE FROM EMPLOYEE WHERE id = ?";

    boolean create(Employee employee);
    List<Employee> getAll();
    boolean delete(Employee employee);
}
