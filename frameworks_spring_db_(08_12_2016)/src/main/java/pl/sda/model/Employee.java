package pl.sda.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "EMPLOYEE")
@NamedQuery(
        name = "Employee.findByNameAndRoleNamedQuery",
        query = "SELECT e FROM Employee e WHERE e.name = ?1 AND e.role = ?2")
public class Employee {

    @Id
    @SequenceGenerator(name = "pk_sequence", sequenceName = "employee_id_seq", allocationSize=1)
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "pk_sequence")
    @Column(name = "ID", unique = true, nullable = false, updatable = false)
    private int id;
    @Column(name = "name", nullable = false)
    private String name;
    @Column(name = "role", nullable = false)
    private String role;

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getRole() {
        return role;
    }
    public void setRole(String role) {
        this.role = role;
    }

    public static EmployeeBuilder builder() {
        return new EmployeeBuilder();
    }

    @Override
    public String toString(){
        return "{ ID = " + id + ", Name = " + name + ", Role = " + role + " }";
    }

    public static class EmployeeBuilder {
        private final Employee employee = new Employee();

        public EmployeeBuilder id(int id) {
            employee.id = id;
            return this;
        }

        public EmployeeBuilder name(String name) {
            employee.name = name;
            return this;
        }

        public EmployeeBuilder role(String role) {
            employee.role = role;
            return this;
        }

        public Employee build() {
            return employee;
        }
    }
}
