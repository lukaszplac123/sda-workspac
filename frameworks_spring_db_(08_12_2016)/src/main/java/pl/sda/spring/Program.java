package pl.sda.spring;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import pl.sda.model.Employee;
import pl.sda.repository.EmployeeDao;
import pl.sda.spring.jdbc.JdbcAppConfig;
import pl.sda.spring.template.TemplateAppConfig;

public class Program {

    private static final Logger logger = LoggerFactory.getLogger(Program.class);
    private final EmployeeDao dao;

    public Program(EmployeeDao dao) {
        this.dao = dao;
    }

    public void test() {
        Employee employee = Employee.builder().id(1).name("Maciej Zurawski").role("PLAYER").build();

        dao.create(employee);
        dao.getAll().forEach(foundEmployee -> logger.info(foundEmployee.toString()));
        dao.delete(employee);
    }

    public static void main(String[] args) {
        ApplicationContext contexts[] = {
                new AnnotationConfigApplicationContext(JdbcAppConfig.class),
                new AnnotationConfigApplicationContext(TemplateAppConfig.class),
        };

        for(ApplicationContext context : contexts) {
            context.getBean(Program.class).test();
        }
    }
}
