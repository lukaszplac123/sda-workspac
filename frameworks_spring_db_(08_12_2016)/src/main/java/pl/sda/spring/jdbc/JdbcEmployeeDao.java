package pl.sda.spring.jdbc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.sda.model.Employee;
import pl.sda.repository.EmployeeDao;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;

public class JdbcEmployeeDao implements EmployeeDao {

    private static final Logger logger = LoggerFactory.getLogger(JdbcEmployeeDao.class);
    private final DataSource dataSource;

    public JdbcEmployeeDao(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public boolean create(Employee employee) {
        return executeDml(employee, createQuery, createExecutor());
    }

    @Override
    public List<Employee> getAll() {
        return executeDql(getAll, dqlExecutor());
    }

    @Override
    public boolean delete(Employee employee) {
        return executeDml(employee, deleteQuery, deleteExecutor());
    }

    private boolean executeDml(Employee employee,
                               String query,
                               BiFunction<PreparedStatement, Employee, Boolean> executor
                               ) {
        return createConnection()
                .flatMap(connection -> createStatement(connection, query))
                .map(statement -> executor.apply(statement, employee))
                .orElse(false);
    }

    private List<Employee> executeDql(String query,
                                      Function<PreparedStatement, List<Employee>> executor) {
        return createConnection()
                .flatMap(connection -> createStatement(connection, query))
                .map(executor)
                .orElseGet(Collections::emptyList);
    }

    private BiFunction<PreparedStatement, Employee, Boolean> createExecutor() {
        return (statement, employee) -> {
            try {
                statement.setInt(1, employee.getId());
                statement.setString(2, employee.getName());
                statement.setString(3, employee.getRole());

                boolean success = statement.executeUpdate() > 0;
                if(success) {
                    logger.info("New employee saved");
                } else {
                    logger.info("Could not save new employee");
                }
                return success;
            } catch (SQLException ex) {
                logger.warn(ex.getMessage(), ex);
                return false;
            } finally {
                cleanUp(statement);
            }
        };
    }

    private Function<PreparedStatement, List<Employee>> dqlExecutor() {
        return (statement) -> {
            try {
                List<Employee> employees = new ArrayList<>();

                ResultSet results = statement.executeQuery();
                while(results.next()) {
                    employees.add(Employee.builder()
                            .id(results.getInt(1))
                            .name(results.getString(2))
                            .role(results.getString(2))
                            .build());
                }
                return employees;
            } catch (SQLException ex) {
                logger.warn(ex.getMessage(), ex);
                return Collections.emptyList();
            } finally {
                cleanUp(statement);
            }
        };
    }

    private BiFunction<PreparedStatement, Employee, Boolean> deleteExecutor() {
        return (statement, employee) -> {
            try {
                statement.setInt(1, employee.getId());

                boolean success = statement.executeUpdate() > 0;
                if (success) {
                    logger.info("Employee deleted");
                } else {
                    logger.info("Employee was not deleted");
                }
                return success;
            } catch (SQLException ex) {
                logger.warn(ex.getMessage(), ex);
                return false;
            } finally {
                cleanUp(statement);
            }
        };
    }

    private Optional<Connection> createConnection() {
        try {
            return Optional.of(dataSource.getConnection());
        } catch (SQLException ex) {
            logger.warn(ex.getMessage(), ex);
            return Optional.empty();
        }
    }

    private Optional<PreparedStatement> createStatement(Connection connection, String sql) {
        try {
            return Optional.of(connection.prepareStatement(sql));
        } catch (SQLException ex) {
            logger.warn(ex.getMessage(), ex);
            return Optional.empty();
        }
    }

    private void cleanUp(Statement statement) {
        try {
            statement.close();
            statement.getConnection().close();
        } catch (SQLException ex) {
            logger.warn(ex.getMessage(), ex);
        }
    }
}
