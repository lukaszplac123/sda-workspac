package pl.sda.spring.jdbc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import pl.sda.spring.Program;
import pl.sda.repository.EmployeeDao;

import javax.sql.DataSource;

@Configuration
public class JdbcAppConfig {

    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("org.postgresql.Driver");
        dataSource.setUrl("jdbc:postgresql://192.168.99.100:2345/company");
        dataSource.setUsername("postgres");
        dataSource.setPassword("postgres");

        return dataSource;
    }

    @Bean
    public EmployeeDao employeeDao(@Autowired DataSource dataSource) {
        return new JdbcEmployeeDao(dataSource);
    }

    @Bean
    public Program program(@Autowired EmployeeDao employeeDao) {
        return new Program(employeeDao);
    }
}
