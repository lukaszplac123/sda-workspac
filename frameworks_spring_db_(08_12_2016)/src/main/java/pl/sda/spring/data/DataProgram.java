package pl.sda.spring.data;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.model.Employee;
import pl.sda.repository.EmployeeDao;

public class DataProgram {

    private static final Logger logger = LoggerFactory.getLogger(DataProgram.class);
    private final EmployeeDao dao;
    private final EmployeeRepository repository;

    public DataProgram(EmployeeDao dao, EmployeeRepository repository) {
        this.dao = dao;
        this.repository = repository;
    }

    @Transactional
    public void test() {
        Employee employee = Employee.builder().name("Maciej Zurawski").role("PLAYER").build();

        dao.create(employee);
        dao.getAll().forEach(foundEmployee -> logger.info(foundEmployee.toString()));
        dao.delete(employee);

        Employee foundEmployee;
        foundEmployee = repository.findByName("Maciej Zurawski");
        foundEmployee = repository.findByNameAndRole("Maciej Zurawski", "PLAYER");
        foundEmployee = repository.findByNameAndRoleQuery("Maciej Zurawski", "PLAYER");
        foundEmployee = repository.findByNameAndRoleNamedQuery("Maciej Zurawski", "PLAYER");
    }

    public static void main(String[] args) {
        new AnnotationConfigApplicationContext(DataAppConfig.class)
                .getBean(DataProgram.class)
                .test();
    }
}
