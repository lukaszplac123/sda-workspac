package pl.sda.spring.data;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.sda.model.Employee;
import pl.sda.repository.EmployeeDao;

import java.util.List;

public class DataEmployeeDao implements EmployeeDao {

    private static final Logger logger = LoggerFactory.getLogger(DataEmployeeDao.class);
    private final EmployeeRepository repository;

    public DataEmployeeDao(EmployeeRepository repository) {
        this.repository = repository;
    }

    @Override
    public boolean create(Employee employee) {
        repository.save(employee);
        return true;
    }

    @Override
    public List<Employee> getAll() {
        return repository.findAll();
    }

    @Override
    public boolean delete(Employee employee) {
        repository.delete(employee);
        return true;
    }
}
