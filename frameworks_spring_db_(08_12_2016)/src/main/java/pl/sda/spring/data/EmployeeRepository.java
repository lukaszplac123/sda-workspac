package pl.sda.spring.data;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import pl.sda.model.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
    Employee findByName(String name);
    Employee findByNameAndRole(String name, String role);

    @Query(value = "SELECT e FROM Employee e WHERE e.name = ?1 AND e.role = ?2")
    Employee findByNameAndRoleQuery(String name, String role);
    Employee findByNameAndRoleNamedQuery(String name, String role);
}
