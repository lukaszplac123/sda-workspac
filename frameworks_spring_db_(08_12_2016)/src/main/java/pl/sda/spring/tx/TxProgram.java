package pl.sda.spring.tx;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.model.Employee;
import pl.sda.repository.EmployeeDao;

public class TxProgram {

    private static final Logger logger = LoggerFactory.getLogger(TxProgram.class);
    private final EmployeeDao dao;

    public TxProgram(EmployeeDao dao) {
        this.dao = dao;
    }

    @Transactional
    public void test() {
        Employee employee = Employee.builder().id(999).name("Maciej Zurawski").role("PLAYER").build();

        dao.create(employee);
        throw new RuntimeException("Sth bad happened !!!");
    }

    public static void main(String[] args) {
            new AnnotationConfigApplicationContext(TxAppConfig.class)
                    .getBean(TxProgram.class)
                    .test();
    }
}
