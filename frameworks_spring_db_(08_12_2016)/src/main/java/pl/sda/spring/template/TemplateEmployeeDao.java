package pl.sda.spring.template;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import pl.sda.model.Employee;
import pl.sda.repository.EmployeeDao;

import java.util.Collections;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;

public class TemplateEmployeeDao implements EmployeeDao {

    private static final Logger logger = LoggerFactory.getLogger(TemplateEmployeeDao.class);
    private final JdbcTemplate jdbcTemplate;

    public TemplateEmployeeDao(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public boolean create(Employee employee) {
        return createExecutor().apply(createQuery, employee);
    }

    @Override
    public List<Employee> getAll() {
        return dqlExecutor().apply(getAll);
    }

    @Override
    public boolean delete(Employee employee) {
        return deleteExecutor().apply(deleteQuery, employee);
    }

    private BiFunction<String, Employee, Boolean> createExecutor() {
        return (query, employee) -> {
            try {
                boolean success = jdbcTemplate
                        .update(query, employee.getId(), employee.getName(), employee.getRole()) > 0;
                if(success) {
                    logger.info("New employee saved");
                } else {
                    logger.info("Could not save new employee");
                }
                return success;
            } catch (DataAccessException ex) {
                logger.warn(ex.getMessage(), ex);
                return false;
            }
        };
    }

    private Function<String, List<Employee>> dqlExecutor() {
        return (query) -> {
            try {
                return jdbcTemplate.query(query, (resultSet, i) -> Employee.builder()
                        .id(resultSet.getInt(1))
                        .name(resultSet.getString(2))
                        .role(resultSet.getString(2))
                        .build());
            } catch (DataAccessException ex) {
                logger.warn(ex.getMessage(), ex);
                return Collections.emptyList();
            }
        };
    }

    private BiFunction<String, Employee, Boolean> deleteExecutor() {
        return (query, employee) -> {
            try {
                boolean success = jdbcTemplate
                        .update(query, employee.getId()) > 0;
                if (success) {
                    logger.info("Employee deleted");
                } else {
                    logger.info("Employee was not deleted");
                }
                return success;
            } catch (DataAccessException ex) {
                logger.warn(ex.getMessage(), ex);
                return false;
            }
        };
    }
}
