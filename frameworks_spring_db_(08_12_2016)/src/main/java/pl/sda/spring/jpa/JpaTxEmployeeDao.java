package pl.sda.spring.jpa;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.model.Employee;
import pl.sda.repository.EmployeeDao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Transactional
public class JpaTxEmployeeDao implements EmployeeDao {

    private static final Logger logger = LoggerFactory.getLogger(JpaTxEmployeeDao.class);

    @PersistenceContext
    private EntityManager em;

    @Override
    public boolean create(Employee employee) {
        em.persist(employee);
        return true;
    }

    @Override
    public List<Employee> getAll() {
        TypedQuery<Employee> q = em.createQuery("FROM Employee e", Employee.class);
        return q.getResultList();
    }

    private Employee getById(int id) {
        return em.find(Employee.class, id);
    }

    @Override
    public boolean delete(Employee employee) {
        em.remove(getById(employee.getId()));
        return true;
    }
}
