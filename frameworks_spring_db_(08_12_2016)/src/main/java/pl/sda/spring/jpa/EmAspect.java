package pl.sda.spring.jpa;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.orm.jpa.EntityManagerHolder;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

@Aspect
public class EmAspect {

    private final EntityManagerFactory entityManagerFactory;
    private final ThreadLocal<EntityManager> entityManagerThreadLocal;

    public EmAspect(EntityManagerFactory entityManagerFactory, ThreadLocal<EntityManager> entityManagerThreadLocal) {
        this.entityManagerFactory = entityManagerFactory;
        this.entityManagerThreadLocal = entityManagerThreadLocal;
    }

    @Around("execution(* pl.sda.spring.jpa.JpaAspectEmployeeDao.*(..))")
    public Object handleEntityManager(ProceedingJoinPoint pjp) throws Throwable {
        EntityManager em = entityManagerFactory.createEntityManager();
        entityManagerThreadLocal.set(em);

        em.getTransaction().begin();

        try {
            Object result = pjp.proceed(pjp.getArgs());
            em.getTransaction().commit();

            return result;
        } catch (Throwable throwable) {
            em.getTransaction().rollback();
            throw throwable;
        } finally {
            em.close();
            entityManagerThreadLocal.remove();
        }
    }
}
