package pl.sda.spring.jpa;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import pl.sda.model.Employee;
import pl.sda.repository.EmployeeDao;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class JpaAspectEmployeeDao implements EmployeeDao {

    private static final Logger logger = LoggerFactory.getLogger(JpaAspectEmployeeDao.class);

    private final ThreadLocal<EntityManager> entityManagerThreadLocal;

    public JpaAspectEmployeeDao(ThreadLocal<EntityManager> entityManagerThreadLocal) {
        this.entityManagerThreadLocal = entityManagerThreadLocal;
    }

    @Override
    public boolean create(Employee employee) {
        entityManagerThreadLocal.get().persist(employee);
        return true;
    }

    @Override
    public List<Employee> getAll() {
        TypedQuery<Employee> q = entityManagerThreadLocal.get().createQuery("FROM Employee e", Employee.class);
        return q.getResultList();
    }

    private Employee getById(int id) {
        return entityManagerThreadLocal.get().find(Employee.class, id);
    }

    @Override
    public boolean delete(Employee employee) {
        entityManagerThreadLocal.get().remove(getById(employee.getId()));
        return true;
    }
}
