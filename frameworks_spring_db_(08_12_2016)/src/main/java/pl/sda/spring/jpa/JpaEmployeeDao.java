package pl.sda.spring.jpa;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.model.Employee;
import pl.sda.repository.EmployeeDao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import java.util.List;

public class JpaEmployeeDao implements EmployeeDao {

    private static final Logger logger = LoggerFactory.getLogger(JpaEmployeeDao.class);
    private final EntityManagerFactory emf;

    public JpaEmployeeDao(EntityManagerFactory emf) {
        this.emf = emf;
    }

    @Override
    public boolean create(Employee employee) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        em.persist(employee);

        em.getTransaction().commit();
        em.close();
        return true;
    }

    @Override
    public List<Employee> getAll() {
        EntityManager em = emf.createEntityManager();

        TypedQuery<Employee> q = em.createQuery("FROM Employee e", Employee.class);
        List<Employee> results = q.getResultList();

        em.close();
        return results;
    }

    @Override
    public boolean delete(Employee employee) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        em.remove(em.find(Employee.class, employee.getId()));

        em.getTransaction().commit();
        em.close();
        return true;
    }
}
