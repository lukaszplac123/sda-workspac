package pl.sda.spring.jpa;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.transaction.annotation.Transactional;
import pl.sda.model.Employee;
import pl.sda.repository.EmployeeDao;

public class JpaProgram {

    private static final Logger logger = LoggerFactory.getLogger(JpaProgram.class);
    private final EmployeeDao dao;

    public JpaProgram(EmployeeDao dao) {
        this.dao = dao;
    }

    public void test() {
        Employee employee = Employee.builder().name("Maciej Zurawski").role("PLAYER").build();

        dao.create(employee);
        dao.getAll().forEach(foundEmployee -> logger.info(foundEmployee.toString()));
        dao.delete(employee);
    }

    public static void main(String[] args) {
        new AnnotationConfigApplicationContext(JpaAppConfig.class)
                .getBean(JpaProgram.class)
                .test();
    }
}
