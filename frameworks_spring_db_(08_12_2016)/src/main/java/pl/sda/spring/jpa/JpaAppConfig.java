package pl.sda.spring.jpa;

import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import pl.sda.repository.EmployeeDao;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableAspectJAutoProxy
@EnableTransactionManagement
@PropertySource("classpath:application.properties")
public class JpaAppConfig {

    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("org.postgresql.Driver");
        dataSource.setUrl("jdbc:postgresql://192.168.99.100:2345/company");
        dataSource.setUsername("postgres");
        dataSource.setPassword("postgres");

        return dataSource;
    }

    @Bean
    public HibernateProperties hibernateProperties(Environment env) {
        return new HibernateProperties(env);
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource, HibernateProperties properties) {
        LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
        entityManagerFactoryBean.setDataSource(dataSource);
        entityManagerFactoryBean.setPersistenceProviderClass(HibernatePersistenceProvider.class);
        entityManagerFactoryBean.setPackagesToScan("pl.sda.model");


        entityManagerFactoryBean.setJpaProperties(properties.toProperties());

        return entityManagerFactoryBean;
    }

    @Bean
    public JpaTransactionManager transactionManager(@Autowired LocalContainerEntityManagerFactoryBean entityManagerFactory) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory.getObject());
        return transactionManager;
    }

    @Bean
    public JpaProgram program(@Autowired EmployeeDao employeeDao) {
        return new JpaProgram(employeeDao);
    }

//    @Bean
//    public EmployeeDao employeeDao(@Autowired JpaTransactionManager transactionManager) {
//        return new JpaEmployeeDao(transactionManager.getEntityManagerFactory());
//    }

//    @Bean
//    public EmployeeDao employeeDao(@Autowired JpaTransactionManager transactionManager) {
//        return new JpaTxEmployeeDao();
//    }


    @Bean
    public EmployeeDao employeeDao(ThreadLocal<EntityManager> entityManagerThreadLocal) {
        return new JpaAspectEmployeeDao(entityManagerThreadLocal);
    }

    @Bean
    public ThreadLocal<EntityManager> entityManagerThreadLocal() {
        return new ThreadLocal<>();
    }

    @Bean
    public EmAspect emAspect(@Autowired EntityManagerFactory entityManagerFactory,
                             @Autowired ThreadLocal<EntityManager> entityManagerThreadLocal) {
        return new EmAspect(entityManagerFactory, entityManagerThreadLocal);
    }
}
