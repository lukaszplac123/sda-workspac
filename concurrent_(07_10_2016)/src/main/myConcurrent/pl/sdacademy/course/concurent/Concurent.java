package pl.sdacademy.course.concurent;

import java.util.List;

/**
 * Hello world!
 *
 */
public class Concurent{
    public static void main( String[] args ){
      
    	Thread modifyingThread = new Thread(new ModifyingThread());
    	Thread displayingThread = new Thread(new DisplayingThread());
    	
    	RandomStrings randomStrings = new RandomStrings();
    	
    	List<String> list = randomStrings.createRandomStrings(20);
    	
    	list.forEach((item) -> System.out.println(item));
    	
    	
    }
}
