package pl.sdacademy.course.concurent;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RandomStrings {

	private List<String> list;
	
	public RandomStrings() {
		list = new ArrayList<String>();
	}
	
	public List<String> createRandomStrings(int size){
		Random random1 = new Random();
		Random random2 = new Random();
		
		for (int i =  0 ; i < size ; i++){
			StringBuilder builder = new StringBuilder();
			for (int j = 0 ; j < random2.nextInt(60) + 10 ; j++){
				char c = (char) (random1.nextInt(25) + 65);
				builder.append(c);
			}
			list.add(builder.toString());
		}
		return list;
	}
	
}
