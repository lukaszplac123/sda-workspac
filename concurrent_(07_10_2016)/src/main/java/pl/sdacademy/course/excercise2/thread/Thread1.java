package pl.sdacademy.course.excercise2.thread;

import java.util.Map;

import pl.sdacademy.course.generate.DataGeneration;
import pl.sdacademy.course.model.Student;

public class Thread1 implements Runnable{

	private String name;
	private Map<String, Student> studentsMap;

	
	
	public Thread1(String name, Map<String, Student> studentsMap) {
		this.name = name;
		this.studentsMap = studentsMap;
	}

	@Override
	public void run() {
		DataGeneration dataGeneration = new DataGeneration();
		for (int i = 50; i < 70 ; i++){
			Student student = dataGeneration.createStudent(i);
			student.setSurname(name + i);
			studentsMap.putIfAbsent(student.getName(), student);
			}
		
		}
	
}

