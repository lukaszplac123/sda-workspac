package pl.sdacademy.course.excercise1;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import pl.sdacademy.course.excercise2.thread.Thread1;
import pl.sdacademy.course.generate.DataGeneration;
import pl.sdacademy.course.model.Student;

public class Excercise2 {
	
	public static void main (String[] args) throws InterruptedException{
		DataGeneration dataGeneration = new DataGeneration();
		Map<String, Student> studentsMap = dataGeneration.generateStudentsMap(50);
		ConcurrentHashMap<String, Student> concurrentHashMap = new ConcurrentHashMap<>(studentsMap);
		
		Thread threadZdzisiek = new Thread(new Thread1("Zdzisiek", concurrentHashMap));
		Thread threadZbyszek = new Thread(new Thread1("Zbyszek", concurrentHashMap));
		
		threadZbyszek.start();
		threadZdzisiek.start();
		
		threadZbyszek.join(); //watek main poczeka na skonczenie watkow Zdziska i Zbyszka
		threadZdzisiek.join();
		concurrentHashMap.forEach((key, value) -> System.out.println(value));
	}
}
