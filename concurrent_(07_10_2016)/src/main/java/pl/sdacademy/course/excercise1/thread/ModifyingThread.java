package pl.sdacademy.course.excercise1.thread;

import pl.sdacademy.course.model.Student;

import java.util.Set;

public class ModifyingThread implements Runnable{

    private Set<Student> students;

    public ModifyingThread(Set<Student> students) {
        this.students = students;
    }

    @Override
    public void run() {
        for (int i = 0; i < 30; i++) {
            Student student = new Student();
            student.setName("Jan" + i);
            student.setSurname("Kowalski" + i);
            student.setEmail("jan.kowalski" + i + "@gmail.com");
            students.add(student);
        }
    }
}
