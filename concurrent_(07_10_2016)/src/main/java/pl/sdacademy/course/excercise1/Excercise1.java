package pl.sdacademy.course.excercise1;

import pl.sdacademy.course.excercise1.thread.DisplayingThread;
import pl.sdacademy.course.excercise1.thread.ModifyingThread;
import pl.sdacademy.course.generate.DataGeneration;
import pl.sdacademy.course.model.Student;

import java.util.Collections;
import java.util.Set;

public class Excercise1 {


    public static void main(String[] args) {
        DataGeneration dataGeneration = new DataGeneration();
        Set<Student> students = Collections.synchronizedSet(dataGeneration.generateStudentsSet(50));

        Thread displayingThread = new Thread(new DisplayingThread(students));
        Thread modifyingThread = new Thread(new ModifyingThread(students));

        modifyingThread.start();
        displayingThread.start();

        try {
            modifyingThread.join();
            displayingThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
