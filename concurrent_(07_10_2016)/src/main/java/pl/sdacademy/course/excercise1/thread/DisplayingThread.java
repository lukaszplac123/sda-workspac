package pl.sdacademy.course.excercise1.thread;

import pl.sdacademy.course.model.Student;

import java.util.Iterator;
import java.util.Set;

public class DisplayingThread implements Runnable{

    public Set<Student> students;

    public DisplayingThread(Set<Student> students) {
        this.students = students;
    }

    @Override
    public void run() {
        synchronized (students) {
            Iterator<Student> studentIterator = students.iterator();
            while (studentIterator.hasNext())
                System.out.println(studentIterator.next());
        }
    }
}
