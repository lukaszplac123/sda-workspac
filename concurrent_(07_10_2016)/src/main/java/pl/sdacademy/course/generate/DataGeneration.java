package pl.sdacademy.course.generate;

import pl.sdacademy.course.model.Student;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class DataGeneration {

    public Set<Student> generateStudentsSet(int dataAmount) {
        HashSet<Student> students = new HashSet<>();
        for (int i = 0; i < dataAmount; i++) {
            students.add(createStudent(i));
        }
        return students;
    }
    
    public Map<String, Student> generateStudentsMap (int dataAmount){
    	HashMap<String, Student> students = new HashMap<>();
    	 for (int i = 0; i < dataAmount; i++) {
             Student student = createStudent(i);
             students.put(student.getName(), student);
         }
    	return students;
    	
    }

	public Student createStudent(int i) {
		Student student = new Student();
		 student.setName("Jan" + i);
		 student.setSurname("Kowalski" + i);
		 student.setEmail("jan.kowalski" + i + "@gmail.com");
		return student;
	}

}
