package cwiczenie1;


/**Class implements three different functions basing on input passed by user as arguments directly through
 * executable jar file 
 * **/ 
public class Calculator {	
	
	/**constant number
	 * **/ 
	final double phi = 1.61803398874895;
	
	/**main method. Allows user to pass 3 arguments.
	 * 1 - a argument that should be int number
	 * 2 - b argument that should be double number
	 * 3 - optional constant. If it is not passed the default value is set to 1.
	 * **/ 
	public static void main(String[] args) {
		Calculator calculator = new Calculator();
		int a = 0;
		try{
			a = Integer.parseInt(args[0]);
		}catch (NumberFormatException e){
			System.out.println("a - nieprawidlowy format. \"a\" musi byc typu int");
			System.exit(1);
		}
		double b = 0;
		try{
			b = Double.parseDouble(args[1]);
		}catch (NumberFormatException e){
			System.out.println("b - nieprawidlowy format. \"b\" musi byc typu double");
			System.exit(0);
		}
		short M;
			if (args.length > 2) {
				M = (short) Integer.parseInt(args[2]);
			} else {
				System.out.println("Nie podano 3-go argumentu. Domyslnie M = 1");
				M = 1;
			}
		System.out.println("M="+M);
		System.out.println("function 1 : " + calculator.functionHash(a, b, M));
		System.out.println("function 2 : " + calculator.functionAt(a, b, M));
		System.out.println("function 3 : " + calculator.functionDollar(a, b, M));
	}

	/**
	 * function hash
	 * **/ 
	public double functionHash(int a, double b, short M){
		return M + (a+b)*(2*a - b);
	}
	
	/**
	 * function at
	 * **/ 
	public double functionAt(int a, double b, short M){
		return Math.pow(functionHash(a, b, M), 2) + (Math.sqrt(Math.pow(a, 3)))/Math.abs(b) + (Math.cos(b) * phi) - M;
	}
	
	/**
	 * function Dollar
	 * **/ 
	public double functionDollar(int a, double b, short M){
		return M * (functionHash(a, b, M)) - (M * functionAt(a, b, M));	
	}
}
