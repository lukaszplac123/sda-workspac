package cwiczenie4.class1;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collector;

public class PersonList implements Comparator<Person>{
	
	List<Person> list;
	
	public PersonList() {
		list = new ArrayList<Person>();
				
	}

	@Override
	public int compare(Person o1, Person o2) {
		return o1.name.compareTo(o2.name);
	}

	
}
