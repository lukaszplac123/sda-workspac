package cwiczenie4.class1;

import java.text.Collator;
import java.util.Comparator;
import java.util.Locale;

public class Test {

	public static void main(String[] args) {
		Test test = new Test();
		PersonList personList = new PersonList();
		
		Locale locale = new Locale("Polish", "Poland");
		Locale defaultLocale = Locale.getDefault();
		System.out.println("Default locale:"+defaultLocale);
		System.out.println("Polish locale:"+locale);
		
		Locale[] all = Locale.getAvailableLocales();
		int i=0;
		for (Locale loc : all){
			System.out.println(i++ +":"+loc);
		}
		
		Person p1 = new Person("Ąa", 5);
		Person p2 = new Person("aA", 5);
		Person p3 = new Person("a", 3);
		Person p4 = new Person("b", 6);
		Person p5 = new Person("b", 2);
		personList.list.add(p1);
		personList.list.add(p2);
		personList.list.add(p3);
		personList.list.add(p4);
		personList.list.add(p5);
		//sortowanie dla default locale
		personList.list.sort(personList);
		
		//sortowanie dla polskich locali
		//Collator collator = Collator.getInstance(locale);
		
		personList.list.forEach(item -> System.out.println(item.name));
	}

}
