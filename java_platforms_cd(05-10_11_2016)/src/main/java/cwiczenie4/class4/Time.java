package cwiczenie4.class4;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;

public class Time {

	public static void main(String[] args) {
		LocalDate date = LocalDate.now();
		LocalDateTime time = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MM yyyy");
		String text = date.format(formatter);

		System.out.println(time);
		System.out.println(date);
		System.out.println(text);
		
		//LocalDate dateParsed = LocalDate.parse(text, formatter);
	}
}
