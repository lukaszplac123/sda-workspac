
package intvalues;

public class Main {

	//kilka mozliwosci zapisu integera
	public static void main(String[] args) {
		int a;
		a = 15;
		System.out.println("1:"+a);
		a = (int) 15L;
		System.out.println("2:"+a);
		a = 0xF;
		System.out.println("3:"+a);
		a = 017;
		System.out.println("4:"+a);
		a = 0b1111;
		System.out.println("5:"+a);
	}

}
