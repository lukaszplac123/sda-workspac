package cwiczenie2;

import java.util.Random;

public class MyArray {
	double[] array;
	
	public MyArray(int size) {
		array = new double[size];
		Random random = new Random();
		for (int i = 0 ; i < array.length; i++){
			array[i] = (double) (random.nextInt(120) - 40);
		}
	}
	
	public void operation1(){
		for (int i = 0; i < array.length; i++){
			if (i%3 == 0) {
				array[i] = Math.pow(array[i] * 2, i);
			}
		}
	}
	
	public void operation2(){
		for (int i = 0; i < array.length; i++){
			if (i%10 == 0) {
				array[i] = array[i]+1;
			}
		}
	}
	
	public void operation3(){
		for (int i = 0; i < array.length; i++){
			if (i%3 == 0) {
				array[i] = array[i]/(-3);
			}
		}
	}
	
	public double operation4(){
		double sum = 0;
		for (int i = 0; i < array.length; i++){
			if ((array[i] > -20.0) && (array[i] < 20.0)) {
				sum = sum + array[i];
			}
		}
		return sum;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < array.length; i++){
			builder.append(array[i]+"|");
		}
		return builder.toString();
	}
}
