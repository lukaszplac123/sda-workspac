package cwiczenie2;

import java.text.DecimalFormat;

public class Test {
	public static void main(String[] args) {
		int size = 0;
		DecimalFormat df = new DecimalFormat("#.###");
		try{
			size = Integer.parseInt(args[0]);
		}catch (NumberFormatException e){
			System.out.println("Nie podano dlugosci tablicy. Uruchom jeszcze raz.");
		}
		MyArray array = new MyArray(size);
		System.out.println("Poczatkowa tablica:"+array.toString());
		array.operation1();
		System.out.println("Operacja1:"+array.toString());
		array.operation2();
		System.out.println("Operacja2:"+array.toString());
		array.operation3();
		System.out.println("Operacja3:"+array.toString());
		double sum = array.operation4();
		System.out.println("Operacja4:"+df.format(array.operation4()));
	}
}
