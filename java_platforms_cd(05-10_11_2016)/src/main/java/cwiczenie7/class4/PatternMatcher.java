package cwiczenie7.class4;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import java.util.regex.*;

public class PatternMatcher {
	
	public static void main(String[] args) {
		
		List<String> stringList = new ArrayList();
		stringList.add("S1234567899aa"); //nie pasuje 
		stringList.add("s1234567899");	//pasuje
		stringList.add("s1234567899AA"); //nie pasuje
		
		//matcher1
		String pat = "S\\d{0,10}|s\\d{10}";
		Pattern pattern = Pattern.compile(pat);
		Matcher matcher = pattern.matcher("S1234567899");
		boolean matches = matcher.matches();
		System.out.println(matches);
		
		//matcher2
		String pat1 = "[a-dg-z]*";
		Pattern pattern1 = Pattern.compile(pat1);
		Matcher matcher1 = pattern1.matcher("abcggggg");
		boolean matches1 = matcher1.matches();
		System.out.println(matches1);
//		List<String> matchedElements = new ArrayList<>();
//		stringList.forEach((item) -> {
//				if (Pattern.matches("sS", input))
//		});
		
		String inputString = "AAAdsdd   dsdasd  sadsdasda	sdsd";
		inputString.replaceAll("\\s+", "|");
		System.out.println(inputString);
	}
}
