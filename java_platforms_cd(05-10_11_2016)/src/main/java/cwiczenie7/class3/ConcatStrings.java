package cwiczenie7.class3;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

public class ConcatStrings {
	public static void main(String[] args) {
		ConcatStrings demo = new ConcatStrings();
		List<String> list1 = new ArrayList<>();
		List<String> list2 = new ArrayList<>();
		List<String> list3 = new ArrayList<>();
		list1.add("Ala");
		list1.add(" ma");
		list2.add(" kota.");
		list2.add(" A kot");
		list3.add(" ma");
		list3.add(" Ale.");
		List<String> stringList = demo.concatListsOfStrings(list1, list2, list3);
		String[] stringArray = demo.arraysFromLists(stringList);
		System.out.println("string array size "+stringArray.length);
		for (int i = 0; i < stringArray.length; i++) {
			System.out.println(stringArray[i]);
		}
		
	}
	
	public List<String> concatListsOfStrings(List<String>... stringLists){
		List<String> outList = new ArrayList<>();
		for (List<String> list: stringLists){
			outList.addAll(list);
		}
		return outList;		
	}
	
	public String[] arraysFromLists(List<String> stringList){
		System.out.println("size of list: " + (stringList.size()));
		String[] arrayOfStrings = new String[stringList.size()];
		stringList.toArray(arrayOfStrings);
		return arrayOfStrings;	
	}
}
