package cwiczenie5.class1_reflections2;

import java.lang.reflect.Method;

//Class.forName...
public class Test1 {

	public static void main(String[] args) {
		Class<Square> myClassStructure = Square.class;
		Method[] methods = myClassStructure.getMethods();
		for (int i = 0; i < methods.length; i++){
			System.out.println(methods[i].toString());
		}
	}
}
