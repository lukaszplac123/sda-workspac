package cwiczenie5.class1;

public class Test {
	public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		Class animal = Class.forName("cwiczenie5.class1.Dog");
		Animal animal1 = (Animal) animal.newInstance();
		animal1.voice();
	}
}
