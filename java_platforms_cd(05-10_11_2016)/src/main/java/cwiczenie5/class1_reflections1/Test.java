package cwiczenie5.class1_reflections1;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Test {
	public static void main(String[] args) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, ClassNotFoundException, InstantiationException, NoSuchMethodException, SecurityException  {
//		Class animal = Class.forName("cwiczenie5.class1_reflections.Duck");
//		Animal animal1 = (Animal) animal.newInstance();
//		animal1.voice();
		
		Class animal = Class.forName("cwiczenie5.class1_reflections.Duck");
		Animal animal1 = (Animal) animal.newInstance();
		Method method =  animal.getMethod("voice");
		method.invoke(animal1, null );
		//animal1.voice();
		
	}
}
