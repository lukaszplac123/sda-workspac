package cwiczenie5.class1_strings;

public class Main {

	public static void main(String[] args) {
		String x = "ala";
		String y = "ala";
		String z = new String("ala");
		String m = new String("ala2");
		System.out.println(x.hashCode());
		System.out.println(y.hashCode());
		System.out.println(z.hashCode());
		System.out.println(m.hashCode());
		System.out.println(x.equals(m));
	}
}
