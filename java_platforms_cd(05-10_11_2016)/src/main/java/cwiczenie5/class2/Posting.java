package cwiczenie5.class2;

import java.util.Currency;


public class Posting{
	
	int id;
	
	Company source;
	
	String transactionCurrency;

	double transactionValue;

	String numericAccount;
	
	char moduleCode;

	public Company getSource() {
		return source;
	}

	public void setSource(Company source) {
		this.source = source;
	}

	public char getModuleCode() {
		return moduleCode;
	}

	public void setModuleCode(char moduleCode) {
		this.moduleCode = moduleCode;
	}

	public double getTransactionValue() {
		return transactionValue;
	}

	public void setTransactionValue(double transactionValue) {
		this.transactionValue = transactionValue;
	}
	
	public String getTransactionCurrency() {
		return transactionCurrency;
	}

	public void setTransactionCurrency(String transactionCurrency) {
		this.transactionCurrency = transactionCurrency;
	}

	Posting(double transactionValue, String numericAccount, char moduleCode, String currency){
		

		String tcCurrency = currency;
		source = new Company(moduleCode, tcCurrency);
		transactionCurrency = tcCurrency;
		this.moduleCode = moduleCode;
		this.transactionValue = transactionValue;
	}
	
	Posting (char codeToGroupBy, String currencyToGroupBy){
		this.moduleCode = codeToGroupBy;
		this.transactionCurrency = currencyToGroupBy;
		source = new Company(moduleCode, currencyToGroupBy);
	}

}
