package cwiczenie5.class2;

import java.util.List;


public class Demo {
	
	public static void main(String[] args) {
	
		Posting posting1 = new Posting(10.43, "acc1", 'D', "PLN");
		Posting posting2 = new Posting(10.58, "acc2", 'A', "PLN");
		Posting posting3 = new Posting(8.45, "acc3", 'B', "PLN");
		Posting posting4 = new Posting(11.23, "acc3", 'B', "PLN");
		Posting posting5 = new Posting(14.53, "acc3", 'D', "PLN");
		Posting posting6 = new Posting(14.53, "acc3", 'B', "PLN");
		Posting posting7 = new Posting(14.53, "acc3", 'B', "PLN");
		Transaction transaction1 = new Transaction(1, 'D');
		Transaction transaction2 = new Transaction(2, 'D');
		transaction1.getPostings().add(posting1);
		transaction1.getPostings().add(posting2);
		transaction1.getPostings().add(posting3);
		transaction1.getPostings().add(posting4);
		transaction1.getPostings().add(posting5);
		transaction1.getPostings().add(posting6);
		transaction1.getPostings().add(posting7);
		
		Posting groupPattern = new Posting('B', "PLN");
		GroupingKey groupingEvaluation = new GroupedPostings();
		List<Posting> postingsGrouped = groupingEvaluation.group(transaction1, groupPattern);
		
		System.out.println("Grouped postings(cash) in transaction 1 with code "+groupPattern.getModuleCode()+" and currency "+groupPattern.getTransactionCurrency() +" :");
		postingsGrouped.forEach((item) -> System.out.println(item.getTransactionValue() + item.getTransactionCurrency()));


		AmountTcCalculator amountTcCalculator = new AmountTcCalculator();
		System.out.println("Dollars for posting 1" + amountTcCalculator.calculate(posting1) + "$");
		System.out.println("Money amount for grouped Postings : "+ groupingEvaluation.evaluateGroupedPostingsAmount(postingsGrouped) + "$");
	}
}
