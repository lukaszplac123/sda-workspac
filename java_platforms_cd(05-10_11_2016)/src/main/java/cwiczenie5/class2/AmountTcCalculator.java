package cwiczenie5.class2;

import java.util.Currency;

public class AmountTcCalculator {
	ExchangeMap exchangeMap;
	
	public AmountTcCalculator() {
		exchangeMap = new ExchangeMap();
		exchangeMap.addExchangeValue(Currency.getInstance("PLN"), 3.9169);
	}
	public double calculate(Posting posting){
		double exchangeFactor = exchangeMap.map.get(posting.transactionCurrency);
		return posting.transactionValue * (1/exchangeFactor);
	}
}
