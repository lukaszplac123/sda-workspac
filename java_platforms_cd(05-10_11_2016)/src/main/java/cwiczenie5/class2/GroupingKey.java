package cwiczenie5.class2;

import java.util.List;

public interface GroupingKey{
	double key = 0;
	public List<Posting> group(Transaction transaction, Posting pattern);
	public double evaluateGroupedPostingsAmount(List<Posting> groupedPostings);
}
