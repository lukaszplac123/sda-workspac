package cwiczenie5.class2;

import java.util.Currency;

public class Company {
	char companyCode;
	String localCurrency;
	
	public String getLocalCurrency() {
		return localCurrency;
	}

	public void setLocalCurrency(String localCurrency) {
		this.localCurrency = localCurrency;
	}

	Company(char companyCode, String localCurrency){
		this.companyCode = companyCode;
		this.localCurrency = localCurrency;
		
	}
}
