package cwiczenie5.class2;

import java.util.Currency;
import java.util.HashMap;
import java.util.Map;

public class ExchangeMap {

	Map<String, Double> map;
	
	ExchangeMap(){
		map = new HashMap<>();
	}
	
	void addExchangeValue(Currency currency, double exchangeValue){
		map.put(currency.getCurrencyCode(), exchangeValue);
	}
}
