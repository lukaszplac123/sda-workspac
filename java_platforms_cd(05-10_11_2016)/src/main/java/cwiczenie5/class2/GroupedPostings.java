package cwiczenie5.class2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GroupedPostings implements GroupingKey{

	Map<Long, List<Posting>> groupingMap;
	List<Posting> groupedPostings;
	
	public GroupedPostings() {
		groupingMap = new HashMap<>();
	}
	

	@Override
	public List<Posting> group(Transaction transaction, Posting pattern) {
		groupedPostings = new ArrayList<>();
		long evaluatedKey = GroupingKeyEvaluation.evaluate(transaction, pattern);
		groupingMap.put(evaluatedKey, groupedPostings);
		for (Posting p : transaction.getPostings()){	
			if (groupingMap.containsKey(GroupingKeyEvaluation.evaluate(transaction, p))){
				groupingMap.get(evaluatedKey).add(p);
			} else{}
		}
		groupedPostings = groupingMap.get(evaluatedKey);
		return groupedPostings;
	}


	@Override
	public double evaluateGroupedPostingsAmount(List<Posting> groupedPostings) {
		double sum = 0;
		AmountTcCalculator calculatorDollars = new AmountTcCalculator();
		for (Posting p : groupedPostings){
			sum += calculatorDollars.calculate(p);
		}
		return sum;
	}
	
	

}
