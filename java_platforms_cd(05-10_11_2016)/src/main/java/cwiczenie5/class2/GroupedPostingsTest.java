package cwiczenie5.class2;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import junit.framework.TestCase;

public class GroupedPostingsTest {

	private Posting pattern1 = new Posting('B', "PLN");
	private Posting pattern2 = new Posting('C', "PLN");
	private Posting pattern3 = new Posting('A', "PLN");
	private Posting posting1 = new Posting(10.43, "acc1", 'D', "PLN");
	private Posting posting2 = new Posting(10.58, "acc2", 'A', "PLN");
	private Posting posting3 = new Posting(8.45, "acc3", 'B', "PLN"); //match pattern
	private Posting posting4 = new Posting(11.23, "acc3", 'B', "PLN"); //match pattern
	private Posting posting5 = new Posting(14.53, "acc3", 'D', "PLN");
	private Posting posting6 = new Posting(14.53, "acc3", 'B', "PLN"); //match pattern
	private Posting posting7 = new Posting(14.53, "acc3", 'B', "PLN"); //match pattern
	private Transaction transaction1 = new Transaction(1, 'D');
	private GroupingKey groupingEvaluation  = new GroupedPostings();
	
	public GroupedPostingsTest(){
		transaction1.getPostings().add(posting1);
		transaction1.getPostings().add(posting2);
		transaction1.getPostings().add(posting3);
		transaction1.getPostings().add(posting4);
		transaction1.getPostings().add(posting5);
		transaction1.getPostings().add(posting6);
		transaction1.getPostings().add(posting7);
	}
	
	
	@Test
	public void testGroupingShouldReturnFourPostings(){
			//given
			List<Posting> expectedList = new ArrayList<>();
			expectedList.add(posting3);
			expectedList.add(posting4);
			expectedList.add(posting6);
			expectedList.add(posting7);
			
			//when
			List<Posting> groupedPostingsList = groupingEvaluation.group(transaction1, pattern2);
			
			//then
			Assert.assertTrue(expectedList.containsAll(groupedPostingsList));
	}
	
	@Test
	public void  testGroupingShouldReturnNoPostings(){
			//given
			List<Posting> expectedList = new ArrayList<>();
		
			//when
			List<Posting> groupedPostingsList = groupingEvaluation.group(transaction1, pattern2);
			
			//then
			Assert.assertTrue(groupedPostingsList.isEmpty());
	}
	
	@Test
	public void  testGroupingShouldReturnOnePosting(){
			//given
			List<Posting> expectedList = new ArrayList<>();
			expectedList.add(posting2);
		
			//when
			List<Posting> groupedPostingsList = groupingEvaluation.group(transaction1, pattern3);
			
			//then
			Assert.assertTrue(expectedList.containsAll(groupedPostingsList));
	}
	
	
	

}
