package cwiczenie5.class2;

public class GroupingKeyEvaluation {
	public static long evaluate(Transaction transaction, Posting posting){
		int moduleNumber = 0;
		int currencyNumber = 0;
		if (posting.getModuleCode() == 'D') moduleNumber = 1;
		if (posting.getModuleCode() == 'C') moduleNumber = 2;
		if (posting.getModuleCode() == 'A') moduleNumber = 3;
		if (posting.getModuleCode() == 'B') moduleNumber = 4;
		if (posting.getSource().getLocalCurrency().equals("PLN")) currencyNumber = 1;
		return (transaction.getId() + moduleNumber + currencyNumber);
	}
}
