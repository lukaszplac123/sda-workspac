package cwiczenie5.class2;

import java.util.ArrayList;
import java.util.List;

public class Transaction {
    private long id;
    private char source;
 
    private List<Posting> postings;
 
    public Transaction(int id, char source) {
		postings = new ArrayList<>();
		this.setId(id);
		this.setSource(source);
	}

	public List<Posting> getPostings() {
		return postings;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public char getSource() {
		return source;
	}

	public void setSource(char source) {
		this.source = source;
	}
}
