package cwiczenie6.class2;

import java.nio.channels.ShutdownChannelGroupException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.stream.Stream;

public class MinArray {
	
	List<Integer> listOfIntegers;
	List<String> listOfStrings;
	
	public MinArray() {
		listOfIntegers = new ArrayList<>();
		listOfStrings = new ArrayList<>();
	}
	
	public static void main(String[] args) {
		//2_1
		MinArray minArray = new MinArray();
		minArray.listOfIntegers = Arrays.asList(6,6,140,150,7,7,500,150,6);
		try{
			Integer value = Collections.min(minArray.listOfIntegers);
			System.out.println("Value - "+value);
			//2_2
			System.out.println("Index - "+minArray.listOfIntegers.indexOf(value));
			//2_3 stream
			Stream stream = minArray.listOfIntegers.stream();
			//stream.
			
			//2_4
			Collections.sort(minArray.listOfIntegers);
			int middle_index = minArray.listOfIntegers.size()/2;
			Integer middle = minArray.listOfIntegers.get(middle_index);
			System.out.println("Middle index - "+ middle_index);
			System.out.println("Middle value - "+ middle);
			
			//2.5
			System.out.println("-----HashSet ---------");
			Set<Integer> setOfUnique = new HashSet<>();
			setOfUnique.addAll(minArray.listOfIntegers);
			setOfUnique.forEach((item) -> System.out.print(item+"|"));
			System.out.println("\n");
			System.out.println("-----LinkedHashSet ---------");
			Set<Integer> setOfUniqueLinkedHashSet = new LinkedHashSet<>();
			setOfUniqueLinkedHashSet.addAll(minArray.listOfIntegers);
			setOfUniqueLinkedHashSet.forEach((item) -> System.out.print(item+"|"));
			System.out.println("\n");
			System.out.println("Size - "+setOfUnique.size());
			
			//2.6
			System.out.println("Given list");
			minArray.listOfIntegers.forEach((item) -> System.out.print(item+"|"));
			System.out.println();
			int index = minArray.indexOfElementWithValueBiggerThanElementsBefore(minArray.listOfIntegers);
			System.out.println(">thanElementsBefore index = "+index);
			
			//2.7
			Map<Integer, Integer> cardinalityMap = new HashMap<>();
			for (Integer listValue : minArray.listOfIntegers){
				if (cardinalityMap.containsKey(listValue)){
					int entryValue = cardinalityMap.get(listValue);
					cardinalityMap.put(listValue, entryValue+1);
				} else{
					cardinalityMap.put(listValue, new Integer(1));
				}
			}
			
			minArray.showMap(cardinalityMap);
			
		} catch (NoSuchElementException e){
			System.out.println("Empty list !!!");
		}
		

	}
	
	private int sumOfElementsIndexBefore(List<Integer> list, int index){
		int sum = 0;
		for (int i = 0 ; i < index; i++ ){
			sum += list.get(i);
		}
		System.out.println(sum);
		return sum;
	}
	
	private int indexOfElementWithValueBiggerThanElementsBefore(List<Integer> listOfIntegers){
		for (int i = listOfIntegers.size() - 1; i > 0 ; i--){
			int sumOfElementsBefore = sumOfElementsIndexBefore(listOfIntegers, i);
			if (sumOfElementsBefore > listOfIntegers.get(i)){
				return i;
			}
		}
		return 0;
	}
	
	private void showMap(Map map){
		map.forEach((key, entry) -> System.out.println("Key:"+key + "|" + "Quantity:"+entry));
	}
	
}
