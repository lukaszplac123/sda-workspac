package pl.sda.kredyt.v1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DecimalFormat;

public class Kredyt {

	private static final Logger logger = LoggerFactory.getLogger(Kredyt.class);

	private double procent;
	private double kwota;
	private double rata;
	private double iloscRat;

	public Kredyt() {
		logger.info("Tworzenie kredytu: {}", this.hashCode());
	}

	public Kredyt(double kwota, double procent, double rat) {
		logger.info("Tworzenie kredytu: {}", this.hashCode());
		this.kwota = kwota;
		this.procent = procent;
		this.iloscRat = rat;
		this.obliczRate();
	}

	void obliczRate() {
		double wsp = 1 - 1 / Math.pow(1.0 + procent / 12, iloscRat);
		wsp = (procent / 12) / wsp;
		rata = wsp * kwota;
	}

	public double getProcent() {
		return procent;
	}

	public void setProcent(double procent) {
		this.procent = procent;
	}

	public double getKwota() {
		return kwota;
	}

	public void setKwota(double kwota) {
		logger.info("setKwota: {}", this.hashCode());
		this.kwota = kwota;
	}

	public double getIloscRat() {
		return iloscRat;
	}

	public void setIloscRat(double iloscRat) {
		this.iloscRat = iloscRat;
	}

	public void setRata(double rata) {

	}
	public double getRata() {
		logger.info("getRata: {}", this.hashCode());
		obliczRate();
		return rata;
	}

	@Override
	public String toString() {
		return String.format("kwota: %s, procent: %s, rat: %s, rata: %s",
				getKwota(),
				getProcent(),
				getIloscRat(),
				new DecimalFormat("#.##").format(getRata()));
	}

}