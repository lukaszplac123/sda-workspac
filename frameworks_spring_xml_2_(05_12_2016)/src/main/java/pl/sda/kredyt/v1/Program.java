package pl.sda.kredyt.v1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import pl.sda.kredyt.v1.rachunek.Rachunek;

public class Program {

    private static final Logger logger = LoggerFactory.getLogger(Program.class);

    private void drukujBeany(ApplicationContext context) {
        for(String beanName : context.getBeanDefinitionNames()) {
            logger.info("Bean name: {}", beanName);
        }
    }

    void przyklad0() {
        ApplicationContext context = new ClassPathXmlApplicationContext("application0.xml");
        for(String beanName : context.getBeanDefinitionNames()) {
            logger.info("Bean name: {}", beanName);
        }

        Kredyt kredyt = (Kredyt) context.getBean("kredyt");
        kredyt.setKwota(1000);
        kredyt.setProcent(0.05);
        kredyt.setIloscRat(20);

        logger.info(kredyt.toString());
    }

    void przyklad1() {
        ApplicationContext context = new ClassPathXmlApplicationContext("application1.xml");

        logger.info("Przyklad 1");
        drukujBeany(context);

        Kredyt kredytOgolny = context.getBean("kredytOgolny", Kredyt.class);
        logger.info(kredytOgolny.toString());

        Kredyt kredytIndywidulany = (Kredyt) context.getBean("kredytIndywidualny");
        logger.info(kredytIndywidulany.toString());
    }

    void przyklad2() {
        ApplicationContext context = new ClassPathXmlApplicationContext("application2.xml");

        logger.info("Przyklad 2");
        drukujBeany(context);

        Klient klient = context.getBean("klient", Klient.class);
        logger.info(klient.toString());
    }

    void przyklad3() {
        ApplicationContext context = new ClassPathXmlApplicationContext("application3.xml");

        logger.info("Przyklad 3");
        drukujBeany(context);

        Klient klient = context.getBean("klient", Klient.class);
        logger.info(klient.toString());
    }

    void przyklad4() {
        ApplicationContext context = new ClassPathXmlApplicationContext("application4.xml");

        logger.info("Przyklad 4");
        drukujBeany(context);

        Klient klient = (Klient)context.getBean("klient1");
        Rachunek rachunek = klient.getRachunek();
        rachunek.zasil(100);
        int i=0;
        while(rachunek.pobierz(10) && i++<100)
            logger.info("{}: Pobieram 10 zł", i);

    }

    void przyklad5() {
        ApplicationContext context = new ClassPathXmlApplicationContext("application5.xml");

        logger.info("Przyklad 5");
        drukujBeany(context);

        Bank bank = (Bank)context.getBean("bank");
        logger.info(bank.toString());
    }

    void przyklad6() {
        ApplicationContext context = new ClassPathXmlApplicationContext("application6.xml");

        logger.info("Przyklad 6");
        drukujBeany(context);

        Bank bank = (Bank)context.getBean("bank");
        logger.info(bank.toString());

        logger.info("Nowy rachunek 1: {}", bank.otworzRachunek().hashCode());
        logger.info("Nowy rachunek 2: {}", bank.otworzRachunek().hashCode());
    }

    void przyklad7() {
        ApplicationContext context = new ClassPathXmlApplicationContext("application7.xml");

        logger.info("Przyklad 7");
        drukujBeany(context);

        Bank bank = (Bank)context.getBean("bank");
        logger.info(bank.toString());

        logger.info("Nowy rachunek 1: {}", bank.otworzRachunek().hashCode());
        logger.info("Nowy rachunek 2: {}", bank.otworzRachunek().hashCode());
    }

    public static void main(String[] args) {
        Program program = new Program();
//        program.przyklad0();
//        program.przyklad1();
//        program.przyklad2();
//        program.przyklad3();
//        program.przyklad4();
//        program.przyklad5();
//        program.przyklad6();
        program.przyklad7();
    }
}
