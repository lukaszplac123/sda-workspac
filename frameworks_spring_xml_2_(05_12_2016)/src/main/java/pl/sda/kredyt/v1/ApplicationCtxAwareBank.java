package pl.sda.kredyt.v1;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import pl.sda.kredyt.v1.rachunek.Rachunek;

import java.util.List;

public class ApplicationCtxAwareBank extends Bank implements ApplicationContextAware {
    private List<Rachunek> rachunki;
    private ApplicationContext context;

	public List<Rachunek> getRachunki() {
        return rachunki;
	}

	public void setRachunki(List<Rachunek> rachunki) {
        this.rachunki = rachunki;
	}

	public Rachunek otworzRachunek(){
		return context.getBean("nowyRachunek", Rachunek.class);
	}

	public String toString() {
        return "Moje rachunki: " + rachunki.toString();
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.context = applicationContext;
	}
}
