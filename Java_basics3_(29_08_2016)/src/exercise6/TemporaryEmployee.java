package exercise6;

public class TemporaryEmployee extends Employee{
	
	public TemporaryEmployee(double grossSalary, String email) {
		super(grossSalary, email);
	}

	@Override
	public double calculateNetSalary() {
		return 0.9 * grossSalary;
	}

}
