package exercise6;

public class StudentEmployee extends Employee{
	
	public StudentEmployee(double grossSalary, String email) {
		super(grossSalary, email);
	}

	@Override
	public double calculateNetSalary() {
		return 0.95 * grossSalary;
	}

}
