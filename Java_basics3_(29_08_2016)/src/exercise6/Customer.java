package exercise6;

public class Customer implements Person {

	protected String email;
	
	
	public Customer(String email){
		this.email = email;
	}
	
	@Override
	public String getEmail() {
		return email;
	}
	
}
