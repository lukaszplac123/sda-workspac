package pl.sda.v4;

public interface Printable extends Printer {
    void setPrinter();
    void pageOrientation();
}
