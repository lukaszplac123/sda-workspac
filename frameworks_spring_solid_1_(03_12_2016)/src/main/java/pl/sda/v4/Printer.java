package pl.sda.v4;

public interface Printer {
    void setHead();
    void setBody();
    void show();
}
