package pl.sda.v4;

public interface Paginable extends Printable {
    void setPageNumber();
    void setMaxPages();
    void setPageNumberLocation();
    void sendLF();
}
