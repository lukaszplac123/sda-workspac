package pl.sda.v4;

public class PdfPrinter implements Paginable {

    public void setHead() {
        /* set head of pdf document */
    }

    public void setBody() {
        /* set body of pdf document */
    }

    public void show() {
        /* show pdf document */
    }

    @Override
    public void setPrinter() {

    }

    @Override
    public void pageOrientation() {

    }

    @Override
    public void setPageNumber() {

    }

    @Override
    public void setMaxPages() {

    }

    @Override
    public void setPageNumberLocation() {

    }

    @Override
    public void sendLF() {

    }
}
