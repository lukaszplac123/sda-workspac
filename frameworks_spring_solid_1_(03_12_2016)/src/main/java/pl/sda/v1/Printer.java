package pl.sda.v1;

public interface Printer {
    void setHead();
    void setBody();
    void show();
}
