package pl.sda.v1;

import java.io.IOException;
import java.util.Properties;

class Report {

	private final String type;

	public Report() {
		type = loadReportType();
	}

	private String loadReportType() {
		Properties reportSettings = new Properties();
		try {
			reportSettings.load(getClass().getResourceAsStream("/report.properties"));
			return reportSettings.getProperty("type");
		} catch (IOException ex) {
			ex.printStackTrace();
			throw new RuntimeException(ex);
		}
	}

	public void generate() {

		Printer printer;
		if(type.equals("html")) {
			printer = new HtmlPrinter();
		} else {
			printer = new PdfPrinter();
		}
		printer.setHead();
		printer.setBody();

		printer.show();
	}
}
