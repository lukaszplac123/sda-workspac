package pl.sda.v2;

class Report {

	private final PrinterFactory factory = new PrinterFactory();

	public void generate() {
		Printer printer = factory.getPrinter();
		printer.setHead();
		printer.setBody();

		printer.show();
	}
}
