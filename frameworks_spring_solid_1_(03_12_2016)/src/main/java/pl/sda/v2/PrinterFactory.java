package pl.sda.v2;

import java.io.IOException;
import java.util.Properties;

public class PrinterFactory {

    private String loadReportType() {
        Properties reportSettings = new Properties();
        try {
            reportSettings.load(getClass().getResourceAsStream("/report.properties"));
            return reportSettings.getProperty("type");
        } catch (IOException ex) {
            ex.printStackTrace();
            throw new RuntimeException(ex);
        }
    }

    public Printer getPrinter() {
        final String type = loadReportType();
        if (type.equals("html")) {
            return new HtmlPrinter();
        } else {
            return new PdfPrinter();
        }
    }
}
