package pl.sda.v2;

public interface Printer {
    void setHead();
    void setBody();
    void show();
}
