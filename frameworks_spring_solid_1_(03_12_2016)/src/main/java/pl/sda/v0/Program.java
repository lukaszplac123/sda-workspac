package pl.sda.v0;

public class Program {

    public static void main(String[] args) {
        Report report = new Report();
        report.generatePdf();
        report.generateHtml();
    }
}
