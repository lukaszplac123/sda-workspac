package pl.sda.v0;

class Report {
    public void generatePdf() {

		PdfPrinter printer = new PdfPrinter();
		printer.setHead();
		printer.setBody();

		printer.show();
	}

	public void generateHtml() {

		HtmlPrinter printer = new HtmlPrinter();
		printer.setHead();
		printer.setBody();

		printer.show();
	}
}
