package pl.sda.v3;

public class Program {

    public static void main(String[] args) {
        Report report = new Report();
        report.setPrinter(new PrinterFactory().getPrinter());
        report.generate();
    }
}
