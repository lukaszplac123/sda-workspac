package pl.sda.v3;

class Report {

	private Printer printer;

	public void setPrinter(Printer printer) {
		this.printer = printer;
	}

	public void generate() {
		printer.setHead();
		printer.setBody();

		printer.show();
	}
}
