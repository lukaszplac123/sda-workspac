package pl.sda.v3;

public class HtmlPrinter implements Printer {

    public void setHead() {
        /* set head of HTML document */
    }

    public void setBody() {
        /* set body of HTML document */
    }

    public void show() {
        /* show HTML document */
    }
}
