package pl.sda.v3;

public interface Printer {
    void setHead();
    void setBody();
    void show();
}
