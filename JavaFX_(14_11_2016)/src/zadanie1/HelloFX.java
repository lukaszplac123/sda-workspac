package zadanie1;
	
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;


public class HelloFX extends Application {
	
	@Override
	public void start(Stage primaryStage) {
			
			Button button = new Button();
			button.setText("HELLO WORLD!");
			StackPane root = new StackPane();
			root.getChildren().add(button);
			Scene scene = new Scene(root,300,300);
			primaryStage.setScene(scene);
			primaryStage.show();
			button.setOnAction((event) -> {
				System.out.println("Hello in FX Wolrd");
			});
		
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
