package zadanie3;
	
import java.util.ArrayList;
import java.util.List;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Accordion;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;


public class WebBrowserStyled extends Application {
	
	@Override
	public void start(Stage primaryStage) {
			
			//leftAligned
			Button button1 = new Button();
			button1.setText("Button1");
			button1.getStyleClass().add("custom-color");
			Button button2 = new Button();
			button2.setText("Button2");
			button2.getStyleClass().add("custom-color");
			Button button3 = new Button();
			button3.setText("Button3");
			button3.getStyleClass().add("custom-color");
			VBox vbox = new VBox();
			vbox.getChildren().addAll(button1, button2, button3);
			
			//upperAlligned
			Label label = new Label();
			label.setText("This is combo");
			ComboBox<String> comboBox = new ComboBox();
			comboBox.getStyleClass().add("custom-color");
			HBox hbox = new HBox();
			hbox.getChildren().addAll(label, comboBox);
			
			//content
			TabPane tabPane = new TabPane();
			Tab webTab = new Tab();
			webTab.getStyleClass().add("custom-color-border");
			WebView webBrowser = new WebView();
			WebEngine engine = webBrowser.getEngine();
			engine.load("http://google.pl");
			webTab.setText("WebBrowser");
			webTab.setContent(webBrowser);
			Tab imageTab = new Tab();
			imageTab.getStyleClass().add("custom-color");
			imageTab.setText("Image");
			tabPane.getTabs().addAll(webTab, imageTab);
			
			//rightAlligned
			Accordion accordion = new Accordion();
			TitledPane pane1 = new TitledPane("co tu jest?", new Label("Nic 1"));
			TitledPane pane2 = new TitledPane("a tutaj?", new Label("Nic 2"));
			TitledPane pane3 = new TitledPane("hmm, a tu?", new Label("Nic 3"));
			pane1.getStyleClass().add("accordion");
			pane2.getStyleClass().add("accordion");
			pane3.getStyleClass().add("accordion");
			accordion.getPanes().addAll(pane1, pane2, pane3);
			VBox vbox2 = new VBox();
			vbox2.getChildren().add(accordion);
			
			
			//bottomAlligned
			DatePicker picker = new DatePicker();
			picker.getStyleClass().add("date-picker");
			Slider slider = new Slider();
			picker.getStyleClass().add("slider");
			Hyperlink hyperLink = new Hyperlink();
			hyperLink.setText("http://google.pl");
			List<Button> buttonList = new ArrayList();
			for (int i = 1 ; i < 8; i++){
				Button button = new Button("Button " + i);
				button.getStyleClass().add("custom-color");
				buttonList.add(button);
			}
			FlowPane bottomPane = new FlowPane();
			bottomPane.getChildren().addAll(picker, slider, hyperLink);
			bottomPane.getChildren().addAll(buttonList);
			
			//setting layout	
			BorderPane pane = new BorderPane();
			pane.setLeft(vbox);
			pane.setTop(hbox);
			pane.setCenter(tabPane);
			pane.setRight(vbox2);
			pane.setBottom(bottomPane);
			
			//creating scene and adding to stage
			Scene scene = new Scene(pane,1600,1000);
			scene.getStylesheets().add("zadanie3/application.css");
			primaryStage.setScene(scene);
			primaryStage.show();
		
		
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
