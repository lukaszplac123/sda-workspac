package exercise6Lists;

public class AccountingDepartment {

	void sendPaychecks (CompanyEmployees companyEmployees){
		for (Employee employee: companyEmployees.getAllEmployees()){
			if (employee == null){
				break;
			}
			System.out.println(employee.getEmail() + " : " + employee.calculateNetSalary());
		}
	}
	
}
