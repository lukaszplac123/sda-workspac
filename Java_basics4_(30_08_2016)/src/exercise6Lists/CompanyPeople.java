package exercise6Lists;

import java.util.ArrayList;
import java.util.List;

public interface CompanyPeople {

	public List<Person> getAllPeople();
}
