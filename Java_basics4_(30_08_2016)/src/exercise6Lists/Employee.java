package exercise6Lists;

public abstract class Employee implements Person {

	protected double grossSalary;
	protected String email;
	
	
	public Employee(double grossSalary, String email){
		this.grossSalary = grossSalary;
		this.email = email;
	}
	
	
	@Override
	public String getEmail() {
		return this.email;
	}
	
	public abstract double calculateNetSalary();
}
