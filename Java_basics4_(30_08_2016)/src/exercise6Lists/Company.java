package exercise6Lists;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

//6. Zaprojektuj klasę abstrakcyjną Employee, która będzie zawierać pole grossSalary (zarobki brutto) 
//i abstrakcyjną metodę calculateNetSalary() która zwraca zarobki netto. 
//Implementacje tej klasy to FullTimeEmployee, Student oraz TemporaryEmployee. 
//Stawki podatków dla nich będą odpowiednio 20%, 5% i 10%, a FullTimeEmployee będzie płacił dodatkowo ubezpieczenie 
//społeczne w wysokości 1000 PLN, które należy odjąć od kwoty brutto przed policzeniem podatku. 

//Napisz klasę Company do której będzie można dodawać obiekty klasy Employee (możesz je przechowywać w tablicy) i w której w każdej chwili będzie można policzyć sumę oraz średnią arytmetyczną zarobków pracowników całej firmy.
public class Company implements CompanyPeople, CompanyEmployees {

	List<Employee> listOfEmployee;
	List<Customer> listOfCustomers;
	List<Person> listOfPeople;
	private int index = 0;
	
	
	public static void main(String[] args){
		Company company = new Company();
		AccountingDepartment accounting = new AccountingDepartment();
		EventDepartament events = new EventDepartament();
		
		Employee emp1 = new FullTimeEmployee(2500, "fte1@gmail.com");
		Employee emp2 = new FullTimeEmployee(2500, "fte2@gmail.com");
		Employee emp3 = new StudentEmployee(1700, "stu1@gmail.com");
		Employee emp4 = new StudentEmployee(1700, "stu2@gmail.com");
		Employee emp5 = new StudentEmployee(1700, "stu3@gmail.com");
		Customer cust1 = new Customer("cust1@o2.pl");
		Customer cust2 = new Customer("cust2@o2.pl");
		Customer cust3 = new Customer("cust3@o2.pl");
		
		company.addEmployee(emp1);
		company.addEmployee(emp2);
		company.addEmployee(emp3);
		company.addEmployee(emp4);
		company.addEmployee(emp5);
		
		company.addCustomer(cust1);
		company.addCustomer(cust2);
		company.addCustomer(cust3);
		
		System.out.print("Pracownicy:\n");
		for (Person person: company.getAllEmployees()){
			if (person == null){
				break;
			}
			System.out.println(person.getEmail());
		}
		
		System.out.print("\n");
		
		System.out.print("Wszyscy:\n");
		for (Person person: company.getAllPeople()){
			if (person == null){
				break;
			}
			System.out.println(person.getEmail());
		}
		
		System.out.print("\n");
		
		System.out.print("Ksiegowosc oblicza wyplate:\n");
		accounting.sendPaychecks(company);
		
		System.out.print("\n");
		
		System.out.print("Wydzia� imprez wysyla zaproszenia:\n");
		events.sendInvitationToTheParty("impreza urodzinowa prezesa", company);	
		
	}
	
	
	public Company(){
		listOfEmployee = new ArrayList<Employee>();
		listOfCustomers = new ArrayList<>();
		listOfPeople = new ArrayList<>();
	}
	
	public void addEmployee(Employee employee){
		listOfEmployee.add(employee);
		listOfPeople.add(employee);
	}
	
	public void addCustomer(Customer customer){
		listOfCustomers.add(customer);
		listOfPeople.add(customer);
	}
	
	public double sumOfSalaries(){
		double sum = 0;
		for(Employee employee : listOfEmployee){
			if(employee != null){
				sum += employee.calculateNetSalary();
			}
		}
		return sum;
	}
	
	public double averageOfSalaries(){
		return sumOfSalaries() / index;
	}
	
	@Override
	public List<Person> getAllPeople(){
		return listOfPeople;
	}


	@Override
	public List<Employee> getAllEmployees() {
		return listOfEmployee;
	}
	
}
