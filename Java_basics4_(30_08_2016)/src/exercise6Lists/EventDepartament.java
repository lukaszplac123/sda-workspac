package exercise6Lists;

public class EventDepartament {

	
	void sendInvitationToTheParty (String partyName, CompanyPeople companyPeople){
		for (Person person: companyPeople.getAllPeople()){
			if (person == null){
				break;
			}
			System.out.println(person.getEmail() + " zaproszenie na: " + partyName);
		}
	}
}
