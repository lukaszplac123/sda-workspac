package exercise6Lists;

public class FullTimeEmployee extends Employee{

	public static final double SOCIAL_INSURANCE = 1000;

	public FullTimeEmployee(double grossSalary, String email) {
		super(grossSalary, email);
	}

	@Override
	public double calculateNetSalary() {
		return 0.8 * (grossSalary - SOCIAL_INSURANCE);
	}

}
