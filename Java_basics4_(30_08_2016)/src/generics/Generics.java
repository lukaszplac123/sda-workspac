package generics;

public class Generics {
	public static void main(String[] args){
		Box box = new Box("zawartosc");
		Object content = box.getContent();
		String contentAsString = (String) box.getContent();
		
		GenericBox<String> genericBox = new GenericBox<String>("zawartosc");
		String genericBoxContent = genericBox.getContent();
	}
}

class GenericBox<T> {
	
	private T content;
	
	public GenericBox(T content){
		this.content = content;
	}
	
	public T getContent(){
		return content;
	}
}

class Box {

	private Object content;
	
	public Box(Object content){
		this.content = content;
	}
	
	public Object getContent(){
		return content;
	}
}