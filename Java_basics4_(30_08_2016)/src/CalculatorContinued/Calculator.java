package CalculatorContinued;

import java.util.Arrays;

public class Calculator {
		
		public Operation op;
	
		public Calculator(String operation){
			String[] operationPieces;
			
			if ((operationPieces = operation.split("\\+")) != null) op = new PlusOperation(Double.parseDouble(operationPieces[0]), Double.parseDouble(operationPieces[1]));
				else if ((operationPieces = operation.split("\\-")) != null) op = new MinusOperation(Double.parseDouble(operationPieces[0]), Double.parseDouble(operationPieces[1]));
				else if ((operationPieces = operation.split("*")) != null) op = new MultiplyOperation(Double.parseDouble(operationPieces[0]), Double.parseDouble(operationPieces[1]));
				else System.out.println("Nieznany format danych");
		}
		
		public static void main (String[] args){
			
			Calculator calc = new Calculator("22-11");
			
			System.out.println(calc.op.calculate());
			
		}
		
}
