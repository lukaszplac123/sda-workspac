package CalculatorContinued;

public interface Operation {

	public double calculate();
}
