package collections;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class Sets {
	public static void main(String[] args){
		Set<Integer> set = new HashSet<>(); // new TreeSet<>();
		
		set.add(1);
		set.add(2);
		set.add(3);
		set.add(1);
		set.add(2);
		
		System.out.println("size = " + set.size());
		System.out.println("set = " + set);
		
		set.remove(1);
		
		System.out.println("set = " + set);
		
		System.out.println("contains 2 ? " + set.contains(2));
		
		TreeSet<Integer> treeSet = new TreeSet<>(); //new TreeSet<>(Collections.reverseOrder());
		
		treeSet.add(5);
		treeSet.add(3);
		treeSet.add(7);
		
		System.out.println("first = " + treeSet.first());
		System.out.println("first = " + treeSet.last());
		
		System.out.println("floor of 8 = " + treeSet.floor(8)); // less than or equals
		System.out.println("floor of 7 = " + treeSet.floor(7));
		System.out.println("lower of 7 = " + treeSet.lower(7));  // less than
		
		System.out.println("ceiling of 4 = " + treeSet.ceiling(4)); // greater than or equals
		System.out.println("ceiling of 5 = " + treeSet.ceiling(5));
		System.out.println("higher of 5 = " + treeSet.higher(5)); // greater than
		
		System.out.println("subset = " + treeSet.subSet(2, 6));
	}
}
