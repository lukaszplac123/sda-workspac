package zad4_2;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.TreeSet;

public class IntegersList {

	private List<Integer> listOfIntegers;
	
	public static void main(String[] args){
		
		IntegersList listMain = new IntegersList();
		
		System.out.println();
		System.out.print(listMain.listOfIntegers+"|");
		
		System.out.println();
		System.out.print(listMain.removeDuplicatesAndSort(listMain.listOfIntegers));;
		
		
	}
	
	IntegersList(){
		listOfIntegers = new ArrayList<Integer>();
		listOfIntegers.add(9);
		listOfIntegers.add(3);
		listOfIntegers.add(7);
		listOfIntegers.add(5);
		listOfIntegers.add(5);
		listOfIntegers.add(2);
		listOfIntegers.add(3);
		listOfIntegers.add(7);
		listOfIntegers.add(2);
			
	}
	
	public List<Integer> removeDuplicatesAndSort(List<Integer> list){
		
		TreeSet<Integer> intSet = new TreeSet<Integer>();
		for (Integer number : list){
			intSet.add(number);	
		}
		List<Integer> tempList1 = new ArrayList<Integer>(intSet);
		//Collections.reverse(tempList1);
		return tempList1;
	}
}
