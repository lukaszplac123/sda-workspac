package pl.sdacademy.jdbc.cwiczenia;

import java.sql.SQLException;

import pl.sdacademy.jdbc.db.ConnectionProvider;
import pl.sdacademy.jdbc.lotto.WynikiLosowania;

public class Cw05Transakcje extends CwiczenieJdbc {
    
    public Cw05Transakcje(ConnectionProvider connectionProvider) {
        super(connectionProvider);
    }

    public void zapiszWynikiLosowaniaIZaktualizujStatystyki(WynikiLosowania wynikiLosowania) throws SQLException {
        // TODO: 5a. Polaczyc dodawanie wynikow losowania i aktualizacje statystyk w jedna operacje
        //           - w pierwszym kroku podac nieprawidlowa wartosc przy aktualizacji statystyk (np. -2) aby spowodowac blad przy UPDATE
        //             i sprawdzic co sie stalo z INSERT'em do tabeli `wyniki_losowania`
        // TODO: 5b. Jawnie kontrolowac transakcje za pomoca metod Connection::setAutoCommit oraz Connection::commit
        //           - sprawdzic co sie dzieje w przypadku gdy nie wywolamy Connection::commit
    	
    }
    
}   
