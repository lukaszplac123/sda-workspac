package pl.sdacademy.jdbc.cwiczenia;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import pl.sdacademy.jdbc.db.ConnectionProvider;

public class Cw03KontrolowanieLiczbyWynikow extends CwiczenieJdbc {
    
    public Cw03KontrolowanieLiczbyWynikow(ConnectionProvider connectionProvider) {
        super(connectionProvider);
    }

    public void pokazListeOsob(Integer max) throws SQLException {
        // TODO: 3a. Stworzyc zapytanie ktore pobierz liste osob ograniczona maksymalnie do `max` wynikow uzywajac PreparedStatement::setMaxRows
        // TODO: 3b. Poprawic zapytanie tak, aby ograniczenie liczby rekordow dzialalo po stronie bazy danych
       
    	//3a
//    	doWithConnection(conn -> {
//    	   PreparedStatement ps = conn.prepareStatement("SELECT * FROM osoby");
//    	   ps.setFetchSize(max+1);
//    	   ps.setMaxRows(max);
//    	   ResultSet rs = ps.executeQuery();
//   	   while (rs.next()){
//           	System.out.print(rs.getString(1));
//           	System.out.print("|");
//           	System.out.print(rs.getString(2));
//           	System.out.print("|");
//           	System.out.print(rs.getString(3));
//           	System.out.print("|");
//           	System.out.println();
//   		}
    	
    	
    	//3b
    	doWithConnection(conn -> {
     	   PreparedStatement ps = conn.prepareStatement("SELECT * FROM osoby LIMIT ?");
     	   ps.setInt(1, max);
     	   ResultSet rs = ps.executeQuery();
    	   while (rs.next()){
            	System.out.print(rs.getString(1));
            	System.out.print("|");
            	System.out.print(rs.getString(2));
            	System.out.print("|");
            	System.out.print(rs.getString(3));
            	System.out.print("|");
            	System.out.println();
    		}
   	   ps.close();
   	   rs.close();
       }); 
    }

}   
