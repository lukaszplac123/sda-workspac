package pl.sdacademy.jdbc.cwiczenia;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.List;

import pl.sdacademy.jdbc.db.ConnectionProvider;
import pl.sdacademy.jdbc.lotto.WynikiLosowania;

public class Cw04InsertUpdate extends CwiczenieJdbc {
    
    public Cw04InsertUpdate(ConnectionProvider connectionProvider) {
        super(connectionProvider);
    }

    public void zapiszWynikiLosowania(WynikiLosowania wynikiLosowania) throws SQLException {
        // TODO: 4a. Zapisac wyniki losowania do tabeli `wyniki_losowania` za pomoca PreparedStatement::executeUpdate
    	doWithConnection(conn -> {
    		PreparedStatement ps = conn.prepareStatement("INSERT INTO wyniki_losowania(id, nazwa, liczba1, liczba2, liczba3, liczba4, liczba5, liczba6, data) VALUES (?,?,?,?,?,?,?,?,?)");
    		ps.setInt(1, wynikiLosowania.getNumer());
    		ps.setString(2, wynikiLosowania.getNazwa());	
    		ps.setInt(3, wynikiLosowania.getLiczby().get(0));
    		ps.setInt(4, wynikiLosowania.getLiczby().get(1));
    		ps.setInt(5, wynikiLosowania.getLiczby().get(2));
    		ps.setInt(6, wynikiLosowania.getLiczby().get(3));
    		ps.setInt(7, wynikiLosowania.getLiczby().get(4));
    		ps.setInt(8, wynikiLosowania.getLiczby().get(5));
    		ps.setTimestamp(9, Timestamp.from(wynikiLosowania.getData()));
    		ps.executeUpdate();
    		ps.close();
    	});

    }
    
    public void zapiszWynikiLosowan(List<WynikiLosowania> wynikiLosowan) throws SQLException {
        // TODO: 4b. Zapisac wyniki wszystkich losowan z przekazanego parametru do tabeli `wyniki_losowania` za pomoca PreparedStatement::addBatch/executeBatch
    	doWithConnection(conn -> {
    		PreparedStatement ps = conn.prepareStatement("INSERT INTO wyniki_losowania(id, nazwa, liczba1, liczba2, liczba3, liczba4, liczba5, liczba6, data) VALUES (?,?,?,?,?,?,?,?,?)");
    		wynikiLosowan.forEach((item) -> {
    			try {
					ps.setInt(1, item.getNumer());
					ps.setString(2, item.getNazwa());
		    		ps.setInt(3, item.getLiczby().get(0));
		    		ps.setInt(4, item.getLiczby().get(1));
		    		ps.setInt(5, item.getLiczby().get(2));
		    		ps.setInt(6, item.getLiczby().get(3));
		    		ps.setInt(7, item.getLiczby().get(4));
		    		ps.setInt(8, item.getLiczby().get(5));
		    		ps.setTimestamp(9, Timestamp.from(item.getData()));
		    		ps.addBatch();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}		
    		});
    		ps.executeBatch();
    		ps.close();
    	});
    }

    public void zwiekszLiczbeLosowan(Integer liczba) throws SQLException {
        // TODO: 4c. Zaktualizowac tabele `statystyki_losowan` - zwiekszyc o n liczbe losowan
    	doWithConnection(conn -> {
    		PreparedStatement ps = conn.prepareStatement("UPDATE statystyki_losowan SET liczba_losowan = liczba_losowan + ?");
    		ps.setInt(1, liczba);
    		ps.execute();
    		ps.close();
    	});
    }

    
}   
