package pl.sdacademy.jdbc.cwiczenia;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;

import pl.sdacademy.jdbc.db.ConnectionProvider;

public class Cw01ProsteZapytanie extends CwiczenieJdbc {
    
    public Cw01ProsteZapytanie(ConnectionProvider connectionProvider) {
        super(connectionProvider);
    }
    
    public void pokazInformacjeOBazieDanych() {
        // TODO: 0d. Wyswietl informacje o bazie danych, sterowniku oraz URL
    	doWithConnection(conn -> {
    		System.out.println(conn.getMetaData().getURL());
    				});
    }

    public void pokazListeOsob() {
        // TODO: 1a. Zaimplementować zapytanie wyświetlające listę wszystkich osób
        doWithConnection(conn -> {
            PreparedStatement ps = conn.prepareStatement("SELECT * FROM osoby");
            ResultSet rs = ps.executeQuery();
            while (rs.next()){
            	System.out.print(rs.getString("id"));
            	System.out.print("|");
            	System.out.print(rs.getString("imie"));
            	System.out.print("|");
            	System.out.print(rs.getString("nazwisko"));
            	System.out.print("|");
            	System.out.println();
            }
            rs.close();
            ps.close();
        });
        
    }
    
    public void pokazListeOsob(String imie) throws SQLException {
        // TODO: 1b. Zaimplementowac zapytanie wyswietlajace dane osob (tabela: osoby) o zadanym imieniu 
        // TODO: 1c. Poprawić implementacje, aby uniknac niebezpieczenstw zwiazanych z SQL Injection
    	doWithConnection(conn -> {
    		PreparedStatement ps = conn.prepareStatement("SELECT * FROM osoby WHERE imie =?");
    		ps.setString(1, imie); //przeciwdzialanie SQLinjection
            ResultSet rs = ps.executeQuery();
            while (rs.next()){
            	System.out.print(rs.getString("id"));
            	System.out.print("|");
            	System.out.print(rs.getString("imie"));
            	System.out.print("|");
            	System.out.print(rs.getString("nazwisko"));
            	System.out.print("|");
            	System.out.println();
            }
            rs.close();
            ps.close();
    	});
        
    }

   
    
    public void pobierzAktualnyCzasZBazyDanych() throws SQLException {
        // TODO: 1d. Napisać funkcję pobierającą bieżący datę na serwerze bazy danych 
        // Przydatne: CallableStatmement, now()
        doWithConnection(conn -> {
        	CallableStatement cs = conn.prepareCall("{? = call now()}");
        	cs.registerOutParameter(1, Types.TIMESTAMP);
        	cs.execute();
        	Timestamp nowTime = cs.getTimestamp(1);
        	System.out.println(nowTime);
            cs.close();
        });
    }

    public void wykonajFunkcjeUpper(String wartosc) throws SQLException {
        // TODO: 1e. Napisać funkcję wykonującą funkcję UPPER na przekazanym ciągu znaków
        // Przydatne: UPPER(str) 
        doWithConnection(conn -> {
        	CallableStatement cs = conn.prepareCall("{? = call UPPER(?)}");
        	cs.setString(2, wartosc);
        	cs.registerOutParameter(1, Types.VARCHAR);
        	cs.execute();
        	String output = cs.getString(1);      	
        	System.out.println(output);
        	cs.close();
        });
    }
}   
