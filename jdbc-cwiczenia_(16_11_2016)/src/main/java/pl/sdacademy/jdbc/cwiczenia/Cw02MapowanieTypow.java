package pl.sdacademy.jdbc.cwiczenia;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import pl.sdacademy.jdbc.db.ConnectionProvider;

public class Cw02MapowanieTypow extends CwiczenieJdbc {
    
    public Cw02MapowanieTypow(ConnectionProvider connectionProvider) {
        super(connectionProvider);
    }

    public void pokazSzczegoloweDaneOsoby(Integer id) throws SQLException {
        // TODO: 2a. Pobrac informacje szczegolowe osoby (w tabeli `szczegoly_osob`), zmapowac i wyswietlic szczegoly wykorzystujac mapowania ResultSet::set...
        // TODO: 2b. Pobrac liczbe osob w tabeli `osoby`
    	doWithConnection((conn -> {
    		PreparedStatement ps1 = conn.prepareStatement("SELECT * FROM szczegoly_osob WHERE osoba_id = ?");
    		PreparedStatement ps2 = conn.prepareStatement("SELECT COUNT(*) FROM osoby");
    		ps1.setInt(1, id);
    		ResultSet rs1 = ps1.executeQuery();
    		while (rs1.next()){
            	System.out.print(rs1.getString(1));
            	System.out.print("|");
            	System.out.print(rs1.getString(2));
            	System.out.print("|");
            	System.out.print(rs1.getString(3));
            	System.out.print("|");
            	System.out.print(rs1.getString(4));
            	System.out.print("|");
            	System.out.println();
    		}
    		ResultSet rs2 = ps2.executeQuery();
    		while (rs2.next()){
            	System.out.print(rs2.getString(1));
            	System.out.print("| <- ilosc osob w bazie");
            	System.out.println();
    		}
    		System.out.println();
    	   	ps1.close();
    	   	rs1.close();
    	   	ps2.close();
    	   	rs2.close();
    	}));

    }

}   
