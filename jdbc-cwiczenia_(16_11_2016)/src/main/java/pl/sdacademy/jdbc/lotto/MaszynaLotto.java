package pl.sdacademy.jdbc.lotto;

import java.time.Instant;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class MaszynaLotto {
    
    private static AtomicInteger liczbaLosowan = new AtomicInteger();
    
    public static WynikiLosowania wylosuj() {
        List<Integer> liczby = ThreadLocalRandom.current()
            .ints(1, 49)
            .distinct()
            .limit(6)
            .boxed()
            .collect(Collectors.toList());
        
        return new WynikiLosowania(
                liczbaLosowan.incrementAndGet(), 
                "Losowanie nr " + liczbaLosowan.get(), 
                liczby, 
                Instant.now());
    }
    
    public static List<WynikiLosowania> wylosuj(Integer liczbaLosowan) {
        return IntStream.range(0, liczbaLosowan)
                .mapToObj(i -> wylosuj())
                .collect(Collectors.toList());
    }
}
