<html>
<jsp:include page="fragment/header.jsp" />
<body>
    <div class="container">
        <div class="panel panel-primary">
            <div class="panel-heading">Login</div>
            <div class="panel-body">
                <form name="f" class="form-horizontal" action="performLogin" method="POST">
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                    <div class="form-group ">
                        <label class="col-sm-2 control-label">User</label>
                        <div class="col-sm-8">
                            <input type="text" name="username" class="form-control" value="">
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="col-sm-2 control-label">Password</label>
                        <div class="col-sm-8">
                            <input type="password" name="password" class="form-control" value="">
                        </div>
                    </div>
                    <input name="submit" class="col-sm-offset-2 btn btn-success" type="submit" value="Login" />
                </form>
            </div>
        </div>
    </div>
</body>
</html>