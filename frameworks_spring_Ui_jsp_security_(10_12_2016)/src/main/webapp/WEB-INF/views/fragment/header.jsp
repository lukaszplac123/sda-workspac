<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<head>
    <title>05-solution-ui-jsp-security</title>

    <!-- Bootstrap -->
    <spring:url value="/resources/css/bootstrap.min.css" var="bootstrapCss" />
    <spring:url value="/resources/css/bootstrap-theme.min.css" var="bootstrapThemeCss" />
    <spring:url value="/resources/css/bootstrap-theme.min.css" var="bootstrapJs" />
    <link href="${bootstrapCss}" rel="stylesheet" />
    <link href="${bootstrapThemeCss}" rel="stylesheet" />
    <link href="${bootstrapJs}" rel="stylesheet" />
</head>

<nav class="navbar navbar-inverse ">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="${urlHome}">SDAcademy - Spring MVC</a>
        </div>
        <div id="navbar">
            <ul class="nav navbar-nav navbar-right">
            </ul>
        </div>
    </div>
</nav>
