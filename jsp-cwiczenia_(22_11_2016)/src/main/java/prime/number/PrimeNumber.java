package prime.number;

public class PrimeNumber {
	public static void main(String[] args) {
		long[] primeNumbers = findFirstPrimeNumbers(100);

		for (int i = 0; i < primeNumbers.length; i++) {
			System.out.println(primeNumbers[i]);
		}
	}

	private static long[] findFirstPrimeNumbers(int n) {
		if (n < 1) {
			throw new IllegalArgumentException("n");
		}

		long[] result = new long[n];
		result[0] = 2;

		int foundCount = 1;
		long i = 3;

		while (foundCount < n) {
			if (isPrime(i)) {
				result[foundCount++] = i;
			}

			i += 2;
		}

		return result;
	}

	private static boolean isPrime(long x) {
		long xSqrt = (long) Math.sqrt(x);

		for (int i = 3; i <= xSqrt; i += 2) {
			if (x % i == 0) {
				return false;
			}
		}

		return true;
	}
}
