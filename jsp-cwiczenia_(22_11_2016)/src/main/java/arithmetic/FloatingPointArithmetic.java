package arithmetic;

public class FloatingPointArithmetic {
	public static void main(String[] args) {
		maxDouble();
		divisionType();
		floatSpecialCases();
	}

	private static void maxDouble() {
		// IEEE 754
		// 64 bits (8 Byte) = 1 bit (sign) + 11 bits (exponent - 1 sign + 10
		// value) + 52 bits (fraction)
		double x = 0x1.fffffffffffffP+1023;

		System.out.println(x);
		System.out.println(x == Double.MAX_VALUE);
	}

	private static void divisionType() {
		System.out.println("-----------------------");

		double x1 = 5 / 2;
		int x2 = 5 / 2;
		double x3 = 5 / 2.0;
		int x4 = (int) (5 / 2.0);

		System.out.println(x1);
		System.out.println(x2);
		System.out.println(x3);
		System.out.println(x4);
	}

	private static void floatSpecialCases() {
		System.out.println("-----------------------");

		double x1 = 3 / 0.0;
		double x2 = -2 / 0.0;
		double x3 = 0 / 0.0;

		System.out.println(x1);
		System.out.println(x2);
		System.out.println(x3);
		System.out.println(x1 * x3);
		System.out.println(x2 * x3);

		System.out.println(Double.isInfinite(x1));
		System.out.println(Double.isInfinite(x3));
		System.out.println(Double.isNaN(x1));
		System.out.println(Double.isNaN(x3));
	}
}
