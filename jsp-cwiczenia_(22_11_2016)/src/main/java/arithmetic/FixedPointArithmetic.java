package arithmetic;

public class FixedPointArithmetic {
	public static void main(String[] args) {
		maxInt();
		intOverflow();
	}

	private static void maxInt() {
		// 32 bits (4 Bytes) = (1 for sign) + (31 value)
		// 1*2^30 +...+1*2^1+1*2^0 = 2^31-1 (decimal)
		int x1 = 0x7f_ff_ff_ff; // hexadecimal (16)
		int x2 = 0b1111111111_1111111111_1111111111_1; // binary (2)
		int x3 = (1 << 31) - 1; // decimal (10)
		int x4 = 01_77777_77777; // octal (8)

		System.out.println(x1 == Integer.MAX_VALUE);
		System.out.println(x2 == Integer.MAX_VALUE);
		System.out.println(x3 == Integer.MAX_VALUE);
		System.out.println(x4 == Integer.MAX_VALUE);
	}

	private static void intOverflow() {
		System.out.println("-----------------------");

		int a = Integer.MAX_VALUE + 1;
		long b = Integer.MAX_VALUE + 1;

		System.out.println(a);
		System.out.println(b);

		try {
			int result = Math.addExact(Integer.MAX_VALUE, 1);
			System.out.println(result);
		} catch (ArithmeticException e) {
			e.printStackTrace();
		}
	}
}
