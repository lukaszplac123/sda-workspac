package datetime;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class DateTimeTest {
	public static void main(String[] args) {

		// taken from Operating System in ISO: date (year, month, day) + 'T'
		// (time separator) + time (hour, minute, second, millisecond)
		LocalDateTime localDateTime = LocalDateTime.now();

		// localDateTime with time-zone taken from Operating System
		ZonedDateTime localDateTimeWithZoneInfo = ZonedDateTime.of(localDateTime, ZoneId.systemDefault());

		System.out.println(localDateTime.toString());
		System.out.println(localDateTimeWithZoneInfo.toString());

		// *************************************88
		ZoneId losAngelesTimeZone = ZoneId.of("America/Los_Angeles");
		LocalDateTime dateTimeInLosAngeles = LocalDateTime.now(losAngelesTimeZone);
		ZonedDateTime dateTimeInLosAngelesWithTimeZoneInfo = dateTimeInLosAngeles.atZone(losAngelesTimeZone);

		System.out.println(dateTimeInLosAngeles.toString());
		System.out.println(dateTimeInLosAngelesWithTimeZoneInfo.toString());
	}
}
