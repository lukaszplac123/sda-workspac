package grammar;

public class App {
	public static void main(String[] args) {
		Automata automata = new Automata();

		String[] words = { "aa", "abcd", "as", "", "abccccce" };

		for (String word : words) {
			System.out.println(word + ": " + automata.isGeneratedByGrammar(word));
		}
	}
}
