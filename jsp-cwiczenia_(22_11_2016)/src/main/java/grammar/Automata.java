package grammar;

/**
 * 1) S -> abA | aa 2) A -> cA | B 3) B -> d | e
 */
public class Automata {

	public boolean isGeneratedByGrammar(String text) {
		if (text == null) {
			throw new IllegalArgumentException("String should be NOT NULL");
		}

		return s0(text);
	}

	private boolean s0(String text) {
		if (canConsume(text, 'a')) {
			return s1(consume(text));
		}

		return false;
	}

	private boolean s1(String text) {
		if (canConsume(text, 'a')) {
			return reachedEndOfStream(consume(text));
		}

		if (canConsume(text, 'b')) {
			return s2(consume(text));
		}
		return false;
	}

	private boolean s2(String text) {
		if (canConsume(text, 'c')) {
			return s2(consume(text));
		}

		if (canConsume(text, 'd')) {
			return reachedEndOfStream(consume(text));
		}

		if (canConsume(text, 'e')) {
			return reachedEndOfStream(consume(text));
		}

		return false;
	}

	private boolean reachedEndOfStream(String text) {
		return text.length() == 0;
	}

	private boolean canConsume(String text, char character) {
		return text.length() > 0 && text.charAt(0) == character;
	}

	private String consume(String text) {
		return text.substring(1);
	}
}
