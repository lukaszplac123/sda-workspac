package reflexion;

import java.lang.reflect.Method;
import java.util.ArrayList;

public class Test {
	public static void main(String[] args) {
		Class<Square> myClassStructureDescripton = Square.class;
		Method[] methods = myClassStructureDescripton.getMethods();

		Object x = new Square(1);
		Square y = new Square(2);

		for (Method method : methods) {
			System.out.println(method.getName());
		}

		new Square(4).getClass();

		System.out.println("-----------------------");
		try {
			Class<?> myClassStructureDescripton2 = Class.forName("reflexion.Squareb");
			new Object().hashCode();
			new String("a");

			new ArrayList<Integer>().stream().distinct().count();

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
}
