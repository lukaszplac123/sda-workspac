package com.sda.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JdbcTest {
	public static void main(String[] args) {

		Connection connection = null;
		try {
			Class.forName("org.h2.Driver");
			connection = DriverManager.getConnection("jdbc:h2:~/mydb", "sa", "");

			String query = "select * from user; select 1 from user;";

			try (Statement statement = connection.createStatement()) {
				statement.execute(query);

				while (statement.getMoreResults()) {
					ResultSet resultSet = statement.getResultSet();
					int i = 1;
					while (resultSet.next()) {
						System.out.println(i + " " + resultSet.getString(1));

					}
					System.out.println("query");
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
