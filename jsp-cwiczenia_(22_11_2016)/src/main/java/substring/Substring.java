package substring;

import org.apache.commons.lang3.StringUtils;

public class Substring {
	public static void main(String[] args) {
		String text = "axd";
		String sequenceToFind = null;

		System.out.println(isSubString(text, sequenceToFind));
		System.out.println(StringUtils.contains(text, sequenceToFind));
	}

	private static boolean isSubString(String text, String sequenceToFind) {
		if (text == null || sequenceToFind == null || text.length() < sequenceToFind.length()) {
			return false;
		}

		return text.contains(sequenceToFind);
	}
}
