package whiripool;

public class Whiripool {
	public static void main(String[] args) {
		int n = 3;
		int[][] matrix = create2DimMatrix(n);

		fill(matrix);
		print(matrix);
	}

	private static void fill(int[][] matrix) {
		int label = 0;
		for (int i = 0; i < matrix.length / 2; i++) {
			label = fillCircle(i, matrix, label);
		}

		int level = matrix.length / 2;
		if (matrix.length % 2 != 0) {
			matrix[level][level] = label;
		}
	}

	private static int fillCircle(int level, int[][] matrix, int label) {
		int n = matrix.length;

		for (int i = level; i < n - level; i++) {
			matrix[level][i] = label++;
		}

		for (int i = level; i < n - level; i++) {
			matrix[i][n - 1 - level] = label++;
		}

		for (int i = level; i < n - level; i++) {
			matrix[n - 1 - level][n - 1 - i] = label++;
		}

		for (int i = level; i < n - level - 1; i++) {
			matrix[n - 1 - i][level] = label++;
		}

		return label;
	}

	private static int[][] create2DimMatrix(int n) {
		int[][] matrix = new int[n][];
		for (int i = 0; i < matrix.length; i++) {
			matrix[i] = new int[matrix.length];
		}
		return matrix;
	}

	private static void print(int[][] matrix) {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				System.out.print(matrix[i][j] + "\t");
			}
			System.out.println("");
		}
	}
}
