package my.sockets;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.net.Socket;

public class SocketThread extends Thread{
	
	Socket clientSocket;
	
	public SocketThread(Socket socket, int name) {
		clientSocket = socket;
		this.setName(Integer.toString(name));
		start();
	}
	
	@Override
	public void run(){
		try {
			Reader whatClientSays = new InputStreamReader(clientSocket.getInputStream());
			BufferedReader in = new BufferedReader(whatClientSays);
			Writer whatClientReceives = new OutputStreamWriter(clientSocket.getOutputStream());
			BufferedWriter out = new BufferedWriter(whatClientReceives);
			sendToClient(out, "Hello send me 5 lines");
			for (int i = 0; i < 5; i++){
				String lineFromClient = in.readLine();
				System.out.println(i + " " + lineFromClient);
				sendToClient(out, getName()+" sends " + lineFromClient);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void sendToClient (BufferedWriter out, String message) throws IOException{
		out.write(message);
		out.flush();
	}

}
