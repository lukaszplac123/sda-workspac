package logging;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.util.logging.StreamHandler;

public class LoggingTest {
	private static final Logger LOGGER = Logger.getLogger(LoggingTest.class.getName());

	public static void main(String[] args) {
		String property = System.getProperty("myLog");
		Logger.getLogger("myLogger", property);
		Logger.getGlobal().setLevel(Level.INFO);
		Logger.getGlobal().addHandler(new StreamHandler(System.out, new SimpleFormatter()));

		Logger.getGlobal().info("(0) global logger test");
		LOGGER.fine("(1) ************ test");

		MyClass test = new MyClass();
		test.run();
	}
}
