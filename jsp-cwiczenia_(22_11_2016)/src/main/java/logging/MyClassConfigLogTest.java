package logging;

import java.io.File;
import java.time.LocalDateTime;
import java.util.logging.Logger;

public class MyClassConfigLogTest {
	private static final Logger LOGGER = Logger.getLogger(MyClassConfigLogTest.class.getName());

	public static void main(String[] args) {

		System.out.println(new File(".").getAbsolutePath());

		LOGGER.info("Test for file handler " + LocalDateTime.now());
	}
}
