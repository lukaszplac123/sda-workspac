package logging;

import java.util.logging.Level;
import java.util.logging.Logger;

public class MyClass {
	private static final Logger LOGGER = Logger.getLogger(LoggingTest.class.getName());

	public void run() {
		Logger.getGlobal().info("(0) global logger test");

		int x = 4;
		LOGGER.log(Level.INFO, "(1) message: x = " + x);
		LOGGER.info("(2) my second message");
		LOGGER.log(Level.WARNING, "(3) message: x = " + x);
	}
}
