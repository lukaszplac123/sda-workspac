package logging;

import java.io.File;
import java.time.LocalDateTime;
import java.util.logging.Logger;

public class MyFileConfigLogTest {
	private static final Logger LOGGER = Logger.getLogger(MyFileConfigLogTest.class.getName());

	public static void main(String[] args) {

		System.out.println(new File(".").getAbsolutePath());

		LOGGER.info("[FILE CFG] Test for file handler " + LocalDateTime.now());
	}
}
