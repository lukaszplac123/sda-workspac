package logging;

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.LogManager;

public class LoggingConfig {
	public LoggingConfig() {
		try {
			System.out.println("Reading config");

			InputStream logConfigFileContent = getClass().getResourceAsStream("/log.properties");
			LogManager.getLogManager().readConfiguration(logConfigFileContent);
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
