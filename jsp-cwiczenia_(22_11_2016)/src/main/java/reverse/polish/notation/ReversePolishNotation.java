package reverse.polish.notation;

/*
 * 3 mozliwosci zapisu operacji:
 * in-fix: argument operator argument, e.g. 1 + 2
 * pre-fix: operator argument argument, e.g. + 1 2
 * post-fix (zwany rowniez Odwrotna Notacja Polska): argument argument operator, e.g. 1 2 + 
 */
public class ReversePolishNotation {

	public static void main(String[] args) {
		ToReversePolishNotationTransformer transformer = new ToReversePolishNotationTransformer();

		String[] infixExpressions = { "1 + 2 * 3 + 4", 
				"1+(2+3)+4",
				"(1+2+3)*4", 
				"(1+2) * (3+4)" };

		for (String infixExpression : infixExpressions) {
			System.out.println(infixExpression + " -> " + transformer.transform(infixExpression));
		}
	}
}
