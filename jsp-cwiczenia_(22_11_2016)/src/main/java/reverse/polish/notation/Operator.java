package reverse.polish.notation;

public enum Operator {
	ADD('+', 1), MUL('*', 2);

	private char operatorChar;
	private int priority;

	private Operator(char operatorChar, int priority) {
		this.operatorChar = operatorChar;
		this.priority = priority;
	}

	public boolean hasLowerPriorityThan(Operator other) {
		return this.priority < other.priority;
	}

	public static Operator toEnum(char operatorChar) {
		if (operatorChar == ADD.operatorChar)
			return ADD;

		if (operatorChar == MUL.operatorChar)
			return MUL;

		throw new IllegalArgumentException("Unknown operator");
	}

	public static boolean isOperator(char possibleOperatorChar) {
		for (Operator operator : values()) {
			if (operator.getOperatorChar() == possibleOperatorChar) {
				return true;
			}
		}

		return false;
	}

	public char getOperatorChar() {
		return operatorChar;
	}
}
