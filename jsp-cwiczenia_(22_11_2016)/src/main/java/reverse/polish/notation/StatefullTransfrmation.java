package reverse.polish.notation;

import java.util.Stack;
import java.util.function.Function;

public class StatefullTransfrmation implements Function<String, String> {
	private Stack<Character> postfixNotation = new Stack<>();
	private Stack<Character> operatorPriorityStack = new Stack<>();

	public String apply(String correctInfixExpression) {

		for (int i = 0; i < correctInfixExpression.length(); i++) {
			char nextChar = correctInfixExpression.charAt(i);

			if (!isSkippable(nextChar)) {
				processCharacter(nextChar);
			}
		}

		putAllOperatorsOnTopOfPostfixStack(false);

		return this.toString();
	}

	private void processCharacter(char nextChar) {
		if (Operator.isOperator(nextChar)) {
			Operator nextOperator = Operator.toEnum(nextChar);

			if (!operatorPriorityStack.isEmpty()) {
				if (operatorPriorityStack.peek() != '(') {
					Operator lastOperatorOnStack = Operator.toEnum(operatorPriorityStack.lastElement());

					if (!lastOperatorOnStack.hasLowerPriorityThan(nextOperator)) {
						putAllOperatorsOnTopOfPostfixStack(true);
					}
				}
			}

			operatorPriorityStack.push(nextOperator.getOperatorChar());
		} else if (nextChar == ')') {
			putAllOperatorsOnTopOfPostfixStack(true);
		} else if (nextChar == '(') {
			operatorPriorityStack.push('(');
		} else {
			postfixNotation.push(nextChar);
		}
	}

	private boolean isSkippable(char nextChar) {
		return Character.isWhitespace(nextChar);
	}

	private void putAllOperatorsOnTopOfPostfixStack(boolean tillOpeningParenthesis) {
		while (!operatorPriorityStack.isEmpty()) {

			if (tillOpeningParenthesis) {
				if (operatorPriorityStack.peek() != '(') {
					Character pop = operatorPriorityStack.pop();
					postfixNotation.push(pop);
				} else {
					Character pop = operatorPriorityStack.pop();
					return;
				}
			} else {
				Character pop = operatorPriorityStack.pop();
				postfixNotation.push(pop);
			}
		}
	}

	@Override
	public String toString() {
		return printStact(postfixNotation);
	}

	private String printStact(Stack<Character> stack) {
		StringBuilder sb = new StringBuilder();

		for (int i = 0; i < stack.size(); i++) {
			sb.append(stack.get(i));
			sb.append(' ');
		}
		return sb.toString();
	}
	
	public Stack<Character> getStack()
	{
		return postfixNotation;
	}
}
