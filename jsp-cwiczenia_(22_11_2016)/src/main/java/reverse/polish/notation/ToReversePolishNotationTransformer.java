package reverse.polish.notation;

import java.util.Stack;

public class ToReversePolishNotationTransformer {
	private Evaluator evaluator = new Evaluator();

	public String transform(String correctInfixExpression) {
		StatefullTransfrmation transformation = new StatefullTransfrmation();

		String reversedPolishNotation = transformation.apply(correctInfixExpression);
		Stack<Character> postFixStackToEvaluate = transformation.getStack();

		System.out.println(evaluator.evaluator(reversedPolishNotation, postFixStackToEvaluate));

		return reversedPolishNotation;
	}
}
