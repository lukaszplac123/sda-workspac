package reverse.polish.notation;

import java.util.Collections;
import java.util.Stack;
import java.util.function.BiFunction;

public class Evaluator {
	private Stack<Token> reversedStack = new Stack<>();

	public double evaluator(String reversedPolishNotation, Stack<Character> postFixStackToEvaluate) {
		//System.out.println(reversedPolishNotation);

		Collections.reverse(postFixStackToEvaluate);

		while (!postFixStackToEvaluate.isEmpty()) {
			Token lastToken = pushOnTopOfEvaluationStack(postFixStackToEvaluate.pop());

			if (lastToken instanceof Operator) {
				Operator operator = (Operator) reversedStack.pop();
				Number arg1 = (Number) reversedStack.pop();
				Number arg2 = (Number) reversedStack.pop();

				//System.out.println(operator + " " + arg1 + " " + arg2);

				Token operationResult = operator.apply(arg1, arg2);
				reversedStack.push(operationResult);
			}
		}

		if (reversedStack.isEmpty()) {
			return 0;
		}

		return ((Number) reversedStack.pop()).doubleValue();
	}

	private Token pushOnTopOfEvaluationStack(Character nextToken) {
		if (isOperator(nextToken)) {
			reversedStack.push(new Operator(nextToken));
		} else {
			reversedStack.push(new Number(nextToken));
		}

		return reversedStack.peek();
	}

	public boolean isOperator(char operator) {
		return operator == '+' || operator == '*';
	}

	private interface Token {
	}

	private class Number implements Token {
		private double argument;

		public Number(char digit) {
			this(Double.parseDouble(Character.toString(digit)));
		}

		public Number(double argument) {
			this.argument = argument;
		}

		public double doubleValue() {
			return argument;
		}

		@Override
		public String toString() {
			return Double.toString(argument);
		}
	}

	private class Operator implements Token {
		private char operator;
		private BiFunction<Number, Number, Number> operation;

		public Operator(char operator) {
			this.operator = operator;

			if (operator == '+') {
				operation = (a, b) -> new Number(a.doubleValue() + b.doubleValue());
			} else if (operator == '*') {
				operation = (a, b) -> new Number(a.doubleValue() * b.doubleValue());
			}
		}

		public Token apply(Number a, Number b) {
			return operation.apply(a, b);
		}

		@Override
		public String toString() {
			return Character.toString(operator);
		}
	}
}
