package zadanie4i5;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Exc4BuffRead {
	
	public static void main(String[] args) throws IOException{
		
		FileReader fileReader = null;
		FileWriter fileWriter = null;
		
		BufferedReader readBuffer = null;
		
		
		try {
			fileReader = new FileReader("inputFile.txt");
			fileWriter = new FileWriter("outputFile.txt");
			readBuffer = new BufferedReader(fileReader);
		
			
			String str = "";
			while((str = readBuffer.readLine()) != null){
				fileWriter.write(str);
			}
			fileWriter.write("Udalo sie");
			
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();	
		}finally{
			if (fileReader != null) fileReader.close();
			if (fileWriter != null) fileWriter.close();
			if (readBuffer != null) readBuffer.close();
		}	
		
	}
	
}

