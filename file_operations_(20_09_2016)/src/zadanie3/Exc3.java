package zadanie3;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class Exc3 {
	
	public static void main(String[] args) throws IOException{
		
		FileInputStream inStream = null;
		FileOutputStream outStream = null;
		
		try {
			inStream = new FileInputStream("inputFile.txt");
			outStream = new FileOutputStream("outputFile.txt");
			
			int c;
			while((c = inStream.read()) != -1){
				outStream.write(c);
			}
			outStream.write("Udalo sie".getBytes());
			
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();	
		}finally{
			if (inStream != null) inStream.close();
			if (outStream != null)outStream.close();
		}
		
		
	}
	
}
