package zadanie7;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Exc7 {
	
	public static void main(String[] args) throws IOException{
		
		FileReader fileReader = null;
		FileWriter fileWriter = null;
		
		try {
			fileReader = new FileReader("inputFile.txt");
			fileWriter = new FileWriter("outputFile.txt");
			
			int c;
			while((c = fileReader.read()) != -1){
				if (c >= 97 && c <= 122) c=c-32;
				fileWriter.write(c);
			}
			fileWriter.write("Udalo sie");
			
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();	
		}finally{
			if (fileReader != null) fileReader.close();
			if (fileWriter != null) fileWriter.close();
		}
		
		
	}
	
}

