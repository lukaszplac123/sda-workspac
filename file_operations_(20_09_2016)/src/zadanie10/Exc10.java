package zadanie10;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Exc10 {
	
	public static void main(String[] args) throws IOException{
		
		FileReader fileReader = null;
		FileWriter fileWriter = null;
		BufferedReader fileBuffer = null;
		List<String> list = new ArrayList<String>();
		
		try {
			
			fileReader = new FileReader("wordList.txt");
			fileWriter = new FileWriter("wordListSorted.txt");
			fileBuffer = new BufferedReader(fileReader);
			
			String str;
			while ((str = fileBuffer.readLine()) != null){
				String temp = str.trim();
				list.add(temp);
			}
			Collections.sort(list);
			
			System.out.println(list);
			
			for (String item : list){
				fileWriter.write(item+"\n");
			}
					
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();	
		}finally{
			if (fileBuffer != null) fileBuffer.close();
			if (fileReader != null) fileReader.close();
			if (fileWriter != null) fileWriter.close();
		}
		
		
	}
	
}

