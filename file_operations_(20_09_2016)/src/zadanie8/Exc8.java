package zadanie8;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Exc8 {
	
	public static void main(String[] args) throws IOException{
		
		FileWriter fileWriter = null;
		
		try {
			fileWriter = new FileWriter("FizzBuzz.txt");
			
			for (int i = 0 ; i < 50; i++){
				if ((i%5 == 0) && (i%3 == 0)){
					fileWriter.write(i+"FizzBuzz");
				} else if (i%5 == 0){
					fileWriter.write(i+"Buzz");
				} else if (i%3 == 0 ){
					fileWriter.write(i+"Fizz");
				} else{fileWriter.write(i+"NIEPODZIELNE");}
			}
			fileWriter.write("Udalo sie");
			
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();	
		}finally{
			if (fileWriter != null) fileWriter.close();
		}
		
		
	}
	
}

