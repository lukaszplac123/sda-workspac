package zadanie9;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Exc9 {
	
	public static void main(String[] args) throws IOException{
		
		FileReader fileReader = null;
		BufferedReader fileBuffer;
		
		try {
			
			fileReader = new FileReader("mojPlik.txt");
			fileBuffer = new BufferedReader(fileReader);
			
			int i = 0;
			while ((fileBuffer.readLine()) != null){
				i++;
			}
			System.out.println(i);
			
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();	
		}finally{
			if (fileReader != null) fileReader.close();
		}
		
		
	}
	
}

