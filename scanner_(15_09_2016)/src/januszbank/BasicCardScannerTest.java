package januszbank;

import static org.junit.Assert.*;

import java.util.Scanner;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class BasicCardScannerTest {

	private CardScanner scanner;
	private String example1;
	private String example2;
	private String example3;
	private String example4;
	
	@Before
	public void setUp() throws Exception {
		scanner = new BasicCardScanner();
		example1 = "aavs123456789bbbb"; //za malo cyfr
		example2 = "avs123456789123456789bbbvb"; //za duzo cyfr
		example3 = "asas1234567891234567sdsd"; //16 cyfr, przypadek prawidlowy
		example4 = "asas1234567891234567sdsdasas1234567891234567sdsd"; //wystepuja 2 karty
	}

	@After
	public void tearDown() throws Exception {
	}

	//przypadek gdy za malo cyfr
	@Test
	public void test1() {
		//given
		boolean value = false;
		
		//when
		boolean result = scanner.hasCard(example1);
		
		//then
		Assert.assertTrue(value == result);
		
	}
	
	
	//przypadek gdy za duzo cyfr
	@Test
	public void test2() {
		//given
		boolean value = false;
		
		//when
		boolean result = scanner.hasCard(example2);
		
		//then
		Assert.assertTrue(value == result);
		
	}
	
	//przypadek prawidlowy - 16 cyfr
	@Test
	public void test3() {
		//given
		boolean value = true;
		
		//when
		boolean result = scanner.hasCard(example3);
		
		//then
		Assert.assertTrue(value == result);
		
	}
	
	//przypadek 2 kart w ciagu
	@Test
	public void test4() {
		//given
		boolean value = false;
		
		//when
		boolean result = scanner.hasCard(example4);
		
		//then
		Assert.assertTrue(value == result);
		
	}

}
