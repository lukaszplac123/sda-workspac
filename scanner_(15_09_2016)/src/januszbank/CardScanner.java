package januszbank;

public interface CardScanner {
	boolean hasCard(String payload);
	String mask(String payload);
}
