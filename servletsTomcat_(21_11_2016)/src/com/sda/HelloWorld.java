
package com.sda;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sun.istack.internal.logging.Logger;

public class HelloWorld extends HttpServlet {


	public static Logger logger = Logger.getLogger(HelloWorld.class.getName(), null);
	
	private static final long serialVersionUID = 779479246766449842L;

	@Override
	public void init() throws ServletException {
		super.init();
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String p1 = req.getParameter("param1");
		String p2 = req.getParameter("param2");
		logger.info("Logger info");
		resp.setContentType("text/html");
		PrintWriter pw = resp.getWriter();
		pw.append("<p>Hello World<strong> (metoda GET)</strong></p>. Have a nice day "+p1+" "+p2);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/html");
		PrintWriter pw = resp.getWriter();
		pw.append("<p>Hello World<strong> (metoda POST)</strong></p>");
	}
}
