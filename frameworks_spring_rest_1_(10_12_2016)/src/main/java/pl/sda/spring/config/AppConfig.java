package pl.sda.spring.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebMvc
@ComponentScan(value = { "pl.sda.spring.rest", "pl.sda.spring.repository" })
public class AppConfig {
}
