package pl.sda.spring.rest.model;

public class Employee {

    private long id;
    private String firstName;
    private String lastName;
    private String role;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getRole() {
        return role;
    }

    public static EmployeeBuilder builder() {
        return new EmployeeBuilder();
    }

    @Override
    public String toString(){
        return "{ ID = " + id + ", First name = " + firstName + ", Last name = " + firstName + ", Role = " + role + " }";
    }

    public static class EmployeeBuilder {
        private final Employee employee = new Employee();

        public EmployeeBuilder id(long id) {
            employee.id = id;
            return this;
        }

        public EmployeeBuilder firstName(String firstName) {
            employee.firstName = firstName;
            return this;
        }

        public EmployeeBuilder lastName(String lastName) {
            employee.lastName = lastName;
            return this;
        }

        public EmployeeBuilder role(String role) {
            employee.role = role;
            return this;
        }

        public Employee build() {
            return employee;
        }
    }
}