package pl.sda.spring.repository;

import org.springframework.stereotype.Repository;
import pl.sda.spring.rest.model.Employee;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Repository
public class EmployeeRepository {

    private static long id = 0;
    private Map<Long, Employee> db = new HashMap<>();

    public void create(Employee employee) {
        employee.setId(id++);
        db.put(employee.getId(), employee);
    }

    public List<Employee> getAll() {
        return db.values().stream().collect(Collectors.toList());
    }

    public void update(Employee employee) {
        db.put(employee.getId(), employee);
    }

    public void delete(long id) {
        db.remove(id);
    }
}
