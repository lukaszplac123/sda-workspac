public class Enums{
	public static void main(String[] args){
		Day day = Day.TUESDAY;
		
		System.out.println(day == Day.TUESDAY);
		
		for(Day each : Day.values()){
			System.out.println(each);
		}

		Direction parsed = Direction.valueOf("NORTH");
		System.out.println(parsed);
		
		Direction.valueOf("notExistent");		
	}
}

enum Day{
	MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY
}

enum Direction{
	NORTH("N"), SOUTH("S"), EAST("E"), WEST("W");
	
	private final String symbol;
	
	private Direction(String symbol){
		this.symbol = symbol;
	}

	public String getSymbol(){
		return symbol;
	}
}