public class Constructors{
	public static void main(String[] args){
		Car defaultCar = new Car();
		defaultCar.printBrand();
		new Car("Mazda").printBrand();
	}
	
	private static class Car{
		private String brand;

		public Car(){
			System.out.println("bezparametrowy konstruktor");
			this.brand = "Fiat";
		}
		
		public Car(String brand){
			System.out.println("konstruktor z parametrem");
			this.brand = brand;
		}
		
		public void printBrand(){
			System.out.println(brand);
		}
	}
}