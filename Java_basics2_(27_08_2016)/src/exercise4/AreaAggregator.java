package exercise4;

public class AreaAggregator {

	public static void main(String[] args){
		Shape[] shapes = new Shape[5];
		shapes[0] = new Circle(4);
		shapes[1] = new Rectangle(3.00, 3.00);
		shapes[2] = new Rectangle(4.00, 3.00);
		shapes[3] = new Square(5.00);
		shapes[4] = new Circle(2);
		
		AreaAggregator aggregator = new AreaAggregator();
		System.out.println("Aggregated shapes area: "+ aggregator.aggregate(shapes) + "sqm");
	}
	
	
	private double aggregate(Shape[] shapes){
		double totalArea = 0;
		for (Shape element : shapes){
			totalArea += element.area();
		}
		return totalArea;
	}
}
