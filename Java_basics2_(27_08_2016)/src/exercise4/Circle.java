package exercise4;

public class Circle implements Shape{
	
	public static final double PI = 3.14;
	private double r;
	
	public Circle(double d){
		this.r = d;
	}
	
	@Override
	public double area() {
		return PI*r*r;
	}
}
