
public class Casting {

	public static void main(String[] args){
		A a = new B();
		//B b = new A();
		B b =  (B)a;
	}
	
	private static class A{
		
	}
	
	private static class B extends A{
			
	}
}
