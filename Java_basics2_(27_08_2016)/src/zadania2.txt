1. Napisz metodę o sygnaturze:
public String[] split(String split, char separator)
która podzieli string na tablice części rozdzielonych znakiem separator.
Np. split(“aa:bb:cc”, ‘:’) zwróci [“aa”, “bb”, “cc”]

2. Mamy dwie tablice liczb całkowitych, należy:- wyświetlić te liczby, które występują w obydwu tablicach- wyświetlić liczby z obu tablic, które się nie powtarzają

3. Zaimplementuj strukturę danych stos jako klasę Stack z dwiema metodami push() i pop(). Użyj dynamicznej tablicy (w momencie gdy tablica jest pełna, tworzymy tablicę dwa razy większą i kopiujemy elementy, a gdy tablica jest wypełniona w mniej niż 75% to pomniejszamy ją dwa razy). Możesz do tego użyć metody System.arraycopy.

4. Zaprojektuj klasę do wyliczania pola figur geometrycznych. Potrzebne będą:
- interfejs Shape który będzie miał metodę area() obliczająca powierzchnię figury
- implementacja Shape dla kwadratu, prostokąta i koła
- klasę AreaAggregator która będzie miała jedną metodę o sygnaturze:
public double aggregate(Shape[] shapes)
Metoda aggregate ma zwracać łączną powierzchnię figur w tablicy shapes.

5. Zaprojektuj prosty kalkulator liczb całkowitych. Będziesz potrzebować do tego:
- interfejs Operation który będzie zawierał bezargumentową metodę calculate() 
- implementacje interfejsu Operation dla działań dodawania, odejmowania, mnożenia, dzielenia

Każda implementacja interfejsu Operation powinna mieć konstruktor który przyjmuje dwie liczby całkowite int (argumenty działania).

6. Zaprojektuj klasę abstrakcyjną Employee, która będzie zawierać pole grossSalary (zarobki brutto) i abstrakcyjną metodę calculateNetSalary() która zwraca zarobki netto. Implementacje tej klasy to FullTimeEmployee, Student oraz TemporaryEmployee. Stawki podatków dla nich będą odpowiednio 20%, 5% i 10%, a FullTimeEmployee będzie płacił dodatkowo ubezpieczenie społeczne w wysokości 1000 PLN, które należy odjąć od kwoty brutto przed policzeniem podatku. 

Napisz klasę Company do której będzie można dodawać i usuwać obiekty klasy Employee (możesz je przechowywać w tablicy) i w której w każdej chwili będzie można policzyć sumę oraz średnią arytmetyczną zarobków pracowników całej firmy.

