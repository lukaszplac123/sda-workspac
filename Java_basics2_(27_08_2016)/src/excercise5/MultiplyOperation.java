package excercise5;

public class MultiplyOperation implements Operation {

	private double a;
	private double b;
	
	public MultiplyOperation(double a, double b){
		this.a = a;
		this.b = b;
	}
	
	@Override
	public double calculate() {
		// TODO Auto-generated method stub
		return a*b;
	}

}
