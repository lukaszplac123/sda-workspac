package excercise5;

public class PlusOperation implements Operation {

	private double a;
	private double b;
	
	public PlusOperation(double a, double b){
		this.a = a;
		this.b = b;
	}
	
	@Override
	public double calculate() {
		// TODO Auto-generated method stub
		return a+b;
	}

}
