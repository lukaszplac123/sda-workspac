package excercise5;

public class Calculator {
		
		public static void main (String[] args){
			
			Calculator calc = new Calculator();
			
			Operation op1 = new PlusOperation(7.00, 5.00);
			Operation op2 = new MultiplyOperation(4.00, 4.00);
			
			Operation op3 = new PlusOperation(op1.calculate(), op2.calculate());
			
			System.out.println(op1.calculate());
			System.out.println(op2.calculate());
			System.out.println(op3.calculate());
			
		}
}
