package myFiles;

import java.lang.reflect.Array;
import java.util.Arrays;

//stos - push dodaje element na stos
//pop - zejmuje element ze stosu
public class Stack {
	
	private int[] tab;
	private int index;
	
	public Stack(int length){
		this.tab = new int[length];
		this.index = 0;
	}
	
	public static void main(String[] args){
		Stack stack = new Stack(5);
		stack.push(2);
		System.out.println("Stos:" + Arrays.toString(stack.tab));
		stack.push(5);
		System.out.println("Stos:" + Arrays.toString(stack.tab));
		stack.push(10);
		System.out.println("Stos:" + Arrays.toString(stack.tab));
		stack.push(11);
		System.out.println("Stos:" + Arrays.toString(stack.tab));
		stack.push(12);
		System.out.println("Stos:" + Arrays.toString(stack.tab));
		stack.push(13);
		System.out.println("Stos:" + Arrays.toString(stack.tab));
		stack.push(21);
		System.out.println("Stos:" + Arrays.toString(stack.tab));
		
		System.out.println("Pobieram..."+stack.pop());
		System.out.println("Stos:" + Arrays.toString(stack.tab));
		
		
	}
	
	public int pop(){
		if (index>0) {
				int temp = this.tab[--index];
				this.tab[index] = 0;
				return temp;
			}
			else {
				System.out.println("Nie ma nic na stosie."); 
				return -1;
			}
	}
	
	public void push(int element){
		this.tab[index++] = element;
		if (index >= this.tab.length) {
			int[] newTab = new int[this.tab.length*2];
			System.arraycopy(this.tab, 0, newTab, 0, this.tab.length);
			this.tab = newTab;
		}
	}
}
