package myFiles;

import java.util.Arrays;

public class ArrayExercises{
	
	static int[] tabA;
	static int[] tabB;
	
	public static void main(String[] args){
		ArrayExercises arrEx = new ArrayExercises(new int[]{1,2,3,4,5}, new int[]{1,2,3,4,7});
		arrEx.displayIntersection();
		int[] first = arrEx.displaySymmetricDifference(tabA, tabB);
		int[] seccond = arrEx.displaySymmetricDifference(tabB, tabA);
		int[] joined = arrEx.concat(first, seccond);
		System.out.println(Arrays.toString(joined));
	}
	
	ArrayExercises(int[] a, int[] b) {
		this.tabA = a;
		this.tabB = b;
	}
	
	private void displayIntersection(){
		int[] table = new int[this.tabA.length > this.tabB.length ? this.tabB.length : this.tabA.length];
		int index = 0;
		for (int i = 0 ; i < this.tabA.length; i++)
			for (int j = 0 ; j< this.tabB.length; j++){
				if (this.tabA[i] == this.tabB[j]){
					table[index++] = this.tabA[i];
					continue;
				}
			}
		for (int element : table){
			if (element != 0 ) System.out.print(element+" ");
		}
		
	}
	
	private int[] displaySymmetricDifference(int[] tab1, int[] tab2){
		int[] table = new int[this.tabA.length > this.tabB.length ? this.tabB.length : this.tabA.length];
		int index = 0;
		for (int i = 0 ; i < tab1.length; i++){
			boolean found = false;
			for (int j = 0 ; j< tab2.length; j++){
				if (tab1[i] == tab2[j])
					found = true;
			}
			if(!found) {
				table[index++] = tab1[i];
			}
		}
		return table;
	}
	
	private int[] concat (int[] tab1, int[] tab2){
		int[] joined = new int[tab1.length + tab2.length];
		int index = 0;
		for (int element: tab1)
			joined[index++] = element;
		for (int element: tab2)
			joined[index++] = element;		
		return joined;
	}
	
}
