public class AbstractClass{
	public static void main(String[] args){
		Car fast = new FastCar();
		Car slow = new SlowCar();
		fast.takeShortRide();
		slow.takeShortRide();
	}
	
	private static abstract class Car{
		// zmienna używana przez obie klasy pochodne
		protected int location;

		protected Car(){
			location = 0;
		}

		public void takeShortRide(){
			moveForward(); // wywołujemy implementację metody w klasie pochodnej
			moveForward();
			moveForward();
		}
		// musimy sami dodać modyfikatory PUBLIC (albo inny) i ABSTRACT
		public abstract void moveForward();
		public abstract int getCurrentLocation();
	}

	private static class FastCar extends Car{
		public void moveForward(){
			location += 3;
		}
		
		public int getCurrentLocation(){
			return location;
		}
	}

	private static class SlowCar extends Car{
		public void moveForward(){
			location += 1;
		}
		
		public int getCurrentLocation(){
			return location;
		}
	}
}