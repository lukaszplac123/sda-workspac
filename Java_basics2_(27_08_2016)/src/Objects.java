public class Objects{
	public static void main(String[] args){
		Car fiat = new Car("fiat");
		Car mazda = new Car("mazda");
		Car anotherMazda = new Car("mazda");
	
		System.out.println("fiat equals mazda = " + fiat.equals(mazda));
		System.out.println("mazda equals anotherMazda = " + mazda.equals(anotherMazda));
		
		System.out.println("fiat hashCode = " + fiat.hashCode());
		System.out.println("mazda hashCode = " + mazda.hashCode());
		System.out.println("anotherMazda hashCode = " + anotherMazda.hashCode());
	
		System.out.println(mazda);	
	}


	private static class Car{
		private String brand;
		
		public Car(String brand){
			this.brand = brand;
		}
	
		@Override
		public boolean equals(Object other){
			if(!(other instanceof Car)){
				return false;
			}
			Car otherCar = (Car) other;
			return otherCar != null && this.brand.equals(otherCar.brand);
		}
	
		@Override
		public int hashCode(){
			return this.brand.hashCode();
		}
		
		@Override
		public String toString(){
			return "Car with brand = " + brand;
		}
	
	}
}
