package com.javakurs.MavenAppWithEclipse.HttpClient;

import java.io.IOException;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;

public class App 
{
	private static String url = "http://www.onet.org/";

	  public static void main(String[] args) {
		  
		  
	    // Stwórz instancję HttpClienta
	    HttpClient client = new HttpClient();

	    // Stwórz instancję metody
	    GetMethod method = new GetMethod(url);
	    
	    try {
	     
	      int statusCode = client.executeMethod(method);

	      if (statusCode != HttpStatus.SC_OK) {
	        System.err.println("Problem przy wykonaniu metody: " + method.getStatusLine());
	      }

	      String response = method.getResponseBodyAsString();

	      System.out.println(response);

	    } catch (HttpException e) {
	      e.printStackTrace();
	    } catch (IOException e) {
	      e.printStackTrace();
	    } finally {
	      // zamknij połączenie
	      method.releaseConnection();
	    }  
	  }
}
