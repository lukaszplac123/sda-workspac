package pl.sda.carshop;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

@Component
public class Operator {

	@Autowired
	private List<CarShop> shops;
	@Resource
	private ApplicationContext context;

	public void setShops(List<CarShop> shops) {
		this.shops = shops;
	}

	public List<CarShop> getShops() {
		return shops;
	}

	public void prepareData() {
		for(CarShop shop : shops) {
			for(int i = 0; i < 5; i++) {
//				Car car = context.getBean("newCar", Car.class);
//				Car car = new Car("Fiat", "Panda", 1000 + i);
//				car.setMake("Fiat");
//				car.setModel("Panda");
//				car.setPrice(1000 + i);

				Car car = (Car)context.getBean("newCar", "Fiat", "Panda", 1000.0 + i);
				shop.addCar(car);
			}
		}
	}

	public void printData() {
		for(CarShop shop : shops) {
			System.out.println("Salon: " + shop.getName() + " wlasciciel: " + shop.getOwner().getName());
			for (Car car : shop.getCars()) {
				System.out.println(car.getMake() + " " + car.getModel() + " " + car.getPrice());
			}
		}
	}
}
