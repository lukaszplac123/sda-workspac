package pl.sda.carshop;


import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Program {

	void test() {
		ApplicationContext context = new ClassPathXmlApplicationContext("application.xml");

		Operator operator = context.getBean("operator", Operator.class);
		operator.prepareData();
		operator.printData();
	}

	void testJava() {
		ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);

		Operator operator = context.getBean("operator", Operator.class);
		operator.prepareData();
		operator.printData();
	}

	public static void main(String[] args) {
		Program program = new Program();
		program.testJava();
	}
}
