package pl.sda.carshop;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
@ComponentScan(value = { "pl.sda.carshop" })
public class AppConfig {


    @Bean(name = "owner1")
    public Owner owner1() {
        Owner owner = new Owner();
        owner.setName("Jan Kowalski");
        owner.setAddress("Krakow");

        return owner;
    }

    @Bean(name = "owner2")
    public Owner owner2() {
        Owner owner = new Owner();
        owner.setName("Adam Nowak");
        owner.setAddress("Rzeszow");

        return owner;
    }

    @Bean
    public CarShop carShop1(@Autowired @Qualifier("owner1") Owner owner) {
        CarShop carShop = new CarShop();
        carShop.setName("Salon samochodowy");
        carShop.setOwner(owner);

        return carShop;
    }

    @Bean
    public CarShop carShop2(@Autowired @Qualifier("owner2") Owner owner) {
        CarShop carShop = new CarShop();
        carShop.setName("Salon samochodowy");
        carShop.setOwner(owner);

        return carShop;
    }

    @Scope("prototype")
    @Bean(name = "newCar")
    public Car newCar(String make, String model, double price) {
        return new Car(make, model, price);
    }
}
