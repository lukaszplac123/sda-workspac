package com.sda.sort;

import java.lang.reflect.Array;

public class MergeSort implements Sortable {

	public int[] preSort (int[] array){
		int[] tempArray = new int[array.length];
		System.arraycopy(array, 0, tempArray, 0, array.length);
		return tempArray;
	}
	
    public void sort(int[] array) {
       // int[] tempArray = preSort(array);
        
    	if (array.length <= 1) {
    		return;
        }

        int[] first = new int[array.length / 2];
        int[] second = new int[array.length - first.length];
        System.arraycopy(array, 0, first, 0, first.length);
        System.arraycopy(array, first.length, second, 0, second.length);

        sort(first);
        sort(second);

        merge(first, second, array);
        
    }

    private static void merge(int[] first, int[] second, int[] result) {
        int iFirst = 0;
        int iSecond = 0;

        int j = 0;
        while (iFirst < first.length && iSecond < second.length) {
            if (first[iFirst] < second[iSecond]) {
                result[j] = first[iFirst];
                iFirst++;
            } else {
                result[j] = second[iSecond];
                iSecond++;
            }
            j++;
        }
        System.arraycopy(first, iFirst, result, j, first.length - iFirst);
        System.arraycopy(second, iSecond, result, j, second.length - iSecond);
    }


}
