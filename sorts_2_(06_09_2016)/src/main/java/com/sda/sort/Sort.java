package com.sda.sort;


import java.util.Arrays;
import java.util.Random;
import java.util.Arrays;

public class Sort {


    public static void main(String[] args) {

        int size = 50000;
        int[] array = getRandomValueArray(size);

        print(array);
        
        new BubbleSort().sort(array);
        System.out.println("po sortowaniu");
        print(array);

    }

    public static int[] getRandomValueArray(int size) {
        Random random = new Random();
        int[] result = new int[size];
        for (int i = 0; i < size; ++i) {
            result[i] = random.nextInt(10000);
        }
        return result;
    }


    public static void print(int[] array) {
        Arrays.stream(array).forEach(i -> {
            System.out.print(i + ", ");
        });
        System.out.println();
    }

}