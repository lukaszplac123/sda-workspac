package com.sda.sort;

public class TestDetail {
		
		private String testName;
		private int size;
		private long result;
	
		public TestDetail(int size, long time1, String name){
			this.setSize(size);
			this.setResult(time1);
			this.testName = name;
			returnDetail();
		}
		
		private TestDetail returnDetail(){
			return this;
		}

		public String getTestName() {
			return testName;
		}

		public void setTestName(String testName) {
			this.testName = testName;
		}

		public int getSize() {
			return size;
		}

		public void setSize(int size) {
			this.size = size;
		}

		public long getResult() {
			return result;
		}

		public void setResult(long result) {
			this.result = result;
		}
		
		
}
