package com.sda.sort;

import java.util.ArrayList;
import java.util.List;

public class ResultsSummary {
	
	private String testName;
	private List<TestDetail> resultsList;
	
	public ResultsSummary(){
		
		this.resultsList = new ArrayList<TestDetail>();
		
	}
	
	public void addResult(TestDetail detail){
		this.resultsList.add(detail);
	}

	public String getTestName() {
		return testName;
	}

	public void setTestName(String testName) {
		this.testName = testName;
	}
	
	public long showAverage(){
		long sum = 0;
		int i = 0;
		for (TestDetail item: resultsList){
			sum += resultsList.get(i).getResult();
			//System.out.println(resultsList.get(i).getTestName());
			//System.out.println(resultsList.get(i).getResult());
			i++;
		}
		return sum/10;
	}

}
