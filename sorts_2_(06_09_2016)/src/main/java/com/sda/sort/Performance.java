package com.sda.sort;

public class Performance {
	
	public static void main (String args[]){
		Sortable bubleSort = new BubbleSort();
		Sortable quickSort = new QuickSort();
		Sortable mergeSort = new MergeSort();
		Sortable heapSort = new HeapSort();
		
		ResultsSummary bubbleResults = new ResultsSummary();
		ResultsSummary quickResults = new ResultsSummary();
		ResultsSummary mergeResults = new ResultsSummary();
		ResultsSummary heapResults = new ResultsSummary();
		
		int size = 10_0000;
        int[] array = Sort.getRandomValueArray(size);
        int[] copy1 = Sort.getRandomValueArray(size);
        int[] copy2 = Sort.getRandomValueArray(size);
        int[] copy3 = Sort.getRandomValueArray(size);
        int[] copy4 = Sort.getRandomValueArray(size);

        		
    
		for (int i = 0; i < 10; i++){
        long time1  = System.currentTimeMillis();
        bubbleResults.setTestName("bubble test");
		bubleSort.sort(copy1);
		time1 = (System.currentTimeMillis() - time1);
		bubbleResults.addResult(new TestDetail(size, time1,"bubble sort"));
		}
        
		for (int i = 0; i < 10; i++){
		long time2  = System.currentTimeMillis();
		quickResults.setTestName("quick test");
		quickSort.sort(copy2);
		time2 = (System.currentTimeMillis() - time2);
		quickResults.addResult(new TestDetail(size, time2,"quick sort"));
		}
		
		for (int i = 0; i < 10; i++){
		long time3  = System.currentTimeMillis();
		mergeResults.setTestName("merge test");
		mergeSort.sort(copy3);
		time3 = (System.currentTimeMillis() - time3);
		mergeResults.addResult(new TestDetail(size, time3,"merge sort"));
		}
		
		for (int i = 0; i < 10; i++){
		long time4  = System.currentTimeMillis();
		heapResults.setTestName("heap test");
		heapSort.sort(copy4);
		time4 = (System.currentTimeMillis() - time4);
		heapResults.addResult(new TestDetail(size, time4,"heap sort"));
		}
		
		System.out.println("Srednia dla "+bubbleResults.getTestName()+" : "+bubbleResults.showAverage());
		System.out.println("Srednia dla "+quickResults.getTestName()+" : "+quickResults.showAverage());
		System.out.println("Srednia dla "+mergeResults.getTestName()+" : "+mergeResults.showAverage());
		System.out.println("Srednia dla "+heapResults.getTestName()+" : "+heapResults.showAverage());
		
	}
}
